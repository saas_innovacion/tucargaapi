foreman run python manage.py runscript get_models_csv \
	--settings tucargaapi.settings.prod --env .env_tucargaapi-master

# editar data/output/TransportCompany.csv
# y guardar en data/input/TransportCompany.csv

foreman run python manage.py runscript campaigns \
	--settings tucargaapi.settings.prod --env .env_tucarga-company-profile

from django.conf import settings
from django.conf.urls import patterns
from django.conf.urls.static import static
from django.http import HttpResponse

from urls import urlpatterns


def root_view(request):
    return HttpResponse()


urlpatterns += patterns(
    '',
    (r'^$', root_view),
)

urlpatterns += static('/media/', document_root=settings.MEDIA_ROOT)

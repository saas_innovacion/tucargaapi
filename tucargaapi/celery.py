from __future__ import absolute_import

from celery import Celery

from django.conf import settings


app = Celery('tucargaapi',
             broker=settings.BROKER_URL,
             backend=settings.CELERY_RESULT_BACKEND)

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ("routes", "0001_initial"),
    )

    def forwards(self, orm):
        # Adding model 'RateImport'
        db.create_table(u'rates_rateimport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('validity', self.gf('django.db.models.fields.IntegerField')(default=90)),
            ('transportcompany', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.TransportCompany'])),
            ('oneway', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('roundtrip', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('route', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['routes.RouteImport'])),
        ))
        db.send_create_signal(u'rates', ['RateImport'])

        # Adding model 'RateExport'
        db.create_table(u'rates_rateexport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('validity', self.gf('django.db.models.fields.IntegerField')(default=90)),
            ('transportcompany', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.TransportCompany'])),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('route', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['routes.RouteExport'])),
        ))
        db.send_create_signal(u'rates', ['RateExport'])


    def backwards(self, orm):
        # Deleting model 'RateImport'
        db.delete_table(u'rates_rateimport')

        # Deleting model 'RateExport'
        db.delete_table(u'rates_rateexport')


    models = {
        u'directory.certification': {
            'Meta': {'object_name': 'Certification'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'directory.company': {
            'Meta': {'object_name': 'Company'},
            'activation_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'address_city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'company_address_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_floor': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'company_address_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_street': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'business_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'business_number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'certifications': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['directory.Certification']", 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'economic_industry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'economic_sector': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_cargo_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_cargo_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_cargo_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_norte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_sur': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_sur_extremo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_clientes': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_comuna': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_direccion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_ejecutivo_que_lo_visito': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_fecha_de_registro_en_el_sitio_web': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_fecha_reunion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_nombre_empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_razon_social': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_region': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rm': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rubro_economico': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rut': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rutas_frecuentes': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sector_eco_revisado': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sitio_web': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sur_extremo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_tipo_de_carga_que_mueve': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_tipo_de_empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_transportistas_con_los_que_trabaja': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_zona_norte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_zona_sur': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pilot_group': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'registration_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'new'", 'max_length': '100'}),
            'web_site': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'year_of_creation': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.coverage': {
            'Meta': {'object_name': 'Coverage'},
            'from_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_from_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'to_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_to_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"})
        },
        u'directory.transportcompany': {
            'LTL_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'TransportCompany'},
            'company': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['directory.Company']", 'unique': 'True'}),
            'coverage': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['directory.Coverage']", 'symmetrical': 'False'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'do_international_trips': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'empty_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freight_types': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'full_load_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'international_trips_from_1': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_from_2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_from_3': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_1': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_3': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'number_of_trips_per_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'locationstree.location': {
            'Meta': {'object_name': 'Location'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'rates.rateexport': {
            'Meta': {'object_name': 'RateExport'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'route': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['routes.RouteExport']"}),
            'transportcompany': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.TransportCompany']"}),
            'validity': ('django.db.models.fields.IntegerField', [], {'default': '90'})
        },
        u'rates.rateimport': {
            'Meta': {'object_name': 'RateImport'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'oneway': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'roundtrip': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'route': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['routes.RouteImport']"}),
            'transportcompany': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.TransportCompany']"}),
            'validity': ('django.db.models.fields.IntegerField', [], {'default': '90'})
        },
        u'routes.routeexport': {
            'Meta': {'unique_together': "(('deposit', 'client', 'port'),)", 'object_name': 'RouteExport'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeexport_clients'", 'to': u"orm['routes.RoutePlace']"}),
            'deposit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'routeexport_desposits'", 'null': 'True', 'to': u"orm['routes.RoutePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeexport_ports'", 'to': u"orm['routes.RoutePlace']"}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Coverage']", 'null': 'True'})
        },
        u'routes.routeimport': {
            'Meta': {'unique_together': "(('port', 'client', 'deposit'),)", 'object_name': 'RouteImport'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeimport_clients'", 'to': u"orm['routes.RoutePlace']"}),
            'deposit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'routeimport_desposits'", 'null': 'True', 'to': u"orm['routes.RoutePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeimport_ports'", 'to': u"orm['routes.RoutePlace']"}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Coverage']", 'null': 'True'})
        },
        u'routes.routeplace': {
            'Meta': {'object_name': 'RoutePlace'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['rates']

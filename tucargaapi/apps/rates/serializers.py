from rest_framework import serializers

import models


class RateExportSerializer(serializers.ModelSerializer):
    update_url = serializers.Field(source='get_update_url')

    class Meta:

        model = models.RateExport
        exclude = ('transportcompany', )


class RateImportSerializer(serializers.ModelSerializer):
    update_url = serializers.Field(source='get_update_url')

    class Meta:

        model = models.RateImport
        exclude = ('transportcompany', )








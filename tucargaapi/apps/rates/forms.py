from django import forms

from .models import RateExport, RateImport


class RateExportForm(forms.ModelForm):

    class Meta:
        model = RateExport
        exclude = ('transportcompany', )
        widgets = {
            'route': forms.HiddenInput(),
        }


class RateImportForm(forms.ModelForm):

    class Meta:
        model = RateImport
        exclude = ('transportcompany', )
        widgets = {
            'route': forms.HiddenInput(),
        }

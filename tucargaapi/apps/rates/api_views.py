from rest_framework import generics

import models
import serializers


class CreateRateBaseAPIView(generics.CreateAPIView):

    def pre_save(self, obj):
        obj.transportcompany = \
            self.request.user.userdirectory.company.transportcompany


class UpdateFilterByTransportcompanyAPIView(generics.UpdateAPIView):

    def get_queryset(self):
        transportcompany = \
            self.request.user.userdirectory.company.transportcompany
        queryset = super(
            UpdateFilterByTransportcompanyAPIView, self).get_queryset().filter(
            transportcompany=transportcompany)
        return queryset


class RateExportCreateAPIView(CreateRateBaseAPIView):

    model = models.RateExport
    serializer_class = serializers.RateExportSerializer


class RateExportUpdateAPIView(UpdateFilterByTransportcompanyAPIView):

    model = models.RateExport


class RateImportCreateAPIView(CreateRateBaseAPIView):

    model = models.RateImport
    serializer_class = serializers.RateImportSerializer


class RateImportUpdateAPIView(UpdateFilterByTransportcompanyAPIView):

    model = models.RateImport

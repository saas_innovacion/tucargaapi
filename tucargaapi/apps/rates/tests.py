from django.core.urlresolvers import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from django_nopassword.models import LoginCode

from tucargaapi.apps.directory import test_factories as directory_factories
from tucargaapi.apps.routes import test_factories as routes_factories

import models
import test_factories

USER_CREDENTIALS = {'email': 'j@doe.com', 'password': ''}
INVALID_USER_CREDENTIALS = {'email': 'homer@s.com', 'password': ''}

client = APIClient()


class PostTestCaseBase(TestCase):

    fixtures = ['fixtures/sites.json']

    def setUp(self):
        self.user = directory_factories.UserFactory(**USER_CREDENTIALS)
        self.login_code = LoginCode.create_code_for_user(self.user)
        self.company = directory_factories.CompanyFactory()
        directory_factories.UserDirectoryFactory(
            user=self.user, company=self.company)

    def login(self, login_code=None, email=None):
        code = login_code and login_code.code or self.login_code.code
        email = email or self.user.email
        result = client.login(code=code, email=email)
        self.assertTrue(result, msg='%s: %s' % (code, email))

    def post_or_put(self, *args):
        raise Exception(
            '`post_or_put` must be implemented in a mode specific class')

    def assertPost(self, url, data):
        response = self.post_or_put(url, data)
        msg = '%s, %s \n %s' % (
            response.status_code, self.EXPECTED_STATUS, response)

        self.assertEqual(response.status_code, self.EXPECTED_STATUS,
                         msg=msg)
        self.assertIn('application/json', response._headers['content-type'])


class CreateTestCaseBase(PostTestCaseBase):

    EXPECTED_STATUS = status.HTTP_201_CREATED

    def post_or_put(self, *args):
        return client.post(*args)


class UpdateTestCaseBase(PostTestCaseBase):

    EXPECTED_STATUS = status.HTTP_200_OK

    def post_or_put(self, *args):
        return client.patch(*args)


class RateExportCreateViewTests(CreateTestCaseBase):

    def test_post(self):
        url = reverse('rate-export-create')
        directory_factories.TransportCompanyFactory(company=self.company)
        route = routes_factories.RouteExportFactory()
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login()

        self.assertPost(url, data)


class RateExportUpdateViewTests(UpdateTestCaseBase):

    def test_post(self):
        transportcompany = directory_factories.TransportCompanyFactory(
            company=self.company)
        route = routes_factories.RouteExportFactory()
        rate = test_factories.RateExportFactory(
            route=route, transportcompany=transportcompany)
        url = reverse('rate-export-update', kwargs={'pk': rate.pk})
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login()

        self.assertPost(url, data)

    def test_post_only_owner(self):
        self.EXPECTED_STATUS = status.HTTP_404_NOT_FOUND
        transportcompany = directory_factories.TransportCompanyFactory(
            company=self.company)
        user = directory_factories.UserFactory(**INVALID_USER_CREDENTIALS)
        login_code = LoginCode.create_code_for_user(user)
        company = directory_factories.CompanyFactory()
        directory_factories.UserDirectoryFactory(
            user=user, company=company)
        directory_factories.TransportCompanyFactory(company=company)
        route = routes_factories.RouteExportFactory()
        rate = test_factories.RateExportFactory(
            route=route, transportcompany=transportcompany)
        url = reverse('rate-export-update', kwargs={'pk': rate.pk})
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login(login_code, user.email)

        self.assertPost(url, data)


class RateImportCreateViewTests(CreateTestCaseBase):

    def test_post(self):
        url = reverse('rate-import-create')
        directory_factories.TransportCompanyFactory(company=self.company)
        route = routes_factories.RouteImportFactory()
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login()

        self.assertPost(url, data)


class RateImportUpdateViewTests(UpdateTestCaseBase):

    def test_post(self):
        transportcompany = directory_factories.TransportCompanyFactory(
            company=self.company)
        route = routes_factories.RouteImportFactory()
        rate = test_factories.RateImportFactory(
            route=route, transportcompany=transportcompany)
        url = reverse('rate-import-update', kwargs={'pk': rate.pk})
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login()

        self.assertPost(url, data)

    def test_post_only_owner(self):
        self.EXPECTED_STATUS = status.HTTP_404_NOT_FOUND
        transportcompany = directory_factories.TransportCompanyFactory(
            company=self.company)
        user = directory_factories.UserFactory(**INVALID_USER_CREDENTIALS)
        login_code = LoginCode.create_code_for_user(user)
        company = directory_factories.CompanyFactory()
        directory_factories.UserDirectoryFactory(
            user=user, company=company)
        directory_factories.TransportCompanyFactory(company=company)
        route = routes_factories.RouteImportFactory()
        rate = test_factories.RateImportFactory(
            route=route, transportcompany=transportcompany)
        url = reverse('rate-import-update', kwargs={'pk': rate.pk})
        data = {'route': route.pk,
                'amount': 100000,
                'validity': models.RateAbstractBase.VALIDITY_90}
        self.login(login_code, user.email)

        self.assertPost(url, data)

# encoding: utf-8

from django.core.urlresolvers import reverse
from django.db import models

from model_utils.models import TimeStampedModel

from tucargaapi.apps.routes.models import RouteImport, RouteExport


class RateAbstractBase(TimeStampedModel):

    VALIDITY_30 = 30
    VALIDITY_60 = 60
    VALIDITY_90 = 90

    VALIDITY_CHOICES = (
        (VALIDITY_30, '30 días'),
        (VALIDITY_60, '60 días'),
        (VALIDITY_90, '90 días'),
        )

    validity = models.IntegerField('Vigencia', choices=VALIDITY_CHOICES,
                                   default=VALIDITY_90)
    transportcompany = models.ForeignKey('directory.TransportCompany')

    class Meta:
        abstract = True


class RateImport(RateAbstractBase):

    oneway = models.IntegerField('$ Oneway', null=True, blank=True)
    roundtrip = models.IntegerField('$ Roundtrip', null=True, blank=True)
    route = models.ForeignKey(RouteImport)

    class Meta:
        verbose_name = 'tarifa importación'
        verbose_name_plural = 'tarifas importación'

    def get_update_url(self):
        return reverse('rate-import-update', args=[str(self.id)])


class RateExport(RateAbstractBase):

    amount = models.IntegerField('precio + IVA')
    route = models.ForeignKey(RouteExport)

    class Meta:
        verbose_name = 'tarifa exportación'
        verbose_name_plural = 'tarifas exportación'

    def get_update_url(self):
        return reverse('rate-export-update', args=[str(self.id)])

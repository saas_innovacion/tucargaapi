from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .api_views import (RateExportCreateAPIView, RateExportUpdateAPIView,
                        RateImportCreateAPIView, RateImportUpdateAPIView)

urlpatterns = patterns(
    '',
    # API
    url(r'^export/create/$',
        login_required(RateExportCreateAPIView.as_view()),
        name='rate-export-create'),
    url(r'^export/update/(?P<pk>\d+)/$',
        login_required(RateExportUpdateAPIView.as_view()),
        name='rate-export-update'),
    url(r'^import/create/$',
        login_required(RateImportCreateAPIView.as_view()),
        name='rate-import-create'),
    url(r'^import/update/(?P<pk>\d+)/$',
        login_required(RateImportUpdateAPIView.as_view()),
        name='rate-import-update'),
)

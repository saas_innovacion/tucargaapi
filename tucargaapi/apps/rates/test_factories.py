import factory

from tucargaapi.apps.routes import test_factories as routes_factories

from . import models


class RateExportFactory(factory.django.DjangoModelFactory):

    route = routes_factories.RouteExportFactory
    amount = 150000

    FACTORY_FOR = models.RateExport


class RateImportFactory(factory.django.DjangoModelFactory):

    route = routes_factories.RouteImportFactory

    FACTORY_FOR = models.RateImport

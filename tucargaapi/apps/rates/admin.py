from django.contrib import admin

from .models import RateExport, RateImport


class RateExportAdmin(admin.ModelAdmin):

    list_display = ('route', 'transportcompany', 'amount')

    readonly_fields = RateExport._meta.get_all_field_names()


class RateImportAdmin(admin.ModelAdmin):

    readonly_fields = RateImport._meta.get_all_field_names()

    list_display = ('route', 'transportcompany', 'oneway', 'roundtrip')


admin.site.register(RateExport, RateExportAdmin)
admin.site.register(RateImport, RateImportAdmin)

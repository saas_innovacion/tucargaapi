from django.db import IntegrityError

from tucargaapi.celery import app as celery

from .models import Report


@celery.task
def create_report_task(**kwargs):
    try:
        rp = Report()
        rp.main(**kwargs)
        rp.to_model()
        rp.save()
    except IntegrityError:
        # silently pass when attempting to create duplicated report
        pass

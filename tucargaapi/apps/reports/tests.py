import datetime

from django.test import TestCase

from tucargaapi.apps.directory import test_factories
from tucargaapi.apps.directory.models import Company, Freight, Quote
from .models import Report


class ReportTests(TestCase):

    fixtures = ['fixtures/sites.json']

    def test_main(self):
        report = Report()
        report.main()

        self.assertIsNotNone(report.start_date)
        self.assertIsNotNone(report.end_date)

        self.assertIsNotNone(report.published_freights)
        self.assertIsNotNone(report.published_freights_count)
        self.assertIsNotNone(report.published_freights_spot)
        self.assertIsNotNone(report.published_freights_count_by_spot)
        self.assertIsNotNone(report.published_freights_tender)
        self.assertIsNotNone(report.published_freights_count_by_tender)

        self.assertIsNotNone(report.published_quotes)
        self.assertIsNotNone(report.published_quotes_count)
        self.assertIsNotNone(report.published_quotes_by_spot)
        self.assertIsNotNone(report.published_quotes_by_tender)

        self.assertIsNotNone(report.registered_freightcompanies_period)
        self.assertIsNotNone(report.registered_freightcompanies_all)
        self.assertIsNotNone(report.registered_transportcompanies_period)
        self.assertIsNotNone(report.registered_transportcompanies_all)

        self.assertIsNotNone(report.allotted_quotes)
        self.assertIsNotNone(report.allotted_quotes_by_spot)
        self.assertIsNotNone(report.allotted_quotes_by_tender)

        self.assertIsNotNone(report.published_freights_spot_by_coverage)
        self.assertIsNotNone(report.published_freights_tender_by_coverage)
        self.assertIsNotNone(report.quotes_per_freight_count)

        self.assertIsNotNone(report.price_per_origin_destination_by_spot)
        self.assertIsNotNone(report.price_per_origin_destination_by_tender)

        self.assertIsNotNone(report.truck_count_by_spot)
        self.assertIsNotNone(report.truck_count_by_tender)

        self.assertIsNotNone(report.company_count)

    def test_to_model(self):
        report = Report()
        report.main()

        report.to_model()

        self.assertTrue(report.number)

        self.assertEqual(report.published_freights_count, 0)
        self.assertEqual(report.published_freights_count_by_spot, 0)
        self.assertEqual(report.published_freights_count_by_tender, 0)

        self.assertEqual(report.company_count, 0)

        self.assertEqual(report.published_quotes_count, 0)
        self.assertEqual(report.published_quotes_count_by_spot, 0)
        self.assertEqual(report.published_quotes_count_by_tender, 0)

        self.assertEqual(report.quotes_per_freight_count, 0)
        self.assertEqual(report.quotes_per_freight_count_by_spot, 0)
        self.assertEqual(report.quotes_per_freight_count_by_tender, 0)

        self.assertEqual(report.allotted_quotes_count, 0)
        self.assertEqual(report.allotted_quotes_count_by_spot, 0)
        self.assertEqual(report.allotted_quotes_count_by_tender, 0)

        self.assertEqual(report.truck_count, 0)
        self.assertEqual(report.truck_count_by_spot, 0)
        self.assertEqual(report.truck_count_by_tender, 0)

        self.assertEqual(report.registered_freightcompanies_all_count, 0)
        self.assertEqual(report.registered_freightcompanies_period_count, 0)
        self.assertEqual(report.registered_transportcompanies_all_count, 0)
        self.assertEqual(report.registered_transportcompanies_period_count, 0)

        self.assertIsNotNone(
            report.freights_per_origin_destination_json_by_spot)
        self.assertIsNotNone(
            report.freights_per_origin_destination_json_by_tender)
        self.assertIsNotNone(
            report.prices_per_origin_destination_json_by_spot)
        self.assertIsNotNone(
            report.prices_per_origin_destination_json_by_tender)

        self.assertIsNotNone(report.truck_count)
        self.assertIsNotNone(report.truck_count_by_spot)
        self.assertIsNotNone(report.truck_count_by_tender)

    def test_save(self):
        report = Report()
        report.main()
        report.to_model()

        report.save()

    def test_registered_freightcompanies_period(self):
        company = test_factories.CompanyFactory(status=Company.STATUS_REJECTED)
        test_factories.FreightCompanyFactory(company=company)
        test_factories.FreightCompanyFactory()
        report = Report()
        report._get_period_init_end()

        report._registered_freightcompanies_period()

        self.assertEqual(report.registered_freightcompanies_period.count(), 1)

    def test_registered_freightcompanies_all(self):
        past = datetime.datetime.now() - datetime.timedelta(days=100)
        company = test_factories.CompanyFactory(status=Company.STATUS_REJECTED)
        test_factories.FreightCompanyFactory(company=company, created=past)
        test_factories.FreightCompanyFactory(created=past)
        test_factories.FreightCompanyFactory()
        report = Report()
        report._get_period_init_end()

        report._registered_freightcompanies_all()

        self.assertEqual(report.registered_freightcompanies_all.count(), 1)

    def test_registered_transportcompanies_period(self):
        company = test_factories.CompanyFactory(status=Company.STATUS_REJECTED)
        test_factories.TransportCompanyFactory(company=company)
        test_factories.TransportCompanyFactory()
        report = Report()
        report._get_period_init_end()

        report._registered_transportcompanies_period()

        self.assertEqual(
            report.registered_transportcompanies_period.count(),
            1)

    def test_registered_transportcompanies_all(self):
        past = datetime.datetime.now() - datetime.timedelta(days=100)
        company = test_factories.CompanyFactory(status=Company.STATUS_REJECTED)
        test_factories.TransportCompanyFactory(company=company, created=past)
        test_factories.TransportCompanyFactory(created=past)
        test_factories.TransportCompanyFactory()
        report = Report()
        report._get_period_init_end()

        report._registered_transportcompanies_all()

        self.assertEqual(
            report.registered_transportcompanies_all.count(),
            1)

    def test_published_quotes(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()

        report._published_quotes()

        self.assertIsNotNone(report.published_quotes)

    def test_published_freights_spot(self):
        test_factories.FreightFactory()
        test_factories.FreightFactory(status=Freight.STATUS_APPROVED)
        test_factories.FreightFactory(status=Freight.STATUS_APPROVED,
                                      is_tender=True)
        test_factories.FreightFactory(status=Freight.STATUS_FINISHED)
        test_factories.FreightFactory(status=Freight.STATUS_REJECTED)

        report = Report()
        report._get_period_init_end()
        report._published_freights()

        report._published_freights_spot()

        self.assertEqual(
            report.published_freights_spot.count(),
            3)

    def test_published_freights_tender(self):
        test_factories.FreightFactory(status=Freight.STATUS_APPROVED)
        test_factories.FreightFactory(status=Freight.STATUS_APPROVED,
                                      is_tender=True)
        test_factories.FreightFactory(status=Freight.STATUS_FINISHED)
        test_factories.FreightFactory(status=Freight.STATUS_REJECTED)
        test_factories.FreightFactory()

        report = Report()
        report._get_period_init_end()
        report._published_freights()

        report._published_freights_tender()

        self.assertEqual(
            report.published_freights_tender.count(),
            1)

    def test_published_freights_spot_by_coverage(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()

        report._published_freights_spot_by_coverage()

        self.assertIsNotNone(report.published_freights_spot_by_coverage)

    def test_published_freights_tender_by_coverage(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()

        report._published_freights_tender_by_coverage()

        self.assertIsNotNone(report.published_freights_tender_by_coverage)

    def test_published_quotes_and_freight_count(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()
        report._published_freights_spot()
        report._published_quotes_by_spot()
        report._published_freights_tender()
        report._published_quotes_by_tender()

        report._published_quotes_count_by_spot()
        report._published_quotes_count_by_tender()
        report._published_freights_count_by_spot()
        report._published_freights_count_by_tender()

        self.assertIsNotNone(report.published_quotes_count_by_spot)
        self.assertIsNotNone(report.published_quotes_count_by_tender)
        self.assertIsNotNone(report.published_freights_count_by_spot)
        self.assertIsNotNone(report.published_freights_count_by_tender)

    def test_published_quotes_by_spot(self):
        freight = test_factories.FreightFactory(status=Freight.STATUS_APPROVED)
        test_factories.QuoteFactory()
        test_factories.QuoteFactory(freight=freight,
                                    status=Quote.STATUS_APPROVED)
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()
        report._published_quotes()

        report._published_quotes_by_spot()

        self.assertEqual(report.published_quotes_by_spot.count(), 2)

    def test_published_quotes_by_tender(self):
        freight = test_factories.FreightFactory(status=Freight.STATUS_APPROVED,
                                                is_tender=True)
        test_factories.QuoteFactory(freight=freight,
                                    status=Quote.STATUS_APPROVED)
        test_factories.QuoteFactory()
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()
        report._published_quotes()
        report._published_quotes_by_tender()

        report._published_quotes_by_tender()

        self.assertEqual(report.published_quotes_by_tender.count(), 1)

    def test_allotted_quotes_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()
        report._published_quotes()
        report._published_quotes_by_spot()

        report._allotted_quotes_by_spot()

        self.assertIsNotNone(report.allotted_quotes_by_spot)

    def test_allotted_quotes_by_tender(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()
        report._published_quotes()
        report._published_quotes_by_tender()

        report._allotted_quotes_by_tender()

        self.assertIsNotNone(report.allotted_quotes_by_tender)

    def test_price_per_origin_destination_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()

        report._price_per_origin_destination_by_spot()

        self.assertIsNotNone(report.price_per_origin_destination_by_spot)

    def test_truck_count_all(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()

        report._truck_count_all()

        self.assertIsNotNone(report.truck_count)

    def test_truck_count_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()

        report._truck_count_by_spot()

        self.assertIsNotNone(report.truck_count_by_spot)

    def test_truck_count_by_tender(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()

        report._truck_count_by_tender()

        self.assertIsNotNone(report.truck_count_by_tender)

    def test_published_freights(self):
        report = Report()
        report._get_period_init_end()

        report._published_freights()

        self.assertIsNotNone(report._published_freights)

    def test_published_freights_count(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()

        report._published_freights_count()

        self.assertIsNotNone(report.published_freights_count)

    def test_published_freights_count_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()

        report._published_freights_count_by_spot()

        self.assertIsNotNone(report.published_freights_count_by_spot)

    def test_published_freights_count_by_tender(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()

        report._published_freights_count_by_tender()

        self.assertIsNotNone(report.published_freights_count_by_tender)

    def test_published_quotes_count(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()

        report._published_quotes_count()

        self.assertIsNotNone(report.published_quotes_count)

    def test_allotted_quotes_count(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()
        report._allotted_quotes()

        report._allotted_quotes_count()

        self.assertIsNotNone(report.allotted_quotes_count)

    def test_allotted_quotes_count_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_spot()
        report._published_quotes()
        report._published_quotes_by_spot()
        report._allotted_quotes()
        report._allotted_quotes_by_spot()

        report._allotted_quotes_count_by_spot()

        self.assertIsNotNone(report.allotted_quotes_count_by_spot)

    def test_allotted_quotes_count_by_tender(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_freights_tender()
        report._published_quotes()
        report._published_quotes_by_tender()
        report._allotted_quotes()
        report._allotted_quotes_by_tender()

        report._allotted_quotes_count_by_tender()

        self.assertIsNotNone(report.allotted_quotes_count_by_tender)

    def test_quotes_per_freight_count_all(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()

        report._quotes_per_freight_count_all()

        self.assertIsNotNone(report.quotes_per_freight_count)

    def test_quotes_per_freight_count_by_spot(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()
        report._published_freights_spot()
        report._published_quotes_by_spot()
        report._published_quotes_count_by_spot()

        report._quotes_per_freight_count_by_spot()

        self.assertIsNotNone(report.quotes_per_freight_count_by_spot)

    def test_quotes_per_freight_count_by_tender(self):
        report = Report()
        report._get_period_init_end()
        report._published_freights()
        report._published_quotes()
        report._published_freights_tender()
        report._published_quotes_by_tender()
        report._published_quotes_count_by_tender()

        report._quotes_per_freight_count_by_tender()

        self.assertIsNotNone(report.quotes_per_freight_count_by_tender)

# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Report'
        db.create_table(u'reports_report', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('period', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('published_freights_count', self.gf('django.db.models.fields.IntegerField')()),
            ('published_freights_count_by_spot', self.gf('django.db.models.fields.IntegerField')()),
            ('published_freights_count_by_tender', self.gf('django.db.models.fields.IntegerField')()),
            ('company_count', self.gf('django.db.models.fields.IntegerField')()),
            ('published_quotes_count', self.gf('django.db.models.fields.IntegerField')()),
            ('published_quotes_count_by_spot', self.gf('django.db.models.fields.IntegerField')()),
            ('published_quotes_count_by_tender', self.gf('django.db.models.fields.IntegerField')()),
            ('quotes_per_freight_count', self.gf('django.db.models.fields.FloatField')()),
            ('quotes_per_freight_count_by_spot', self.gf('django.db.models.fields.FloatField')()),
            ('quotes_per_freight_count_by_tender', self.gf('django.db.models.fields.FloatField')()),
            ('allotted_quotes_count', self.gf('django.db.models.fields.IntegerField')()),
            ('allotted_quotes_count_by_spot', self.gf('django.db.models.fields.IntegerField')()),
            ('allotted_quotes_count_by_tender', self.gf('django.db.models.fields.IntegerField')()),
            ('truck_count', self.gf('django.db.models.fields.IntegerField')()),
            ('truck_count_by_spot', self.gf('django.db.models.fields.IntegerField')()),
            ('truck_count_by_tender', self.gf('django.db.models.fields.IntegerField')()),
            ('registered_freightcompanies_all_count', self.gf('django.db.models.fields.IntegerField')()),
            ('registered_freightcompanies_period_count', self.gf('django.db.models.fields.IntegerField')()),
            ('registered_transportcompanies_all_count', self.gf('django.db.models.fields.IntegerField')()),
            ('registered_transportcompanies_period_count', self.gf('django.db.models.fields.IntegerField')()),
            ('freights_per_origin_destination_json_by_spot', self.gf('django.db.models.fields.TextField')()),
            ('freights_per_origin_destination_json_by_tender', self.gf('django.db.models.fields.TextField')()),
            ('prices_per_origin_destination_json_by_spot', self.gf('django.db.models.fields.TextField')()),
            ('prices_per_origin_destination_json_by_tender', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'reports', ['Report'])

        # Adding unique constraint on 'Report', fields ['start_date', 'end_date']
        db.create_unique(u'reports_report', ['start_date', 'end_date'])


    def backwards(self, orm):
        # Removing unique constraint on 'Report', fields ['start_date', 'end_date']
        db.delete_unique(u'reports_report', ['start_date', 'end_date'])

        # Deleting model 'Report'
        db.delete_table(u'reports_report')


    models = {
        u'reports.report': {
            'Meta': {'unique_together': "(('start_date', 'end_date'),)", 'object_name': 'Report'},
            'allotted_quotes_count': ('django.db.models.fields.IntegerField', [], {}),
            'allotted_quotes_count_by_spot': ('django.db.models.fields.IntegerField', [], {}),
            'allotted_quotes_count_by_tender': ('django.db.models.fields.IntegerField', [], {}),
            'company_count': ('django.db.models.fields.IntegerField', [], {}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            'freights_per_origin_destination_json_by_spot': ('django.db.models.fields.TextField', [], {}),
            'freights_per_origin_destination_json_by_tender': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'period': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'prices_per_origin_destination_json_by_spot': ('django.db.models.fields.TextField', [], {}),
            'prices_per_origin_destination_json_by_tender': ('django.db.models.fields.TextField', [], {}),
            'published_freights_count': ('django.db.models.fields.IntegerField', [], {}),
            'published_freights_count_by_spot': ('django.db.models.fields.IntegerField', [], {}),
            'published_freights_count_by_tender': ('django.db.models.fields.IntegerField', [], {}),
            'published_quotes_count': ('django.db.models.fields.IntegerField', [], {}),
            'published_quotes_count_by_spot': ('django.db.models.fields.IntegerField', [], {}),
            'published_quotes_count_by_tender': ('django.db.models.fields.IntegerField', [], {}),
            'quotes_per_freight_count': ('django.db.models.fields.FloatField', [], {}),
            'quotes_per_freight_count_by_spot': ('django.db.models.fields.FloatField', [], {}),
            'quotes_per_freight_count_by_tender': ('django.db.models.fields.FloatField', [], {}),
            'registered_freightcompanies_all_count': ('django.db.models.fields.IntegerField', [], {}),
            'registered_freightcompanies_period_count': ('django.db.models.fields.IntegerField', [], {}),
            'registered_transportcompanies_all_count': ('django.db.models.fields.IntegerField', [], {}),
            'registered_transportcompanies_period_count': ('django.db.models.fields.IntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'truck_count': ('django.db.models.fields.IntegerField', [], {}),
            'truck_count_by_spot': ('django.db.models.fields.IntegerField', [], {}),
            'truck_count_by_tender': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['reports']
# encoding: utf-8

from __future__ import division

import json

from django.db import models

from collections import defaultdict

from tucargaapi.apps.directory.models import (
    Company, Freight, FreightCompany, Quote, TransportCompany)


class Report(models.Model):

    DEFAULT_PERIOD = 'week'
    DEFAULT_PERIODS_BEFORE = 0
    PERIOD_WEEK = DEFAULT_PERIOD
    PERIOD_MONTH = 'month'
    PERIOD_CHOICES = (
        (PERIOD_WEEK, 'Semanal'),
        (PERIOD_MONTH, 'Mensual'),
        )

    _COMPANY_VALID_STATUS = (Company.STATUS_APPROVED,
                             Company.STATUS_NEW)
    _FREIGHT_VALID_STATUS = (Freight.STATUS_NEW,
                             Freight.STATUS_APPROVED,
                             Freight.STATUS_FINISHED)
    _QUOTE_VALID_STATUS = (Quote.STATUS_NEW,
                           Quote.STATUS_APPROVED, )

    number = models.IntegerField('Semana')
    period = models.CharField(max_length=10, choices=PERIOD_CHOICES)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    published_freights_count = models.IntegerField('Cargas publicadas')
    published_freights_count_by_spot = models.IntegerField('Cargas publicadas')
    published_freights_count_by_tender = \
        models.IntegerField('Cargas publicadas')

    company_count = models.IntegerField('Empresas que publicaron')

    published_quotes_count = models.IntegerField('Cotizaciones publicadas')
    published_quotes_count_by_spot = \
        models.IntegerField('Cotizaciones publicadas')
    published_quotes_count_by_tender = \
        models.IntegerField('Cotizaciones publicadas')

    quotes_per_freight_count = models.FloatField(
        'Cotizaciones por publicación')
    quotes_per_freight_count_by_spot = models.FloatField(
        'Cotizaciones por publicación spot')
    quotes_per_freight_count_by_tender = models.FloatField(
        'Cotizaciones por publicación licitación')

    allotted_quotes_count = models.IntegerField('Cotizaciones adjudicadas')
    allotted_quotes_count_by_spot = \
        models.IntegerField('Cotizaciones adjudicadas')
    allotted_quotes_count_by_tender = \
        models.IntegerField('Cotizaciones adjudicadas')

    truck_count = models.IntegerField('Fletes')
    truck_count_by_spot = models.IntegerField('Fletes')
    truck_count_by_tender = models.IntegerField('Fletes')

    registered_freightcompanies_all_count = models.IntegerField(
        'Total empresas registradas')
    registered_freightcompanies_period_count = models.IntegerField(
        'Empresas registradas en el periodo')
    registered_transportcompanies_all_count = models.IntegerField(
        'Total empresas transporte registradas')
    registered_transportcompanies_period_count = models.IntegerField(
        'Empresas transporte registradas en el periodo')

    freights_per_origin_destination_json_by_spot = models.TextField()
    freights_per_origin_destination_json_by_tender = models.TextField()
    prices_per_origin_destination_json_by_spot = models.TextField()
    prices_per_origin_destination_json_by_tender = models.TextField()

    class Meta:
        unique_together = ('start_date', 'end_date')

    def _as_dict(self, data):
        dict_data = json.loads(data)
        if dict_data['header']:
            return dict_data

    def freights_per_origin_destination_by_spot_as_dict(self):
        result = self._as_dict(
            self.freights_per_origin_destination_json_by_spot)
        return result

    def prices_per_origin_destination_by_spot_as_dict(self):
        result = self._as_dict(
            self.prices_per_origin_destination_json_by_spot)
        return result

    def freights_per_origin_destination_by_tender_as_dict(self):
        result = self._as_dict(
            self.freights_per_origin_destination_json_by_tender)
        return result

    def prices_per_origin_destination_by_tender_as_dict(self):
        result = self._as_dict(
            self.prices_per_origin_destination_json_by_tender)
        return result

    def _registered_freightcompanies_period(self):
        """ Sets freight companies created in a period"""
        self.registered_freightcompanies_period = \
            self._model_records_each_period(
                FreightCompany,
                company__status__in=Report._COMPANY_VALID_STATUS
            )

    def _registered_freightcompanies_all(self):
        """ Sets all freight companies created"""
        self.registered_freightcompanies_all = FreightCompany.objects.filter(
            company__status__in=Report._COMPANY_VALID_STATUS,
            created__lt=self.start_date)

    def _registered_transportcompanies_period(self):
        """ Sets transport companies created in a period"""
        self.registered_transportcompanies_period = \
            self._model_records_each_period(
                TransportCompany,
                company__status__in=Report._COMPANY_VALID_STATUS
            )

    def _registered_transportcompanies_all(self):
        """ Sets all transport companies"""
        self.registered_transportcompanies_all = \
            TransportCompany.objects.filter(
                company__status__in=Report._COMPANY_VALID_STATUS,
                created__lt=self.start_date)

    def _published_freights(self):
        self.published_freights = self._model_records_each_period(
            Freight,
            status__in=Report._FREIGHT_VALID_STATUS)

    def _published_freights_spot(self):
        """ Sets spot freights created in a period"""
        self.published_freights_spot = \
            self.published_freights.filter(is_tender=False)

    def _published_freights_tender(self):
        """ Sets tender freights created in a period"""
        self.published_freights_tender = \
            self.published_freights.filter(is_tender=True)

    def _published_quotes(self):
        self.published_quotes = Quote.objects.filter(
            freight__in=self.published_freights,
            status__in=Report._QUOTE_VALID_STATUS)

    def _published_quotes_by_spot(self):
        self.published_quotes_by_spot = self.published_quotes.filter(
            freight__in=self.published_freights_spot)

    def _published_quotes_by_tender(self):
        self.published_quotes_by_tender = self.published_quotes.filter(
            freight__in=self.published_freights_tender)

    def _published_quotes_count(self):
        self.published_quotes_count = \
            self.published_quotes.count()

    def _published_quotes_count_by_spot(self):
        self.published_quotes_count_by_spot = \
            self.published_quotes_by_spot.count()

    def _published_quotes_count_by_tender(self):
        self.published_quotes_count_by_tender = \
            self.published_quotes_by_tender.count()

    def _published_freights_count(self):
        self.published_freights_count = \
            self.published_freights.count()

    def _published_freights_count_by_spot(self):
        self.published_freights_count_by_spot = \
            self.published_freights_spot.count()

    def _published_freights_count_by_tender(self):
        self.published_freights_count_by_tender = \
            self.published_freights_tender.count()

    def _quotes_per_freight_count(self, published_freights_count,
                                  published_quotes_count):
        result = 0
        if published_freights_count:
            result = published_quotes_count / \
                published_freights_count
        return result

    def _quotes_per_freight_count_all(self):
        self.quotes_per_freight_count = self._quotes_per_freight_count(
            self.published_freights_count,
            self.published_quotes_count)

    def _quotes_per_freight_count_by_spot(self):
        self.quotes_per_freight_count_by_spot = self._quotes_per_freight_count(
            self.published_freights_count_by_spot,
            self.published_quotes_count_by_spot)

    def _quotes_per_freight_count_by_tender(self):
        self.quotes_per_freight_count_by_tender = \
            self._quotes_per_freight_count(
                self.published_freights_count_by_tender,
                self.published_quotes_count_by_tender)

    def _allotted_quotes(self):
        self.allotted_quotes = self.published_quotes.filter(allotted=True)

    def _allotted_quotes_by_spot(self):
        self.allotted_quotes_by_spot = self.published_quotes_by_spot.filter(
            allotted=True)

    def _allotted_quotes_by_tender(self):
        self.allotted_quotes_by_tender = \
            self.published_quotes_by_tender.filter(allotted=True)

    def _allotted_quotes_count(self):
        self.allotted_quotes_count = self.allotted_quotes.count()

    def _allotted_quotes_count_by_spot(self):
        self.allotted_quotes_count_by_spot = \
            self.allotted_quotes_by_spot.count()

    def _allotted_quotes_count_by_tender(self):
        self.allotted_quotes_count_by_tender = \
            self.allotted_quotes_by_tender.count()

    def _truck_count(self, published_freights):
        c = 0
        for freight in published_freights:
            c += freight.freightdetail_set.all()[0].units
        return c

    def _truck_count_all(self):
        self.truck_count = self._truck_count(
            self.published_freights)

    def _truck_count_by_spot(self):
        self.truck_count_by_spot = self._truck_count(
            self.published_freights_spot)

    def _truck_count_by_tender(self):
        self.truck_count_by_tender = self._truck_count(
            self.published_freights_tender)

    def _published_freights_by_coverage(self, published_freights):
        zones_dict = defaultdict(list)
        companies = []

        for f in published_freights:
            o_region = f.get_origin_region()
            d_region = f.get_destination_region()
            location = (o_region, d_region)
            company = f.userdirectory.company

            zones_dict[location].append(company)
            companies.append(company)

        companies = set(companies)

        zones = []
        for zone in zones_dict:
            items = [zone[0], zone[1]]
            items.append(len(zones_dict[zone]))
            counts = []
            for company in companies:
                occurrences = zones_dict[zone].count(company)
                counts.append(occurrences)
            items.extend(counts)
            zones.append(items)
        companies = [c.name for c in companies]
        results = {
            'header': companies,
            'body': zones,
        }

        return results

    def _published_freights_spot_by_coverage(self):
        self.published_freights_spot_by_coverage = \
            self._published_freights_by_coverage(self.published_freights_spot)

    def _published_freights_tender_by_coverage(self):
        self.published_freights_tender_by_coverage = \
            self._published_freights_by_coverage(
                self.published_freights_tender)

    def _price_per_origin_destination(self, published_freights):
        zones = defaultdict(list)
        companies = []
        for f in published_freights:
            o_region = f.get_origin_region()
            d_region = f.get_destination_region()
            prices = [
                (q.userdirectory.company, q.truck_unit_price, q.allotted)
                for q in f.quote_set.filter(
                    status__in=Report._QUOTE_VALID_STATUS)]
            zones[(o_region, d_region)].extend(prices)
            companies.extend([price[0] for price in prices])
        companies = set(companies)

        zones_list = []
        for k, v in zones.items():
            items = [k[0], k[1]]
            prices_dict = defaultdict(list)
            for zone in zones[k]:
                prices_dict[zone[0]].append((zone[1], zone[2]))

            for company in companies:
                items.append(prices_dict.get(company, '-'))
            zones_list.append(items)
        results = {
            'header': companies,
            'body': zones_list,
        }

        return results

    def _price_per_origin_destination_by_spot(self):
        self.price_per_origin_destination_by_spot = \
            self._price_per_origin_destination(
                self.published_freights_spot)

    def _price_per_origin_destination_by_tender(self):
        self.price_per_origin_destination_by_tender = \
            self._price_per_origin_destination(
                self.published_freights_tender)

    def _company_count(self):
        companies = []
        for freight in self.published_freights:
            companies.append(freight.userdirectory.company)
        self.company_count = len(set(companies))

    def _get_period_init_end(self, period=None, periods_before=None):
        self.period = period or Report.DEFAULT_PERIOD
        self.periods_before = \
            int(periods_before or Report.DEFAULT_PERIODS_BEFORE)
        FRAME_METHOD = {
            Report.PERIOD_WEEK: Report._get_week_init_end,
            Report.PERIOD_MONTH: Report._get_month_init_end,
            }
        init, end = FRAME_METHOD[self.period](self.periods_before)
        self.start_date = init
        self.end_date = end

    def _model_records_each_period(self, model, **kwargs):
        return model.objects.filter(
            created__gte=self.start_date,
            created__lt=self.end_date,
            **kwargs
            )

    def main(self, **kwargs):
        self._get_period_init_end(**kwargs)
        self._published_freights()
        self._published_freights_count()
        self._published_freights_spot()
        self._published_freights_count_by_spot()
        self._published_freights_tender()
        self._published_freights_count_by_tender()

        self._published_quotes()
        self._published_quotes_by_spot()
        self._published_quotes_by_tender()
        self._published_quotes_count()

        self._registered_freightcompanies_period()
        self._registered_freightcompanies_all()
        self._registered_transportcompanies_period()
        self._registered_transportcompanies_all()

        self._allotted_quotes()
        self._allotted_quotes_by_spot()
        self._allotted_quotes_by_tender()

        self._published_freights_spot_by_coverage()
        self._published_freights_tender_by_coverage()

        self._quotes_per_freight_count_all()

        self._price_per_origin_destination_by_spot()
        self._price_per_origin_destination_by_tender()

        self._truck_count_by_spot()
        self._truck_count_by_tender()

        self._company_count()

    def to_model(self):
        if self.period == Report.PERIOD_WEEK:
            self.number = self.start_date.isocalendar()[1]
        else:
            self.number = self.start_date.month

        self._published_quotes_count_by_spot()
        self._published_quotes_count_by_tender()

        self._quotes_per_freight_count_by_spot()
        self._quotes_per_freight_count_by_tender()

        self._truck_count_all()
        self._allotted_quotes_count()
        self._allotted_quotes_count_by_spot()
        self._allotted_quotes_count_by_tender()
        self.registered_freightcompanies_all_count = \
            self.registered_freightcompanies_all.count()
        self.registered_freightcompanies_period_count = \
            self.registered_freightcompanies_period.count()
        self.registered_transportcompanies_all_count = \
            self.registered_transportcompanies_all.count()
        self.registered_transportcompanies_period_count = \
            self.registered_transportcompanies_period.count()

        def default_serializer(obj):
            if isinstance(obj, set):
                return [item.name for item in obj]
            return obj.name

        self.prices_per_origin_destination_json_by_spot = json.dumps(
            self.price_per_origin_destination_by_spot,
            default=default_serializer)
        self.prices_per_origin_destination_json_by_tender = json.dumps(
            self.price_per_origin_destination_by_tender,
            default=default_serializer)

        # rename to published_freights_by_coverage*
        self.freights_per_origin_destination_json_by_spot = json.dumps(
            self.published_freights_spot_by_coverage,
            default=default_serializer)
        self.freights_per_origin_destination_json_by_tender = json.dumps(
            self.published_freights_tender_by_coverage,
            default=default_serializer)

    @classmethod
    def _get_week_init_end(cls, periods_before):
        import datetime
        now = datetime.datetime.now()
        days_a_week = 7
        oneweek = datetime.timedelta(days_a_week)
        next_monday = ((now.day - now.weekday()) + days_a_week)
        end = datetime.datetime(
            now.year, now.month, next_monday, 0, 0)
        init = end - oneweek
        init = init - oneweek * periods_before
        end = end - oneweek * periods_before
        return init, end

    @classmethod
    def _get_month_init_end(cls, periods_before):
        import datetime
        now = datetime.datetime.now()
        end = datetime.datetime(
            now.year, now.month + 1 - periods_before, 1, 0, 0)
        init = datetime.datetime(
            now.year, now.month - periods_before, 1, 0, 0)
        return init, end

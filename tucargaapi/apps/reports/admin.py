from django import forms
from django.conf.urls import patterns, url
from django.contrib import admin
from django.contrib.admin import helpers
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .models import Report
from .tasks import create_report_task


class CreateReportForm(forms.Form):
    period = forms.ChoiceField(help_text='Semanal o Mensual', choices=Report.PERIOD_CHOICES)
    periods_before = forms.IntegerField(
        help_text='Semanas o meses atras. 0 es el mes en curso',
        min_value=0)


class ReportAdmin(admin.ModelAdmin):

    ordering = ['-number']
    list_display = [
        'number',
        'published_freights_count',
        'published_quotes_count',
        'quotes_per_freight_count',
        'company_count',
        'allotted_quotes_count',
        'truck_count',
    ]

    def get_urls(self):
        urls = super(ReportAdmin, self).get_urls()
        my_urls = patterns(
            '',
            url(r'^create-report/$',
                self.admin_site.admin_view(self.create_report_view),
                name='create-report')
        )
        return my_urls + urls

    def create_report_view(self, request):
        if request.method == 'POST':
            form = CreateReportForm(request.POST)
            if form.is_valid():
                create_report_task.delay(**form.cleaned_data)
                return HttpResponseRedirect(reverse('admin:reports_report_changelist'))
        else:
            form = CreateReportForm()

        adminform = helpers.AdminForm(
            form, list([(None, {'fields': form.base_fields})]),
            self.get_prepopulated_fields(request)
        )
        context = {'adminform': adminform,
                   'app_label': Report._meta.app_label,
                   'opts': Report._meta,
        }
        return render(request, 'admin/reports/create_report.html', context)

admin.site.register(Report, ReportAdmin)

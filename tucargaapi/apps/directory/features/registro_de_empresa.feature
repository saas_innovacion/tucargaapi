Feature: Registro de empresa

  Una empresa se puede registrar en la página usando el formulario de registro

  @v1
  Scenario: Un usuario va al formulario de registro
     When Voy a la página de registro
     Then Veo el formulario de registro

  @v1
  Scenario: Un usuario envía el formulario de registro
     When Envío el formulario de registro
       | contact_email | company_business_number |
       | nuevo@et.com  |            11.111.111-1 |
     Then Existe un registro
      And La empresa "11.111.111-1" existe
      And La empresa "11.111.111-1" es nueva
      And El usuario "nuevo@et.com" existe
      And El usuario "nuevo@et.com" pertenece a la empresa de transporte "11.111.111-1"
      And Se envió un correo a "staff@tucarga.cl"
      # el correo es porque hay una empresa nueva
      And Se envió un correo a "nuevo@et.com"
      # el correo es porque se le avisa que será contactado

  @v1
  Scenario: Un usuario envía el formulario de registro
     When Envío el formulario de registro
       | contact_email | company_business_number | company_type |
       | nuevo@et.com  |            11.111.111-1 | trans        |
     Then La empresa de transporte "11.111.111-1" existe

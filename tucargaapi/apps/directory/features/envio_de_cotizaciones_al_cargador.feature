Feature: [Backoffice] se puede modificar el correo que se envía al cargador

  En el formulario de un publiación de carga se puede ver y modificar
  un texto personalizado para incluir en el correo con las
  cotizaciones que se envía al cargador

  @v1
  Scenario: Veo las cotizaciones en el backoffice
    Given Una Publicación aprobada
      And La publicación tiene cotizaciones
      And Inicié sesión en el backoffice
     When Voy a la página de la publicación en el backoffice
     Then Se ven las cotizaciones
    # And Se puede editar un texto para incluir en el mail a al cargador con las cotizaciones

  @v2
  Scenario: Envío las cotizaciones desde el backoffice
    Given El usuario "juan@ec.com" pertenece a la empresa de transporte "11.111.111-1"
      And La empresa "11.111.111-1" hizo una publicación
      And La Publicación esta aprobada
      # la empresa 2 cotiz'o
      And Inicié sesión en el backoffice
     When Selecciono "enviar cotizaciones al cargador" en la Publicación en el backoffice
      And Guardo la Publicación en el backoffice
     Then Se envió un correo a "juan@ec.com"
      And El mensaje personalizado va incluido en el correo a "juan@ec.com"
      And El checkbox "Enviar email a empresas de transporte" no esta seleccionado
      And El checkbox "El email a las empesas de transporte ya fue enviado" esta seleccionado

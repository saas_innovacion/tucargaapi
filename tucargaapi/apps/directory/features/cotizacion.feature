Feature: El transportista puede cotizar
  
  Cuando el transportista abre el link para cotizar puede ver el
  detalle de la publicación y un formulario para cotizar. Cuando
  cotiza los contactos de la empresa reciben un email con la
  cotización.

  @v1
  Scenario: Hay una publicación para cotizar
    Given Una Publicación aprobada
      And El usuario "transportista@et.com" pertenece a la empresa "11.111.111-1"
     When El usuario "transportista@et.com" abre el link para cotizar
     Then Se ve un resumen de la Publicación
      And Se ve un formulario para cotizar

  @v1
  Scenario: Se cotiza una publicación
    Given Una Publicación aprobada
      And El usuario "transportista@et.com" pertenece a la empresa "11.111.111-1"
     When El usuario "transportista@et.com" envía el formulario de cotización
     Then Se envió un correo a "staff@tucarga.cl"
        # de la cotizacion enviada
      And Se envió un correo a "transportista@et.com"
        # confirmado el precio cotizado

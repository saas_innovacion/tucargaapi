Feature: El cargador puede adjudicar una empresa

  El cargador puede ver un resumen de la publicación y un formulario
  con las cotizaciones y elegir una o rechazarlas

  @v2
  Scenario: El transportista ve las cotizaciones que se han hecho
    Given Publicación con cotizaciones
     When Voy a la página para elegir precio
     Then Se ve un resumen de la Publicación
      And Se ven las ofertas de precio
      And Se ve un formulario para elegir precio

  @v2
  Scenario: El transportista elige un precio
    Given Publicación con cotizaciones
     When Se envía el formulario para adjudicar empresa
     Then Se envió un correo a "staff@tucarga.cl"
        # con la selección del cargador
      And Se envió un correo a "transportista@adjudicado.com"
        # indicando que su cotizacion fue aceptada. Con copian al cliente y su informacion de contacto
      And Se envió un correo a "transportista@noadjudicado.com"
        # indicando que la cotizacion fue adjudicada a otra empresa

Feature: Se pueden validar las cotizaciones en el backoffice

  @v1
  Scenario: Detalle de una cotización
    Given Una Cotización
      And Inicié sesión en el backoffice
     When En el backoffice voy a la página de una cotización
     Then Se ve un campo de estatus

  @v1
  Scenario: Valido la Cotización
    Given Una Cotización
     When Selecciono el estado a "aprobada" para cotizacion en el backoffice
      And Guardo la cotización en el backoffice
     Then El estatus es aprobada

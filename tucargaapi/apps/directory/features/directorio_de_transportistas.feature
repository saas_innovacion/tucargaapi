Feature: Directorio de empresas de transporte

  Se puede ver una página con un directorio de las empresas de
  transporte válidas.  Las empresas se pueden filtrar por el origen y
  destino de sus rutas y por el tipo de carga que transportan.

  @v1
  Scenario: Un usuario ve el directorio
    Given La empresa de transporte rut "22.222.222-2"
      And La empresa de transporte rut "33.333.333-3"
     When Voy a la página del directorio
     Then Veo una lista con las empresas de transporte
      # And Veo un filtro por rutas
      # And Veo un filtro por tipo de carga

  # Scenario: El usuario filtra por ruta
  #   Given La empresa de transporte rut "22.222.222-2"
  #     And La empresa de transporte rut "33.333.333-3"
  #    When Filtro por ruta ""
  #    Then Veo una lista con las empresas de transporte
  #     And Solo estan las empresas con ruta

  # Scenario: El usuario filtra por tipo de carga
  #   Given La empresa de transporte rut "22.222.222-2"
  #     And La empresa de transporte rut "33.333.333-3"
  #    When Filtro por tipo de carga ""
  #    Then Veo una lista con las empresas de transporte
  #     And Solo estan las empresas con tipo de carga ""

Feature: Publicar una carga

  @wip
  @v1
  Scenario: Un usuario nuevo
    # Given 
     When Envío el formulario de publicacion de carga
       | contact_email   | company_business_number |
       | cargador@ec.com |            11.111.111-1 |
       # Agrego mis datos personales y de empresa
     Then Se creó una publicación de carga
      And Se envió un correo a "cargador@ec.com"
          # estimado hemos recibido tu publicación. Nos contactaremos a la brevedad
      And Se envió un correo a "staff@tucarga.cl"
          # Avisando que se publicó una carga
          # El link lleva al backoffice

  @v1
  Scenario: Un usuario registrado
    Given El usuario "yaregistrado@cargador.com" pertenece a la empresa "11.111.111-1"
     When Envío el formulario de publicacion de carga
       | contact_email             |
       | yaregistrado@cargador.com |
       # Agrego mis datos personales y de empresa
     Then Se creó una publicación de carga
      And Se envió un correo a "yaregistrado@cargador.com"
          # estimado hemos recibido tu publicación. Nos contactaremos a la brevedad
      And Se envió un correo a "staff@tucarga.cl"
          # Avisando que se publicó una carga
          # El link lleva al backoffice

Feature: [Backoffice] se puede validar una publicacion

  @v1
  Scenario: Ver una publicación nueva, empresa nueva
    Given El usuario "juan@ec.com" pertenece a la empresa "11.111.111-1"
      And La empresa "11.111.111-1" hizo una publicación
      And Inicié sesión en el backoffice
     When Voy a la página de la publicación en el backoffice
     Then Se ve la publicación
      And Se ve que la empresa no es validada

  @v1
  Scenario: Aprobar una publicación nueva
    Given El usuario "juan@ec.com" pertenece a la empresa "11.111.111-1"
      And La empresa "11.111.111-1" hizo una publicación
      And Inicié sesión en el backoffice
     When Selecciono estado "aprobada" para la publicación en el backoffice
      And Guardo la Publicación en el backoffice
     Then Se envió un correo a "juan@ec.com"
        # detallando la publicación que hice con numero de publicacion P1045

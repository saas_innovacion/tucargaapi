Feature: [Backoffice] Se puede seleccionar la empresas de transporte que cotizarán

  En el backoffice se puede ver una lista de las empresas de
  transporte para seleccionar las que se quiere que reciban la
  publicación por correo. Tambien se puede modificar el contenido del
  correo que se va a enviar a los usuarios de las empresas de
  transporte

  @v1
  Scenario: Al ver una Publicación en el backoffice veo los detalles para el correo de cotizar
    Given El usuario "juan@ec.com" pertenece a la empresa "11.111.111-1"
    Given La empresa "11.111.111-1" hizo una publicación
      And Inicié sesión en el backoffice
     When Voy a la página de la publicación en el backoffice
     Then Se puede seleccionar las empresa de transporte
    # And Se puede editar un texto para el email de publicacion de carga a empresas de transporte
      And El checkbox "El email a las empresas de transporte ya fue enviado" no esta seleccionado

  @v1
  Scenario: Envio las cotizaciones desde el backoffice
    Given El usuario "juan@ec.com" pertenece a la empresa "11.111.111-1"
    Given La empresa "11.111.111-1" hizo una publicación
      And La Publicación esta aprobada
      And Inicié sesión en el backoffice
      And El usuario "juan@et.com" pertenece a la empresa de transporte "22.222.222-2"
     When Selecciono la empresa de transporte "22.222.222-2" para ser contactada
      And Selecciono "Enviar email a empresas de transporte" en la Publicación
    # And Agrego un mensage personalizado para los transportistas
      And Guardo la Publicación en el backoffice
     Then Se envió un correo a "juan@et.com"
    # And El mensaje personalizado va incluido en el correo a "juan@et.com"
      And El checkbox "Enviar email a empresas de transporte" no esta seleccionado
      And El checkbox "El email a las empesas de transporte ya fue enviado" esta seleccionado

# coding: utf-8
import mock

from django.contrib.auth import get_user_model
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client

from behave import given, when, then
from django_nopassword.models import LoginCode
from rest_framework import status

from tucargaapi.apps.directory.models import Freight, \
    CompanyAndUserRegistration, Company, UserDirectory, TransportCompany
from tucargaapi.apps.directory.test_factories import FreightFactory, \
    QuoteFactory, UserFactory, TransportCompanyFactory, CompanyFactory, \
    UserDirectoryFactory, ContainerTypeFactory, FreightTypeFactory, \
    QuoteDataFactory
from tucargaapi.apps.directory.forms import QuoteForm


User = get_user_model()
test_case = TestCase('__init__')
client = Client()


def check_email_to(to):
    for m in mail.outbox:
        if to in m.to:
            return m


def check_email_body(body, text):
    if text in body:
        return True


def is_text_in_content(selector, text, content):
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(content)
    element = soup.find(id=selector)
    return text in str(element)


@given(u'Publicación con cotizaciones')  # noqa
def impl(context):
    assert False


@when(u'Voy a la página para elegir precio')  # noqa
def impl(context):
    assert False


@then(u'Se ve un resumen de la Publicación')  # noqa
def impl(context):
    test_case.assertIn('object', context.response.context)
    test_case.assertIsInstance(context.response.context['object'], Freight)


@then(u'Se ven las ofertas de precio')  # noqa
def impl(context):
    assert False


@then(u'Se ve un formulario para elegir precio')  # noqa
def impl(context):
    assert False


@when(u'Se envía el formulario para adjudicar empresa')  # noqa
def impl(context):
    assert False


@then(u'Se envió un correo a "{email}"')  # noqa
def impl(context, email):
    context.last_email = check_email_to(email)
    test_case.assertIsNotNone(context.last_email)


@when(u'El Usuario "{email}" abre el link para cotizar')  # noqa
def impl(context, email):
    url = reverse('quote', args=(context.freight.pk,))
    login_code = LoginCode.create_code_for_user(context.users[email], next=url)
    context.response = client.get(login_code.login_url(), follow=True)
    test_case.assertEqual(context.response.status_code, status.HTTP_200_OK)


@then(u'Se ve un formulario para cotizar')  # noqa
def impl(context):
    test_case.assertIn('form', context.response.context)
    test_case.assertIsInstance(context.response.context['form'], QuoteForm)


@when(u'El usuario "{email}" envía el formulario de cotización')  # noqa
@mock.patch('tucargaapi.apps.directory.views.QuoteView.get_success_url')
def impl(context, mock_get_success_url, email=None):
    mock_get_success_url.return_value = '/'
    url = reverse('quote', kwargs={'pk': context.freight.pk})
    login_code = LoginCode.create_code_for_user(context.users[email])
    client.login(email=email, code=login_code.code)
    data = QuoteDataFactory()

    response = client.post(url, data=data)
    test_case.assertEqual(
        response.status_code,
        status.HTTP_302_FOUND,
        u'status code is not 201.\n status_code: %s\n response:\n%s' % (response.status_code, response.content.decode('utf-8')))


@when(u'Voy a la página de la publicacion')  # noqa
def impl(context):
    assert False


@then(u'Se ven las cotizaciones')  # noqa
def impl(context):
    selector = 'quote_set-group'
    text = 'quote_set-0'
    result = is_text_in_content(selector, text, context.response.content)
    test_case.assertTrue(result)


@then(u'Se ve el contenido del email de cotizaciones al Cargador')  # noqa
def impl(context):
    assert False


@when(u'Guardo la Publicación en el backoffice')  # noqa
def impl(context):
    context.freight.save()


@when(u'Selecciono "Enviar email a empresas de transporte" en la Publicación')  # noqa
def impl(context):
    context.freight.send_email_to_transport_companies = True


@when(u'Selecciono "Enviar cotizaciones al cargador" en la Publicación en el backoffice')  # noqa
def impl(context):
    assert False


@then(u'El checkbox "Enviar email a empresas de transporte" no esta seleccionado')  # noqa
def impl(context):
    test_case.assertFalse(
        context.freight.send_email_to_transport_companies)


@then(u'El checkbox "El email a las empesas de transporte ya fue enviado" esta seleccionado')  # noqa
def impl(context):
    test_case.assertTrue(
        context.freight.email_to_transport_companies_sent)


@then(u'El checkbox "El email a las empresas de transporte ya fue enviado" no esta seleccionado')  # noqa
def impl(context):
    test_case.assertFalse(
        context.freight.email_to_transport_companies_sent)


@when(u'Envío el formulario de publicacion de carga')  # noqa
@mock.patch('tucargaapi.apps.directory.models.FreightLocation')  # noqa
@mock.patch('tucargaapi.apps.directory.models.FreightDetail')  # noqa
def impl(context, mock_freightlocation, mock_freightdetail):
    url = reverse('freight-create')
    data = {'contact_email': context.table[0][0]}
    if len(context.table[0]) == 2:
        data['company_business_number'] = context.table[0][1]
    # prefix for every relation ie: FreightWayPoint
    # "origin": context.commune.pk,
    # "destination": context.commune.pk,
    containertype = ContainerTypeFactory()
    freighttype = FreightTypeFactory()
    data.update({"obj_type": Freight.OBJ_TYPE_EMPTY,
                 "reason": "some reason",
                 "company_address_city": "s",
                 "contact_last_name": "l",
                 "freight_type": Freight.OBJ_TYPE_EMPTY,
                 "company_address_number": "1",
                 "company_address_street": "s",
                 "company_address_region": context.region.pk,
                 "company_address_commune": context.commune.pk,
                 "contact_first_name": "f", "service_type": "importation",
                 "company_business_name": "b", "company_address_floor": "f",
                 "company_name": "c",
                 "freightdetail_0_container_type": containertype.pk,
                 "freightdetail_0_obj_type": freighttype.pk,
                 "freightdetail_0_units": 10,
                 "freightdetail_0_weight": 10,
                 })
    response = client.post(url, data=data)
    test_case.assertEqual(
        response.status_code,
        status.HTTP_201_CREATED,
        'status code is not 201.\n status_code: %s\n response:\n%s' % (response.status_code, response))


@given(u'Una Cotización')  # noqa
def impl(context):
    context.quote = QuoteFactory()


@when(u'En el backoffice voy a la página de una cotización')  # noqa
def impl(context):
    url = reverse('admin:directory_quote_change', args=(context.quote.pk,))
    context.response = client.get(url)
    test_case.assertEqual(context.response.status_code, status.HTTP_200_OK)


@then(u'Se ve un campo de estatus')  # noqa
def impl(context):
    test_case.assertIn('id_status', context.response.content)


@then(u'El estatus es aprobada')  # noqa
def impl(context):
    test_case.assertEqual('approved', context.quote.status)


# @given(u'Una Publicación aprobada')  # noqa
# def impl(context):
#     context.freight = FreightFactory(status='approved')


@given(u'Una publicación')  # noqa
def impl(context):
    context.freight = FreightFactory()


@given(u'La Publicación esta aprobada')  # noqa
def impl(context):
    context.freight.status = 'approved'
    context.freight.save()


@given(u'Una publicación aprobada')  # noqa
def impl(context):
    context.execute_steps(u'Given Una publicación')
    context.execute_steps(u'Given La Publicación esta aprobada')


@given(u'La empresa "{business_number}" hizo una publicación')  # noqa
def impl(context, business_number):
    context.execute_steps(u'Given Una empresa rut "%s"' % business_number)
    context.freight = FreightFactory(
        userdirectory=context.userdirectory)


@when(u'Voy a la página de la publicación en el backoffice')  # noqa
def impl(context):
    url = reverse('admin:directory_freight_change', args=(context.freight.pk,))
    context.response = client.get(url)
    test_case.assertEqual(context.response.status_code, status.HTTP_200_OK)


@given(u'Inicié sesión en el backoffice')  # noqa
def impl(context):
    result = client.login(email='admin@tucarga.cl', password='admin')
    assert result, "Login failed"


@then(u'Se ve la publicación')  # noqa
def impl(context):
    test_case.assertEqual(
        context.response.context['original'],
        context.freight)


@then(u'Se ve que la empresa no es validada')  # noqa
def impl(context):
    selector = 'id_userdirectory'
    text = '>%s/' % context.freight.userdirectory.company
    result = is_text_in_content(selector, text,
                                context.response.content)
    test_case.assertTrue(result)


@then(u'Se puede seleccionar las empresa de transporte')  # noqa
def impl(context):
    selector = 'id_contacted_transport_companies'
    text = 'multiple="multiple"'
    result = is_text_in_content(selector, text, context.response.content)
    test_case.assertTrue(result)


@then(u'Se puede editar un texto para el email de publicacion de carga a empresas de transporte')  # noqa
def impl(context):
    selector = 'id_custom_message_to_transport_companies'
    text = '<textarea '
    result = is_text_in_content(selector, text, context.response.content)
    test_case.assertTrue(result)


@then(u'Se creó una publicación de carga')  # noqa
def impl(context):
    Freight.objects.get()


@given(u'La publicación tiene cotizaciones')  # noqa
def impl(context):
    QuoteFactory(freight=context.freight)


@given(u'Una empresa rut "{business_number}"')  # noqa
def impl(context, business_number):
    context.companies[business_number] = \
        CompanyFactory(business_number=business_number)


@given(u'La empresa de transporte rut "{business_number}"')  # noqa
def impl(context, business_number):
    context.execute_steps(u'Given Una empresa rut "%s"' % business_number)
    context.transportcompanies[business_number] = \
        TransportCompanyFactory(company=context.companies[business_number])


@given(u'Un usuario "{email}"')  # noqa
def impl(context, email):
    context.users[email] = UserFactory(email=email)


@given(u'El usuario "{email}" pertenece a la empresa "{business_number}"')  # noqa
def impl(context, business_number, email):
    context.execute_steps(u'Given Un Usuario "%s"' % email)
    context.execute_steps(u'Given Una empresa rut "%s"' % business_number)
    context.userdirectory = UserDirectoryFactory(
        company=context.companies[business_number],
        user=context.users[email])


@given(u'El usuario "{email}" pertenece a la empresa de transporte "{business_number}"')  # noqa
def impl(context, email, business_number):
    context.execute_steps(
        u'Given El usuario "%s" pertenece a la empresa "%s"' %
        (email, business_number))
    context.transportcompanies[business_number] = \
        TransportCompanyFactory(company=context.companies[business_number])


@when(u'Selecciono la empresa de transporte "{business_number}" para ser contactada')  # noqa
def impl(context, business_number):
    context.freight.contacted_transport_companies.add(
        context.transportcompanies[business_number])


@when(u'Selecciono estado "aprobada" para la publicación en el backoffice')  # noqa
def impl(context):
    context.freight.status = 'approved'


@when(u'Selecciono el estado a "aprobada" para cotizacion en el backoffice')  # noqa
def impl(context):
    context.quote.status = 'approved'


@when(u'Guardo la cotización en el backoffice')  # noqa
def impl(context):
    context.quote.save()


@then(u'Se puede editar un texto para incluir en el mail a al cargador con las cotizaciones')  # noqa
def impl(context):
    assert False


@when(u'Agrego un mensage personalizado para los transportistas')
def impl(context):
    context.freight.custom_message_to_transport_companies = 'custom message'


@then(u'El mensaje personalizado va incluido en el correo a "{email}"')
def impl(context, email):
    text = context.freight.custom_message_to_transport_companies
    content = context.last_email.body
    result = check_email_body(content, text)
    test_case.assertTrue(result)


@when(u'Voy a la página del directorio')  # noqa
def impl(context):
   url = reverse('transportcompany-list')
   context.response = client.get(url)
   test_case.assertEqual(context.response.status_code, status.HTTP_200_OK)


@then(u'Veo una lista con las empresas de transporte')  # noqa
def impl(context):
    test_case.assertEqual(2, len(context.response.data))


@when(u'Voy a la página de registro')  # noqa
def impl(context):
    url = reverse('registration')
    context.response = client.get(url)
    test_case.assertEqual(context.response.status_code, status.HTTP_200_OK)


@then(u'Veo el formulario de registro')  # noqa
def impl(context):
    test_case.assertIn('form', context.response.context)


@when(u'Envío el formulario de registro')  # noqa
def impl(context):
    contact_email = context.table[0][0]
    business_number = context.table[0][1]
    url = reverse('registration')
    data = {'company_type': 'trans',
            'company_name': 'c',
            'company_business_name': 'b',
            'company_business_number': business_number,
            'company_address_street': 's',
            'company_address_number': '1',
            'company_address_floor': 'f',
            'company_address_region': context.region.pk,
            'company_address_commune': context.commune.pk,
            'company_address_city': 'Gotham',
            'company_own_trucks': False,
            'company_own_storage': True,
            'contact_first_name': 'f',
            'contact_last_name': 'f',
            'contact_position': 'f',
            'contact_email': contact_email,
            'contact_phone': 'f',
            'contact_mobile': 'f', }

    response = client.post(url, data=data)
    test_case.assertEqual(
        response.status_code,
        status.HTTP_302_FOUND,
        u'status code is not 201.\n status_code: %s\n response:\n%s' % (response.status_code, response.content.decode('utf-8')))


@then(u'Existe un registro')  # noqa
def impl(context):
    test_case.assertTrue(CompanyAndUserRegistration.objects.get())


@then(u'La empresa "{business_number}" existe')  # noqa
def impl(context, business_number):
    test_case.assertTrue(
        Company.objects.get(business_number=business_number))


@then(u'La empresa "{business_number}" es nueva')  # noqa
def impl(context, business_number):
    test_case.assertEqual(
        Company.objects.get(business_number=business_number).status,
        Company.STATUS_CHOICES[0][0])


@then(u'El usuario "{email}" existe')  # noqa
def impl(context, email):
    test_case.assertTrue(User.objects.get(email=email))


@then(u'El usuario "{email}" pertenece a la empresa de transporte "{business_number}"')  # noqa
def impl(context, email, business_number):
    test_case.assertTrue(
        UserDirectory.objects.get(user__email=email,
                                  company__business_number=business_number)
        )


@then(u'La empresa de transporte "{business_number}" existe')  # noqa
def impl(context, business_number):
    test_case.assertTrue(
        TransportCompany.objects.get(company__business_number=business_number)
        )

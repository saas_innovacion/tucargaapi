from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.core import mail
from django.core.management import call_command

from locationstree.models import Location
from locationstree import levels

from tucargaapi.apps.directory.models import Company, Freight, \
    FreightFirstStep, FreightType, TransportCompany


User = get_user_model()


def before_all(context):
    call_command('loaddata', 'chile')
    context.commune = Location.objects.filter(level=levels.CHILE['commune'])[0]
    context.region = Location.objects.filter(level=levels.CHILE['region'])[0]
    Site.objects.create(domain='api.tucarga.cl', name='Tu Carga API')


def before_feature(context, feature):
    context.companies = {}
    context.users = {}
    FreightType.objects.create()
    context.transportcompanies = {}


def before_scenario(context, scenario):
    user, c = User.objects.get_or_create(email='staff@tucarga.cl')
    group, c = Group.objects.get_or_create(name='staff')
    group.user_set.add(user)
    User.objects.create_superuser(
        email='admin@tucarga.cl',
        password='admin')


def after_scenario(context, scenario):
    Freight.objects.all().delete()
    User.objects.all().delete()
    Company.objects.all().delete()
    FreightFirstStep.objects.all().delete()
    TransportCompany.objects.all().delete()
    mail.outbox = []

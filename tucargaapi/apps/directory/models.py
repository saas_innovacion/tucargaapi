# coding=utf-8
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.db import models
from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver

from django_nopassword.models import LoginCode
from model_utils import Choices
from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel
from mptt.models import TreeManyToManyField
# from track_data import track_data
import locationstree.levels

from tucargaapi.apps.directory import utils


class Certification(models.Model):

    name = models.CharField('nombre', max_length=50)
    description = models.TextField('descripción')

    class Meta:
        verbose_name = u'certificación'
        verbose_name_plural = u'certificaciones'

    def __unicode__(self):
        return self.name


class Coverage(models.Model):

    name = models.CharField('nombre', max_length=50, unique=True)
    from_regions = TreeManyToManyField(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='coverage_from_regions',
        verbose_name='de',)
    to_regions = TreeManyToManyField(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='coverage_to_regions',
        verbose_name='a',)

    class Meta:
        verbose_name = u'cobertura'

    def __unicode__(self):
        return self.name


class Company(TimeStampedModel):
    STATUS_NEW = 'new'
    STATUS_APPROVED = 'approved'
    STATUS_REJECTED = 'rejected'

    STATUS_CHOICES = ((STATUS_NEW, 'Nuevo'),
                      (STATUS_APPROVED, 'Aprobada'),
                      (STATUS_REJECTED, 'Rechazada'),
                      )

    PUBLIC_ATTRIBUTES = ['name', 'commune', 'business_number']

    name = models.CharField('nombre', max_length=100)
    business_name = models.CharField('Razón Social', max_length=100)
    business_number = models.CharField('RUT', unique=True, max_length=20)
    year_of_creation = models.IntegerField(
        'Año de creación',
        null=True,
        blank=True)
    phone = models.CharField(
        'teléfono',
        max_length=20,
        null=True,
        blank=True)

    logo = models.ImageField(upload_to='company')

    # address
    address_street = models.CharField(
        'Calle',
        max_length=50,
        null=True,
        blank=True)
    address_number = models.CharField(
        'Número',
        max_length=50,
        null=True,
        blank=True)
    address_floor = models.CharField(
        'Pasaje/Oficina/Piso',
        null=True,
        blank=True,
        max_length=50)
    address_region = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Región',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='company_address_regions',
        )
    address_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Comuna',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='company_address_communes',
        )
    address_city = models.CharField(
        'Ciudad',
        max_length=50,
        null=True,
        blank=True)

    # certifications
    certifications = models.ManyToManyField(
        'Certification',
        verbose_name=Certification._meta.verbose_name,
        null=True,
        blank=True)

    # Internal use
    economic_industry = models.CharField(
        'Rubro económico',
        max_length=100,
        null=True,
        blank=True)
    economic_sector = models.CharField(
        'Sector económico revisado',
        max_length=100,
        null=True,
        blank=True)
    registration_date = models.DateField(
        'fecha de registro en el sitio web',
        null=True,
        blank=True)
    pilot_group = models.CharField('grupo piloto', max_length=20)
    activation_date = models.DateField(
        'fecha de activación',
        null=True,
        blank=True)
    web_site = models.CharField(
        'sitio web',
        max_length=100,
        blank=True)

    status = models.CharField('estado', max_length=100, choices=STATUS_CHOICES,
                              default=STATUS_CHOICES[0][0])

    # imported fields, all are char field to keep what was imported
    # without change
    import_tipo_de_empresa = models.CharField(max_length=200,null=True, blank=True)
    import_nombre_empresa = models.CharField(max_length=200,null=True, blank=True)
    import_razon_social = models.CharField(max_length=200,null=True, blank=True)
    import_rut = models.CharField(max_length=200,null=True, blank=True)
    import_region = models.CharField(max_length=200,null=True, blank=True)
    import_comuna = models.CharField(max_length=200,null=True, blank=True)
    import_direccion = models.CharField(max_length=200,null=True, blank=True)
    import_sector_eco_revisado = models.CharField(max_length=200,null=True, blank=True)
    import_rubro_economico = models.CharField(max_length=200,null=True, blank=True)
    import_tipo_de_carga_que_mueve = models.CharField(max_length=200,null=True, blank=True)
    import_fecha_de_registro_en_el_sitio_web = models.CharField(max_length=200,null=True, blank=True)
    import_ejecutivo_que_lo_visito = models.CharField(max_length=200,null=True, blank=True)
    import_fecha_reunion = models.CharField(max_length=200,null=True, blank=True)
    import_transportistas_con_los_que_trabaja = models.CharField(max_length=200,null=True, blank=True)
    import_clientes = models.CharField(max_length=200,null=True, blank=True)
    import_rutas_frecuentes = models.CharField(max_length=200,null=True, blank=True)
    import_zona_norte = models.CharField(max_length=200,null=True, blank=True)
    import_centro_norte = models.CharField(max_length=200,null=True, blank=True)
    import_centro = models.CharField(max_length=200,null=True, blank=True)
    import_rm = models.CharField(max_length=200,null=True, blank=True)
    import_centro_sur = models.CharField(max_length=200,null=True, blank=True)
    import_zona_sur = models.CharField(max_length=200,null=True, blank=True)
    import_centro_sur_extremo = models.CharField(max_length=200,null=True, blank=True)
    import_sur_extremo = models.CharField(max_length=200,null=True, blank=True)
    import_sitio_web = models.CharField(max_length=200,null=True, blank=True)
    import_contacto_1 = models.CharField(max_length=200,null=True, blank=True)
    import_cargo_1 = models.CharField(max_length=200,null=True, blank=True)
    import_mail_1 = models.CharField(max_length=200,null=True, blank=True)
    import_telefono_1 = models.CharField(max_length=200,null=True, blank=True)
    import_celular_1 = models.CharField(max_length=200,null=True, blank=True)
    import_contacto_2 = models.CharField(max_length=200,null=True, blank=True)
    import_cargo_2 = models.CharField(max_length=200,null=True, blank=True)
    import_mail_2 = models.CharField(max_length=200,null=True, blank=True)
    import_telefono_2 = models.CharField(max_length=200,null=True, blank=True)
    import_celular_2 = models.CharField(max_length=200,null=True, blank=True)
    import_contacto_3 = models.CharField(max_length=200,null=True, blank=True)
    import_cargo_3 = models.CharField(max_length=200,null=True, blank=True)
    import_mail_3 = models.CharField(max_length=200,null=True, blank=True)
    import_telefono_3 = models.CharField(max_length=200,null=True, blank=True)
    import_celular_3 = models.CharField(max_length=200,null=True, blank=True)

    class Meta:
        verbose_name = u'empresa'

    def __unicode__(self):
        return self.name

    def get_logo_url(self):
        if self.logo:
            return self.logo.url
        return ''

    @property
    def address(self):
        return u'{} {}'.format(
            self.address_street or '',
            self.address_number or '').strip()


class Route(models.Model):

    from_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='De',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='route_from_communes',
        )
    to_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='A',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='route_to_communes',
        )

    class Meta:
        verbose_name = u'ruta'

    def __unicode__(self):
        return u'De: %s - A: %s' % (self.from_commune, self.to_commune)


class TransportCompany(TimeStampedModel):
    company = models.OneToOneField(
        'Company',
        verbose_name=Company._meta.verbose_name)

    # operations
    number_of_trips_per_month = models.IntegerField(
        'Número de viajes al mes',
        null=True,
        blank=True)
    full_load_trips_percentage = models.IntegerField(
        'Porcentaje de viajes con camión lleno',
        null=True,
        blank=True)
    LTL_trips_percentage = models.IntegerField(
        'Porcentaje de viajes LTL',
        null=True,
        blank=True)

    freight_types = models.CommaSeparatedIntegerField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='tipos de carga')

    # coverage and routes
    coverage = models.ManyToManyField(
        'Coverage',
        verbose_name=Coverage._meta.verbose_name)
    do_international_trips = models.NullBooleanField('¿Hace viajes internacionales?')
    international_trips_from_1 = models.CharField(max_length=20, null=True,
                                                  blank=True)
    international_trips_to_1 = models.CharField(max_length=20, null=True,
                                                blank=True)
    international_trips_from_2 = models.CharField(max_length=20, null=True,
                                                  blank=True)
    international_trips_to_2 = models.CharField(max_length=20, null=True,
                                                blank=True)
    international_trips_from_3 = models.CharField(max_length=20, null=True,
                                                  blank=True)
    international_trips_to_3 = models.CharField(max_length=20, null=True,
                                                blank=True)

    # empty trips
    empty_trips_percentage = models.IntegerField(
        '¿Qué porcentaje de sus viajes son vacíos?',
        null=True,
        blank=True)
    # plus EmptyRoute

    class Meta:
        verbose_name = u'empresa de transporte'
        verbose_name_plural = u'empresas de transporte'

    def __unicode__(self):
        return u'%s' % (self.company)

    def has_storage(self):
        try:
            self.transportcompanystorage
            return True
        except TransportCompanyStorage.DoesNotExist:
            return False

    def coverage_names(self):
        coverages = []
        for cov in self.coverage.all():
            coverages.append(cov.name)
        return ','.join(coverages)

    def freighttypes_names(self):
        freighttypes = []
        for freight_type in self.freight_types:
            freighttypes.append(
                FreightDetail.OBJ_TYPES_DICT[int(freight_type)])
        return ','.join(freighttypes)


class FreightCompanyType(models.Model):

    name = models.CharField('Nombre', max_length=20, unique=True)

    class Meta:
        verbose_name = u'tipo de empresa generadora de carga'
        verbose_name_plural = u'tipos de empresa generadora de carga'

    def __unicode__(self):
        return u'%s' % (self.name)


class FreightCompany(TimeStampedModel):

    company = models.OneToOneField(
        'Company',
        verbose_name=Company._meta.verbose_name)
    obj_type = models.ForeignKey(
        'FreightCompanyType',
        null=True,
        blank=True,
        verbose_name=FreightCompanyType._meta.verbose_name)

    class Meta:
        verbose_name = u'empresa generadora de carga'
        verbose_name_plural = u'empresas generadoras de carga'

    def __unicode__(self):
        return u'%s' % (self.company.name)


class FreightInsurance(models.Model):

    transportcompany = models.ForeignKey(
        'TransportCompany',
        verbose_name=TransportCompany._meta.verbose_name)
    insurance_company = models.CharField('Compañía de seguros', max_length=50)
    amount = models.IntegerField('Monto de cobertura en UF')
    validity = models.DateField('Vigencia')
    description = models.TextField('Descripción')

    class Meta:
        verbose_name = u'Seguro'


class CompanyLocation(models.Model):

    company = models.ForeignKey(
        'Company',
        verbose_name=Company._meta.verbose_name)
    region = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Región',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='companylocation_regions',
        )
    commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Comuna',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='companylocation_communes',
        )
    address_city = models.CharField('Ciudad', max_length=50)
    name = models.CharField('Nombre', max_length=50)
    phone = models.CharField('Teléfono', max_length=20)

    class Meta:
        verbose_name = u'Ubicación/Oficina'


class Day(models.Model):
    name = models.CharField('Nombre', max_length=20, unique=True)

    class Meta:
        verbose_name = u'dia'
        ordering = ['pk']

    def __unicode__(self):
        return u'%s' % self.name


class EmptyRoute(models.Model):

    transportcompany = models.ForeignKey(
        'TransportCompany',
        verbose_name=TransportCompany._meta.verbose_name)
    route = models.ForeignKey('Route', verbose_name=Route._meta.verbose_name)
    truck_type = models.ForeignKey(
        'TruckType',
        verbose_name='tipo de camión',
        related_name='emptyroute_body_types')
    equipment = models.ManyToManyField(
        'Equipment',
        verbose_name='equipamiento',
        related_name='emptyroute_equipments')
    days = models.ManyToManyField('Day', verbose_name='días')
    frecuency = models.CharField('frecuencia', max_length=50)
    number_of_trucks = models.IntegerField('número de camiones')
    reference_price = models.DecimalField(
        'precio referencial del flete + IVA',
        max_digits=12,
        decimal_places=2)
    reference_price_currency = models.CharField(
        'moneda',
        max_length=3,
        default='CLP')

    class Meta:
        verbose_name = u'ruta vacía'
        verbose_name_plural = u'rutas vacías'


class TransportCompanyEquipment(models.Model):

    transportcompany = models.OneToOneField(
        'TransportCompany',
        verbose_name=TransportCompany._meta.verbose_name)
    truck = models.IntegerField('camión', null=True, blank=True)
    truck_body_types = models.ManyToManyField('TruckType',verbose_name='camión: tipos de carroceía', related_name='transportcompanyequipment_truck_body_types', null=True, blank=True)
    truck_equipment = models.ManyToManyField('Equipment', verbose_name='camión: equipamientos', related_name='transportcompanyequipment_truck_equipments', null=True, blank=True)
    trailer = models.IntegerField('remolque', null=True, blank=True)
    trailer_body_types = models.ManyToManyField('TruckType', verbose_name='remolque: tipos de carroceía', related_name='transportcompanyequipment_trailer_body_types', null=True, blank=True)
    trailer_equipment = models.ManyToManyField('Equipment', verbose_name='remolque: equipamientos', related_name='transportcompanyequipment_trailer_equipments', null=True, blank=True)
    semi_trailer = models.IntegerField('semiremolque', null=True, blank=True)
    semi_trailer_body_types = models.ManyToManyField('TruckType', verbose_name='semiremolque: tipos de carroceía', related_name='transportcompanyequipment_semi_trailer_body_types', null=True, blank=True)
    semi_trailer_equipment = models.ManyToManyField('Equipment', verbose_name='semiremolque: equipamientos', related_name='transportcompanyequipment_semitrailer_equipments', null=True, blank=True)
    tractor = models.IntegerField('tracto camión', null=True, blank=True)
    cargo_van = models.IntegerField('utilitario', null=True, blank=True)
    outsource = models.NullBooleanField('Trabaja con terceros para realizar transporte')
    outsource_quantity = models.IntegerField('Número de empresas con que trabaja', null=True, blank=True)
    outsource_names = models.TextField('Nombre de algunas empresas con las que trabaja', null=True, blank=True)
    own_gps = models.NullBooleanField('¿Su transporte cuenta con GPS?')
    outsource_gps = models.NullBooleanField('¿El transporte que terceriza tiene GPS?')

    class Meta:
        verbose_name = u'equipamiento de empresa de transporte'
        verbose_name = u'equipamientos de empresa de transporte'


class TransportCompanyStorage(models.Model):

    transportcompany = models.OneToOneField('TransportCompany')
    description = models.TextField('Descripción')
    total_m2 = models.IntegerField('m2 total')
    built_m2 = models.IntegerField('m2 construidos')
    capacidad = models.IntegerField('capacidad')
    unit = models.CharField('Unidad', max_length=50)

    # address
    address_region = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Región',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='companystorage_address_regions',
        )
    address_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Comuna',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='companystorage_address_communes',
        )
    address_city = models.CharField('Ciudad', max_length=50)
    address_street = models.CharField('Calle', max_length=50)
    address_number = models.CharField('Número', max_length=50)
    address_floor = models.CharField('Pasaje/Oficina/Piso', max_length=50)

    class Meta:
        verbose_name = u'Almacenaje (tipo y capacidad)'
        verbose_name_plural = u'Almacenajes'


class TruckType(models.Model):

    name = models.CharField('nombre', max_length=100)
    sort_order = models.IntegerField('orden', default=0)

    class Meta:
        verbose_name = u'tipo de camión'
        verbose_name_plural = u'tipos de camión'

    def __unicode__(self):
        return u'%s' % self.name


class Equipment(models.Model):

    name = models.CharField('Nombre', max_length=100)
    sort_order = models.IntegerField('orden', default=0)

    class Meta:
        verbose_name = u'equipamiento'

    def __unicode__(self):
        return u'%s' % self.name


class UserDirectory(TimeStampedModel):
    """This model extends User model to add app specific fields

    """
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name='usuario')

    company = models.ForeignKey(
        'Company',
        verbose_name=Company._meta.verbose_name)
    is_owner = models.BooleanField('es dueño', default=False)
    is_approved = models.BooleanField('está aprobado', default=False)

    # Contact information
    position = models.CharField(
        'Cargo',
        max_length=100,
        null=True,
        blank=True)
    phone = models.CharField(
        'Telefono',
        max_length=50,
        null=True,
        blank=True)
    mobile = models.CharField(
        'Celular',
        max_length=50,
        null=True,
        blank=True)
    companylocation = models.ForeignKey(
        'CompanyLocation',
        null=True,
        blank=True,
        verbose_name=CompanyLocation._meta.verbose_name)

    def __unicode__(self):
        return u'%s/%s' % (self.company, self.user)

    class Meta:
        verbose_name = u'contacto'


# @track_data('status')
class Freight(TimeStampedModel):

    STATUS_NEW = 'new'
    STATUS_APPROVED = 'approved'
    STATUS_REJECTED = 'rejected'
    STATUS_FINISHED = 'finished'

    STATUS = Choices((STATUS_NEW, 'Nueva'),
                     (STATUS_APPROVED, 'Aprobada'),
                     (STATUS_REJECTED, 'Rechazada'),
                     (STATUS_FINISHED, 'Terminada'), )

    OBJ_TYPE_IMPORT = 'impo'
    OBJ_TYPE_EXPORT = 'expo'
    OBJ_TYPE_DOMESTIC = 'dome'
    OBJ_TYPE_EMPTY = 'empt'

    OBJ_TYPE_CHOICES = Choices(
        (OBJ_TYPE_IMPORT, 'Importación'),
        (OBJ_TYPE_EXPORT, 'Exportación'),
        (OBJ_TYPE_DOMESTIC, 'Domestico'),
        (OBJ_TYPE_EMPTY, 'Contenedor Vacío'))

    # Information
    obj_type = StatusField(
        'tipo de servicio',
        choices_name='OBJ_TYPE_CHOICES')
    reason = models.CharField(
        'Motivo',
        max_length=200,
        null=True,
        blank=True)

    # Requirements
    insurance = models.CharField(
        'seguro de carga',
        max_length=50,
        blank=True)
    truck_type = models.ForeignKey(
        'TruckType',
        null=True,
        blank=True,
        verbose_name='tipo de camión',
        related_name='freight_truck_types')
    equipment = models.ForeignKey(
        'Equipment',
        null=True,
        blank=True,
        verbose_name='equipamiento',
        related_name='freight_equipments')
    other = models.CharField(
        'otro',
        max_length=50,
        blank=True)
    needs_storage = models.TextField(
        '¿Necesita almacenaje?',
        blank=True)
    service_conditions = models.TextField(
        'condiciones de servicio y forma de pago',
        blank=True)
    comments = models.TextField(
        'comentarios o información adicional',
        blank=True)

    # Internal use
    send_emails = models.BooleanField(
        'Enviar correos. Desmarcar para que no se envien corres',
        default=True)
    is_tender = models.BooleanField('Es una licitación', default=False)
    userdirectory = models.ForeignKey('UserDirectory', verbose_name='contacto')
    contacted_transport_companies = models.ManyToManyField(
        'TransportCompany',
        verbose_name='empresas de transporte contactadas',
        null=True,
        blank=True)
    custom_message_to_transport_companies = models.TextField(
        'Mensaje personalizado a las empresas de transporte',
        blank=True)
    send_email_to_transport_companies = models.BooleanField(
        'Enviar email a empresas de transporte',
        default=False)
    email_to_transport_companies_sent = models.BooleanField(
        'El email a las empesas de transporte ya fue enviado',
        default=False)
    email_about_publication_approved_sent = models.BooleanField(
        'El email de publicación aprobada fue enviado al cargador',
        default=False)
    status = StatusField('estado')

    class Meta:
        verbose_name = u'carga'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_admin_url(self):
        url = reverse('admin:directory_freight_change', args=(self.id,))
        return url

    def get_absolute_url(self):
        return reverse('freight-detail', kwargs={'pk': self.pk})

    @classmethod
    def create_from_firststep(self, firststep, userdirectory):
        Freight.objects.create(
            obj_type=firststep.obj_type,
            userdirectory=userdirectory)

    def email_transport_companies(self):
        if not self.send_emails:
            return

        site = utils.get_api_site()
        obj_url = reverse('quote', kwargs={'pk': self.pk})
        custom_message = self.custom_message_to_transport_companies
        for company in self.contacted_transport_companies.filter(company__status=Company.STATUS_APPROVED):
            for userdirectory in company.company.userdirectory_set.all():
                user = userdirectory.user
                login_code = LoginCode.create_code_for_user(
                    user=user,
                    next=obj_url)
                url = utils.get_url(site, 'login-with-code', login_code.code)
                context_dict = {
                    user.email: {
                        'FNAME': user.first_name,
                        'LNAME': user.last_name,
                        'COMPANY': company.company.name,
                        'ORDERNUMBER': self.pk,
                        'ORDERDATE': self.created,
                        'LINKORDER': url,
                        # 'CUSTOMMESSAGE': self.custom_message
                        }
                    }
                utils.email_users(
                    [user],
                    template_name='freight_ready_to_be_quoted',
                    local_vars=context_dict)

    def get_origin_region(self):
        if self.obj_type == Freight.OBJ_TYPE_EMPTY:
            location = self.get_withdraw_location()
        else:
            location = self.get_origin_waypoint()
        return location.region

    def get_destination_region(self):
        if self.obj_type == Freight.OBJ_TYPE_EMPTY:
            location = self.get_return_location()
        else:
            location = self.get_destination_waypoint()
        return location.region

    def get_origin_waypoint(self):
        return self.freightwaypoint_set.filter(
            obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN).get()

    def get_destination_waypoint(self):
        return self.freightwaypoint_set.filter(
            obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION).get()

    def get_withdraw_location(self):
        return self.freightlocation_set.filter(
            obj_type=FreightLocation.OBJ_TYPE_WITHDRAW).get()

    def get_return_location(self):
        return self.freightlocation_set.filter(
            obj_type=FreightLocation.OBJ_TYPE_RETURN).get()


class FreightLocation(models.Model):

    OBJ_TYPE_WITHDRAW = 'withdraw'
    OBJ_TYPE_RETURN = 'return'

    OBJ_TYPE_CHOICES = Choices((OBJ_TYPE_WITHDRAW, 'Retiro'),
                               (OBJ_TYPE_RETURN, 'Devolución'))

    freight = models.ForeignKey(
        'Freight',
        verbose_name=Freight._meta.verbose_name)

    obj_type = StatusField('Tipo', choices_name='OBJ_TYPE_CHOICES')

    region = models.ForeignKey(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightlocation_regions',
        verbose_name='región',
        null=True,
        blank=True)
    commune = models.ForeignKey(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightlocation_communes',
        verbose_name='comuna',)
    deposit_name = models.CharField('Nombre depósito', max_length=50)
    from_date = models.DateTimeField('Desde', null=True, blank=True)
    to_date = models.DateTimeField('Hasta', null=True, blank=True)

    class Meta:

        unique_together = ('freight', 'obj_type')


class FreightWayPoint(models.Model):

    OBJ_TYPE_ORIGIN = 'origin'
    OBJ_TYPE_DESTINATION = 'destination'

    OBJ_TYPE_CHOICES = Choices((OBJ_TYPE_ORIGIN, 'origen'),
                               (OBJ_TYPE_DESTINATION, 'destino'))

    POSITION_CHOICES = ([(x, x) for x in range(10)])

    freight = models.ForeignKey(
        'Freight',
        verbose_name=Freight._meta.verbose_name)

    obj_type = StatusField('Tipo', choices_name='OBJ_TYPE_CHOICES')

    # import/export
    region = models.ForeignKey(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightwaypoint_regions',
        verbose_name='región',
        null=True,
        blank=True)
    # import/export/domestic
    commune = models.ForeignKey(
        'locationstree.Location',
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightwaypoint_communes',
        null=True,
        blank=True,
        verbose_name='comuna', )
    who_load_unload = models.CharField(
        '¿Quíen carga/descarga?',
        max_length=50,
        blank=True)
    load_unload_time = models.CharField(
        'Tiempo carga/descarga',
        max_length=10,
        blank=True)
    from_date = models.DateTimeField('Desde')
    to_date = models.DateTimeField('Hasta', null=True, blank=True)

    # impo/expo
    ship_name = models.CharField('Nombre nave', max_length=50, blank=True)
    # this should be port_or_plant
    origin_destination_port = models.CharField(
        'Puerto origen/destino',
        max_length=50,
        blank=True)

    # position = models.PositiveIntegerField(
    #     'Posición',
    #     choices=POSITION_CHOICES,
    #     default=POSITION_CHOICES[0][0])


class ContainerType(models.Model):

    name = models.CharField('Nombre', max_length=50)
    sort_order = models.IntegerField('orden', default=0)

    class Meta:
        verbose_name = u'tipo de contenedor'
        verbose_name_plural = u'tipos de contenedor'

    def __unicode__(self):
        return u'%s' % self.name


class FreightDetail(models.Model):

    OBJ_TYPE_CONTAINER = 1
    OBJ_TYPE_GENERAL = 7
    OBJ_TYPE_BULK = 3
    OBJ_TYPE_PROJECT = 8
    OBJ_TYPE_LIQUID = 4
    OBJ_TYPE_HAZARDOUS = 5
    OBJ_TYPE_REFRIGERATED = 2

    # Types order must be kept, is how they should be displayed
    OBJ_TYPES_DICT = {
        OBJ_TYPE_CONTAINER: 'Contenedor',
        OBJ_TYPE_GENERAL: 'General',
        OBJ_TYPE_BULK: 'A granel',
        OBJ_TYPE_PROJECT: 'De proyecto/Sobredimensionada',
        OBJ_TYPE_LIQUID: 'Líquida',
        OBJ_TYPE_HAZARDOUS: 'Peligrosa',
        OBJ_TYPE_REFRIGERATED: 'Refrigerada/Multitemperatura',
        }

    OBJ_TYPES = OBJ_TYPES_DICT.items()

    freight = models.ForeignKey(
        'Freight',
        verbose_name=Freight._meta.verbose_name)

    obj_type = models.IntegerField(
        choices=OBJ_TYPES,
        default=OBJ_TYPE_CONTAINER,
        verbose_name='tipo de carga')

    container_type = models.ForeignKey(
        'ContainerType',
        null=True,
        blank=True,
        verbose_name=ContainerType._meta.verbose_name)
    units = models.PositiveIntegerField('unidades')
    weight = models.PositiveIntegerField(
        'peso unitario Kg',
        null=True,
        blank=True)
    volume = models.FloatField(
        'volumen unitario m3',
        null=True,
        blank=True)
    description = models.CharField(
        'descripción',
        max_length=100,
        null=True,
        blank=True)


class FreightFirstStep(TimeStampedModel):
    """Stores data before creating a Freight

    """
    obj_type = models.CharField(
        'tipo de  servicio',
        max_length=4,
        choices=Freight.OBJ_TYPE_CHOICES)
    reason = models.CharField(
        'Motivo',
        max_length=200,
        null=True,
        blank=True)

    insurance = models.CharField(
        'seguro de carga',
        max_length=50,
        blank=True)
    truck_type = models.ForeignKey(
        'TruckType',
        null=True,
        blank=True,
        verbose_name='tipo de camión',
        related_name='freightfirststep_truck_types')
    equipment = models.ForeignKey(
        'Equipment',
        null=True,
        blank=True,
        verbose_name='equipamiento',
        related_name='freightfirststep_equipments')
    other = models.CharField(
        'otro',
        max_length=50,
        blank=True)
    needs_storage = models.TextField(
        '¿Necesita almacenaje?',
        blank=True)
    service_conditions = models.TextField(
        'condiciones de servicio y forma de pago',
        blank=True)
    comments = models.TextField(
        'comentarios o información adicional',
        blank=True)

    # User information
    contact_email = models.EmailField('email de contacto')
    contact_first_name = models.CharField(
        'nombre',
        max_length=50,
        null=True,
        blank=True)
    contact_last_name = models.CharField(
        'apellido',
        max_length=50,
        null=True,
        blank=True)
    userdirectory_position = models.CharField(
        'Cargo',
        max_length=100,
        null=True,
        blank=True)
    userdirectory_phone = models.CharField(
        'Telefono',
        max_length=50,
        null=True,
        blank=True)
    userdirectory_mobile = models.CharField(
        'Celular',
        max_length=50,
        null=True,
        blank=True)

    # Company information
    company_name = models.CharField(
        'nombre de empresa',
        max_length=100,
        null=True,
        blank=True)
    company_business_name = models.CharField(
        'Razón Social',
        max_length=50,
        null=True,
        blank=True)
    company_business_number = models.CharField(
        'R.U.T.',
        max_length=50,
        null=True,
        blank=True)
    company_phone = models.CharField(
        'teléfono',
        max_length=20,
        null=True,
        blank=True)
    company_address_street = models.CharField(
        'Calle',
        max_length=50,
        null=True,
        blank=True)
    company_address_number = models.CharField(
        'Número',
        max_length=50,
        null=True,
        blank=True)
    company_address_floor = models.CharField(
        'Pasaje/Oficina/Piso',
        max_length=50,
        null=True,
        blank=True)
    company_address_region = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Región',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='firststep_company_address_regions',
        )
    company_address_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Comuna',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='firststep_company_address_communes',
        )
    company_address_city = models.CharField(
        'Ciudad',
        max_length=50,
        null=True,
        blank=True)

    # FreightLocation *Withdraw* information
    # Must be cero or one
    # prefix: freightlocation_withdraw
    freightlocation_withdraw_region = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightfirststep_freightlocation_withdraw_regions',
        verbose_name='región',)
    freightlocation_withdraw_commune = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightfirststep_freightlocation_withdraw_communes',
        verbose_name='comuna',)
    freightlocation_withdraw_deposit_name = models.CharField(
        'Nombre depósito',
        max_length=50,
        null=True,
        blank=True)
    freightlocation_withdraw_from_date = models.DateTimeField(
        'Desde',
        null=True,
        blank=True)
    freightlocation_withdraw_to_date = models.DateTimeField(
        'Hasta',
        null=True,
        blank=True)

    # FreightLocation *Returns* information
    # Must be cero or one
    # prefix: freightlocation_return
    freightlocation_return_region = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightfirststep_freightlocation_return_regions',
        verbose_name='región',)
    freightlocation_return_commune = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightfirststep_freightlocation_return_communes',
        verbose_name='comuna',)
    freightlocation_return_deposit_name = models.CharField(
        'Nombre depósito',
        max_length=50,
        null=True,
        blank=True)
    freightlocation_return_from_date = models.DateTimeField(
        'Desde',
        null=True,
        blank=True)
    freightlocation_return_to_date = models.DateTimeField(
        'Hasta',
        null=True,
        blank=True)

    # FreightWayPoint *Origin* information
    # Must be cero or one
    # prefix: freightwaypoint_origin
    freightwaypoint_origin_region = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightfirststep_freightwaypoint_origin_regions',
        verbose_name='región')
    freightwaypoint_origin_commune = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightfirststep_freightwaypoint_origin_communes',
        verbose_name='comuna')
    freightwaypoint_origin_who_load_unload = models.CharField(
        '¿Quíen carga?',
        max_length=50,
        blank=True)
    freightwaypoint_origin_load_unload_time = models.CharField(
        'Tiempo carga',
        max_length=10,
        blank=True)
    freightwaypoint_origin_from_date = models.DateTimeField(
        'Desde',
        null=True,
        blank=True)
    freightwaypoint_origin_to_date = models.DateTimeField(
        'Hasta',
        null=True,
        blank=True)
    freightwaypoint_origin_ship_name = models.CharField(
        'Nombre nave',
        max_length=50,
        blank=True)
    freightwaypoint_origin_origin_destination_port = models.CharField(
        'Puerto/Planta origen',
        max_length=50,
        blank=True)

    # FreightWayPoint *Destination* information
    # Can be cero or more
    # prefix: freightwaypoint_destination
    freightwaypoint_destination_region = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='freightfirststep_freightwaypoint_destination_regionss',
        verbose_name='región')
    freightwaypoint_destination_commune = models.ForeignKey(
        'locationstree.Location',
        null=True,
        blank=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='freightfirststep_freightwaypoint_destination_communes',
        verbose_name='comuna')
    freightwaypoint_destination_who_load_unload = models.CharField(
        '¿Quíen descarga?',
        max_length=50,
        blank=True)
    freightwaypoint_destination_load_unload_time = models.CharField(
        'Tiempo descarga',
        max_length=10,
        blank=True)
    freightwaypoint_destination_from_date = models.DateTimeField(
        'Desde',
        null=True,
        blank=True)
    freightwaypoint_destination_to_date = models.DateTimeField(
        'Hasta',
        null=True,
        blank=True)
    freightwaypoint_destination_ship_name = models.CharField(
        'Nombre nave',
        max_length=50,
        blank=True)
    freightwaypoint_destination_origin_destination_port = models.CharField(
        'Puerto/Planta destino',
        max_length=50,
        blank=True)

    # FreightDetail information
    # prefix: freightdetail_0
    freightdetail_0_obj_type = models.IntegerField(
        choices=FreightDetail.OBJ_TYPES,
        default=FreightDetail.OBJ_TYPE_CONTAINER,
        verbose_name='tipo de carga')
    freightdetail_0_units = models.PositiveIntegerField(
        'unidades')
    freightdetail_0_weight = models.PositiveIntegerField(
        'peso unitario Kg',
        null=True,
        blank=True)
    freightdetail_0_volume = models.PositiveIntegerField(
        'volumen unitario m3',
        null=True,
        blank=True)
    freightdetail_0_description = models.CharField(
        'descripción',
        max_length=100,
        null=True,
        blank=True)
    freightdetail_0_container_type = models.ForeignKey(
        'ContainerType',
        null=True,
        blank=True,
        verbose_name=ContainerType._meta.verbose_name)

    # FreightDetail information
    # prefix: freightdetail_1
    freightdetail_1_obj_type = models.IntegerField(
        choices=FreightDetail.OBJ_TYPES,
        null=True,
        blank=True,
        verbose_name='tipo de carga')
    freightdetail_1_units = models.PositiveIntegerField(
        'unidades',
        null=True,
        blank=True)
    freightdetail_1_weight = models.PositiveIntegerField(
        'peso unitario Kg',
        null=True,
        blank=True)
    freightdetail_1_volume = models.PositiveIntegerField(
        'volumen unitario m3',
        null=True,
        blank=True)
    freightdetail_1_description = models.CharField(
        'descripción',
        max_length=100,
        null=True,
        blank=True)
    freightdetail_1_container_type = models.ForeignKey(
        'ContainerType',
        null=True,
        blank=True,
        related_name='freightfirststep_freightdetail_1_container_types',
        verbose_name=ContainerType._meta.verbose_name)

    # FreightDetail information
    # prefix: freightdetail_2
    freightdetail_2_obj_type = models.IntegerField(
        choices=FreightDetail.OBJ_TYPES,
        null=True,
        blank=True,
        verbose_name='tipo de carga')
    freightdetail_2_units = models.PositiveIntegerField(
        'unidades',
        null=True,
        blank=True)
    freightdetail_2_weight = models.PositiveIntegerField(
        'peso unitario Kg',
        null=True,
        blank=True)
    freightdetail_2_volume = models.PositiveIntegerField(
        'volumen unitario m3',
        null=True,
        blank=True)
    freightdetail_2_description = models.CharField(
        'descripción',
        max_length=100,
        null=True,
        blank=True)
    freightdetail_2_container_type = models.ForeignKey(
        'ContainerType',
        null=True,
        blank=True,
        related_name='freightfirststep_freightdetail_2_container_types',
        verbose_name=ContainerType._meta.verbose_name)

    class Meta:
        verbose_name = u'primer paso de publicación de carga'
        verbose_name_plural = u'primeros pasos de publicación de carga'


class Quote(TimeStampedModel):

    STATUS_NEW = 'new'
    STATUS_APPROVED = 'approved'
    STATUS_REJECTED = 'rejected'

    STATUS = ((STATUS_NEW, 'Nueva'),
              (STATUS_APPROVED, 'Aprobada'),
              (STATUS_REJECTED, 'Rechazada'))

    UNIT_DAY = 'day'
    UNIT_OUR = 'hour'

    UNIT_CHOICES = ((UNIT_DAY, 'Día'),
                    (UNIT_OUR, 'Hora'))

    BILLIN_UNIT_CTR_DAY = 'ctrday'
    BILLIN_UNIT_M2_MONTH = 'm2month'

    BILLIN_UNIT_CHOICES = ((BILLIN_UNIT_CTR_DAY, 'CTR día'),
                           (BILLIN_UNIT_M2_MONTH, 'M2 mes'))

    freight = models.ForeignKey(
        'Freight',
        verbose_name=Freight._meta.verbose_name)
    userdirectory = models.ForeignKey(
        'UserDirectory',
        verbose_name=UserDirectory._meta.verbose_name)

    # container return quote #1
    deposit_name = models.CharField(
        'devolución de contenedor',
        max_length=50,
        null=True,
        blank=True)
    truck_type = models.ForeignKey(
        'TruckType',
        verbose_name='tipo de camión',
        related_name='quote_truck_types')
    equipment = models.ForeignKey(
        'Equipment',
        null=True,
        blank=True,
        related_name='quote_equipments',
        verbose_name=Equipment._meta.verbose_name)
    units = models.IntegerField('unidades')
    truck_unit_price = models.IntegerField(
        'valor unitario camión + equipamiento')
    price_per_ton = models.IntegerField(
        'valor por tonelada',
        null=True,
        blank=True)
    trip_duration = models.CharField('duración viaje', max_length=20)
    # container return quote #2
    deposit_name_2 = models.CharField(
        'devolución de contenedor 2',
        max_length=50,
        null=True,
        blank=True)
    truck_type_2 = models.ForeignKey(
        'TruckType',
        null=True,
        blank=True,
        verbose_name='tipo de camión 2',
        related_name='quote_truck_types_2')
    equipment_2 = models.ForeignKey(
        'Equipment',
        null=True,
        blank=True,
        related_name='quote_equipments_2',
        verbose_name=Equipment._meta.verbose_name + ' 2')
    units_2 = models.IntegerField(
        'unidades 2',
        null=True,
        blank=True)
    truck_unit_price_2 = models.IntegerField(
        'valor unitario camión 2',
        null=True,
        blank=True)
    price_per_ton_2 = models.IntegerField(
        'valor por tonelada 2',
        null=True,
        blank=True)
    trip_duration_2 = models.CharField(
        'duración viaje 2',
        max_length=20,
        null=True,
        blank=True)
    # Storage
    free_days = models.IntegerField(
        'días libre',
        null=True,
        blank=True)
    billing_unit = StatusField(
        'unidad de cobro',
        choices_name='BILLIN_UNIT_CHOICES',
        null=True,
        blank=True)
    rate = models.IntegerField(
        'tarifa CLP $',
        null=True,
        blank=True)
    # In case of exceeding length of storage stay
    max_stay_per_load_unload = models.IntegerField(
        'estadía máxima por evento de carga y descarga')
    freight_aditional_amount = models.IntegerField(
        'valor adicional flete CLP')
    freight_aditional_amount_unit = StatusField(
        'unidad valor adicional flete',
        choices_name='UNIT_CHOICES')
    freight_underslung_amount = models.IntegerField(
        'valor underslung CLP',
        null=True,
        blank=True)
    freight_underslung_amount_unit = StatusField(
        'unidad valor underslung',
        choices_name='UNIT_CHOICES',
        default=None,
        null=True,
        blank=True)
    deadfreight = models.IntegerField('falso flete (%)')

    validity = models.DateField('vigencia')
    insurance = models.CharField(
        'seguro cotización',
        max_length=50,
        blank=True)
    comments = models.TextField(
        'comentarios o información adicional',
        blank=True)

    # Internal use
    status = StatusField()

    confirmation_email_sent = models.BooleanField(
        'Email de confirmación de cotización enviado',
        default=False)
    allotted = models.BooleanField(
        'Adjudicada',
        default=False)

    class Meta:
        verbose_name = u'cotización'
        verbose_name_plural = u'cotizaciones'

    def get_absolute_url(self):
        return reverse('quote-detail', kwargs={'pk': self.pk})

    def email_transport_company(self):
        if not self.freight.send_emails:
            return

        site = utils.get_api_site()
        for userdirectory in self.userdirectory.company.userdirectory_set.all():
            user = userdirectory.user
            login_code = LoginCode.create_code_for_user(
                user=user, next=self.get_absolute_url())
            url = utils.get_url(site, 'login-with-code', login_code.code)
            context_dict = {
                user.email: {
                    'FNAME': user.first_name,
                    'LNAME': user.last_name,
                    'COMPANY': userdirectory.company.name,
                    'ORDERNUMBER': self.freight.pk,
                    'LINKORDER': url,
                    }
                }
            template_name = 'quote_created'
            utils.email_users(
                [user],
                template_name=template_name,
                local_vars=context_dict)


class CompanyAndUserRegistration(TimeStampedModel):

    COMPANY_TYPE_TRANSPORT = 'trans'
    COMPANY_TYPE_FREIGHTFORWARDER = 'freig'
    COMPANY_TYPE_STORAGE = 'stora'

    COMPANY_TYPE_CHOICES = (
        (COMPANY_TYPE_TRANSPORT, 'Transportista'),
        (COMPANY_TYPE_FREIGHTFORWARDER, 'Freight Forwarder'),
        (COMPANY_TYPE_STORAGE, 'Almacenaje/Bodega'), )

    company_type = StatusField(choices_name='COMPANY_TYPE_CHOICES')

    company_name = models.CharField('Nombre empresa', max_length=100)
    company_business_name = models.CharField('Razón social', max_length=50)
    company_business_number = models.CharField('RUT', unique=True, max_length=20)

    company_address_street = models.CharField('Calle', max_length=50)
    company_address_number = models.CharField('Número', max_length=50)
    company_address_floor = models.CharField('Oficina', max_length=50)
    company_address_region = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Región',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['region']},
        related_name='companyanduser_address_regions',
        )
    company_address_commune = models.ForeignKey(
        'locationstree.Location',
        verbose_name='Comuna',
        null=True,
        limit_choices_to={'level': locationstree.levels.CHILE['commune']},
        related_name='companyanduser_address_communes',
        )
    company_address_city = models.CharField('Ciudad', max_length=50)

    company_own_trucks = models.BooleanField('¿Tiene camiones propios?')
    company_own_storage = models.BooleanField('¿Cuenta con almacenaje?')

    contact_first_name = models.CharField('Nombre', max_length=50)
    contact_last_name = models.CharField('Apellido', max_length=50)
    contact_position = models.CharField('Cargo que desempeña', max_length=50)
    contact_email = models.CharField('Correo electrónico', max_length=50)
    contact_phone = models.CharField('Telefono fijo', max_length=50)
    contact_mobile = models.CharField('Telefono celular', max_length=50)

    class Meta:
        verbose_name = u'Registro de empresa'
        verbose_name_plural = u'Registros de empresa'


@receiver(post_save, sender=Freight, dispatch_uid='freight_post_save_callback')
def freight_post_save_callback(sender, instance, created, **kwargs):
    """Email <staff> when a Freight is created

    """
    if not instance.send_emails:
        return

    if created:
        site = utils.get_api_site()
        url = utils.get_url(site, 'admin:directory_freight_change',
                            instance.pk)
        context = {'URL': url}
        utils.email_group(
            'staff',
            template_name='staff_freight_created',
            global_vars=context)


@receiver(post_save, sender=Freight,
          dispatch_uid='freight_created_email_user_about_publication_request')
def freight_created_email_user_about_publication_request(
        sender, instance, created, **kwargs):
    if not instance.send_emails:
        return

    if created:
        userdirectory_set = instance.userdirectory.company.userdirectory_set.all()
        # userdirectory_set = instance.company.userdirectory_set.exclude(
        #     user=instance.user)
        users = [userdirectory.user for userdirectory in userdirectory_set]
        for user in users:
            site = utils.get_api_site()  # move outside for loop
            login_code = LoginCode.create_code_for_user(
                user=user, next=instance.get_absolute_url())
            url = utils.get_url(site, 'login-with-code', login_code.code)
            context = {
                user.email: {
                    'FNAME': user.first_name,
                    'LNAME': user.last_name,
                    'COMPANY': instance.userdirectory.company.name,
                    'ORDERNUMBER': instance.pk,
                    'LINKORDER': url,
                    }
                }
            utils.email_users(
                [user],
                local_vars=context,
                template_name='freight_created')


@receiver(post_save, sender=Freight,
          dispatch_uid='freight_post_save_email_user_about_publication_approved')
def freight_post_save_email_user_about_publication_approved(
        sender, instance, created, **kwargs):
    if not instance.send_emails:
        return

    if instance.status == 'approved' and \
            instance.email_about_publication_approved_sent is False:
        site = utils.get_api_site()
        for userdirectory in instance.userdirectory.company.userdirectory_set.all():
            user = userdirectory.user
            login_code = LoginCode.create_code_for_user(
                user=user,
                next=instance.get_absolute_url())
            url = utils.get_url(site, 'login-with-code', login_code.code)
            context = {
                user.email: {
                    'FNAME': user.first_name,
                    'LNAME': user.last_name,
                    'COMPANY': userdirectory.company.name,
                    'ORDERNUMBER': instance.pk,
                    'LINKORDER': url,
                    }
                }
            prefix = 'freight_approved'
            utils.email_users(
                [user],
                template_name=prefix,
                local_vars=context,)

        instance.email_about_publication_approved_sent = True
        instance.save()


@transaction.atomic
@receiver(post_save, sender=FreightFirstStep,
          dispatch_uid='freightfirststep_created_create_freight')
def freightfirststep_created_create_freight(
        sender, instance, created, **kwargs):
    if created:
        # get or create user
        user_kwargs = utils.get_attributes_by_prefix(instance, 'contact')
        user, user_created = get_user_model().objects.get_or_create(
            email=instance.contact_email,
            defaults=user_kwargs)
        if user_created:
            user.set_password(None)
            user.save()
            # get or create company
            company_kwargs = utils.get_attributes_by_prefix(instance, 'company')
            company, company_created = Company.objects.get_or_create(
                business_number=instance.company_business_number,
                defaults=company_kwargs)
            # get or create userdirectory
            userdirectory_kwargs = utils.get_attributes_by_prefix(
                instance,
                'userdirectory')
            userdirectory_kwargs.update({
                'is_owner': company_created,
                'is_approved': company_created})
            userdirectory, c = UserDirectory.objects.get_or_create(
                user=user,
                company=company,
                defaults=userdirectory_kwargs)
        else:
            # get user's userdirectory, that should exist
            userdirectory = user.userdirectory

        # create Freight
        freight = Freight.objects.create(
            obj_type=instance.obj_type,
            reason=instance.reason,
            insurance=instance.insurance,
            truck_type=instance.truck_type,
            equipment=instance.equipment,
            other=instance.other,
            service_conditions=instance.service_conditions,
            comments=instance.comments,
            needs_storage=instance.needs_storage,
            userdirectory=userdirectory)
        # create withdraw FreightLocation
        freightlocation_withdraw_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightlocation_withdraw')
        if any(freightlocation_withdraw_kwargs.values()):
            freightlocation_withdraw_kwargs['freight'] = freight
            freightlocation_withdraw_kwargs['obj_type'] = FreightLocation.OBJ_TYPE_WITHDRAW
            FreightLocation.objects.create(**freightlocation_withdraw_kwargs)
        # create return FreightLocation
        freightlocation_return_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightlocation_return')
        if any(freightlocation_return_kwargs.values()):
            freightlocation_return_kwargs['freight'] = freight
            freightlocation_return_kwargs['obj_type'] = FreightLocation.OBJ_TYPE_RETURN
            FreightLocation.objects.create(**freightlocation_return_kwargs)
        # create origin FreightWayPoint
        freightwaypoint_origin_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightwaypoint_origin')
        if any(freightwaypoint_origin_kwargs.values()):
            freightwaypoint_origin_kwargs['freight'] = freight
            freightwaypoint_origin_kwargs['obj_type'] = FreightWayPoint.OBJ_TYPE_ORIGIN
            FreightWayPoint.objects.create(**freightwaypoint_origin_kwargs)
        # create destination FreightWayPoint
        freightwaypoint_destination_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightwaypoint_destination')
        if any(freightwaypoint_destination_kwargs.values()):
            freightwaypoint_destination_kwargs['freight'] = freight
            freightwaypoint_destination_kwargs['obj_type'] = FreightWayPoint.OBJ_TYPE_DESTINATION
            FreightWayPoint.objects.create(**freightwaypoint_destination_kwargs)
        # create Freight detail(s)
        freightdetail_0_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightdetail_0')
        freightdetail_0_kwargs['freight'] = freight
        FreightDetail.objects.create(**freightdetail_0_kwargs)

        freightdetail_1_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightdetail_1')
        if any(freightdetail_1_kwargs.values()):
            freightdetail_1_kwargs['freight'] = freight
            FreightDetail.objects.create(**freightdetail_1_kwargs)

        freightdetail_2_kwargs = utils.get_attributes_by_prefix(
            instance,
            'freightdetail_2')
        if any(freightdetail_2_kwargs.values()):
            freightdetail_2_kwargs['freight'] = freight
            FreightDetail.objects.create(**freightdetail_2_kwargs)



@receiver(post_save, sender=FreightFirstStep,
          dispatch_uid='freightfirststep_created_send_email_if_user_does_not_exist')
def freightfirststep_created_send_email_if_user_does_not_exist(
        sender, instance, created, **kwargs):
    if created:
        pass
        # try:
        #     User.objects.get(email=instance.contact_email)
        # except User.DoesNotExist:
        #     site = utils.get_api_site()


@receiver(post_save, sender=Freight,
          dispatch_uid='freight_post_save_email_transport_companies')
def freight_post_save_email_transport_companies(
        sender, instance, created, **kwargs):
    """Email Transport Companies's users, one email for each user,
    when Freight status `send_email_to_transport_companies` is `True`
    and status is `approved`. Then set
    `send_email_to_transport_companies` to False and
    `email_to_transport_companies_sent` to `True`.

    """
    if instance.send_email_to_transport_companies:  # and instance.status == 'approved':
        instance.email_transport_companies()
        instance.send_email_to_transport_companies = False
        instance.email_to_transport_companies_sent = True
        instance.save()


@receiver(post_save, sender=Quote,
          dispatch_uid='quote_post_save_email_tucarga')
def quote_post_save_email_tucarga(
        sender, instance, created, **kwargs):
    """Email TuCarga staff, when a user quote a freight.

    """
    if not instance.freight.send_emails:
        return

    if created:
        site = utils.get_api_site()
        url = utils.get_url(site, 'admin:directory_quote_change', instance.pk)
        context = {'URL': url}
        utils.email_group('staff',
                          template_name='staff_quote_created',
                          global_vars=context)


@receiver(post_save, sender=Quote,
          dispatch_uid='quote_post_save_email_transport_company')
def quote_post_save_email_transport_company(
        sender, instance, created, **kwargs):
    """Email Transport Companies's users, one email for each user,
    when a user quote a freight. Then sets `confirmation_email_sent`
    to `True`.

    """
    if not instance.confirmation_email_sent:
        instance.email_transport_company()
        instance.confirmation_email_sent = True
        instance.save()


@receiver(post_save, sender=CompanyAndUserRegistration,
          dispatch_uid='company_registration_listener')
def company_registration_listener(sender, instance, created, **kwargs):
    """Create Companies, Users and UserDirectory when
    CompanyAndUserRegistration is created

    """
    if created:
        company = Company.objects.create(
            name=instance.company_name,
            business_name=instance.company_business_name,
            business_number=instance.company_business_number,
            address_street=instance.company_address_street,
            address_number=instance.company_address_number,
            address_floor=instance.company_address_floor,
            address_region=instance.company_address_region,
            address_commune=instance.company_address_commune,
            address_city=instance.company_address_city,
            )
        if instance.company_type == CompanyAndUserRegistration.COMPANY_TYPE_TRANSPORT:
            TransportCompany.objects.create(company=company)

        user = get_user_model().objects.create(
            email=instance.contact_email,
            first_name=instance.contact_first_name,
            last_name=instance.contact_last_name,
            )
        UserDirectory.objects.create(
            company=company,
            user=user,
            phone=instance.contact_phone,
            mobile=instance.contact_mobile,
            position=instance.contact_position
            )
        site = utils.get_api_site()
        url = utils.get_url(site, 'admin:directory_company_change', company.pk)
        utils.email_group(
            'staff',
            global_vars={'URL': url},
            template_name='staff_company_registered')
        context = {
            user.email: {
                'FNAME': user.first_name,
                'LNAME': user.last_name,
                'COMPANY': company.name,
                }
            }
        utils.email_users(
            [user],
            template_name='registration_confirmation',
            local_vars=context,)

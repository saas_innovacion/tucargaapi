from rest_framework import serializers

from tucargaapi.apps.directory.tests_models import EmptyModel


class EmptyModelSerializer(serializers.ModelSerializer):

    class Meta:

        model = EmptyModel

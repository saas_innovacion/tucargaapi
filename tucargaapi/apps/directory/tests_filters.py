import django_filters

from tucargaapi.apps.directory.tests_models import EmptyModel


class EmptyModelFilter(django_filters.FilterSet):

    class Meta:

        model = EmptyModel

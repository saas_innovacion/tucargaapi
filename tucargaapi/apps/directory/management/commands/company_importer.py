# coding=utf-8
import csv
import logging
import pickle
import tempfile

from django.contrib.auth import get_user_model
from django.core.management.base import LabelCommand
from django.db import transaction

from locationstree.models import Location

from tucargaapi.apps.directory.models import Company, UserDirectory, \
    TransportCompany, Coverage, FreightType

logger = logging.getLogger(__name__)

User = get_user_model()


class Command(LabelCommand):
    args = '<csv file>'
    help = 'Load companies from csv'

    @transaction.atomic
    def handle_label(self, label, **options):
        company_c = 0
        contacts_c = 0
        transportcompany_c = 0
        with open(label, 'r') as csvfile:
            errors = {'contacts': {},
                      'companies': {},
                      'transportcompanies': {}, }
            rows = csv.reader(csvfile, delimiter=',')
            for row in rows:
                kwargs = get_values_from_row(row)
                empresa, company_c = create_company(company_c, errors, kwargs)
                if empresa:
                    contacts_c = create_contacts(empresa, kwargs, contacts_c, errors)
                    if kwargs['tipo_de_empresa'] == 'Transportista':
                        transportcompany_c = create_transport_company(empresa, kwargs, transportcompany_c, errors)

        from pprint import pprint
        pprint('%s empresas creadas' % company_c)
        pprint('%s contactos creados' % contacts_c)
        pprint('%s empresas de transporte creadas' % transportcompany_c)
        pprint(errors['companies'])
        pprint(errors['contacts'])
        pprint(errors['transportcompanies'])


class LocationCache:
    def __init__(self):
        self._cache = {}
        self._file_name = '../tmp/comuna_map_pickle'
        self.load()

    def get(self, key):
        try:
            return self._cache[key]
        except:
            return key

    def set(self, key, value):
        self._cache[key] = value
        self.save()

    def load(self):
        self.get_file()
        try:
            self._cache = pickle.load(self._file)
        except EOFError:
            pass
        self._file.close()

    def get_file(self):
        try:
            file_obj = open(self._file_name)
        except IOError:
            file_obj = tempfile.TemporaryFile()
        self._file = file_obj

    def save(self):
        self._file = open(self._file_name, 'w')
        pickle.dump(self._cache, self._file)
        self._file.close()

location_cache = LocationCache()


def capitalize_words(words):
    words = ' '.join([w.capitalize() for w in words.split(' ')])
    words = words.replace(' Del ', ' del ').replace(' De ', ' de ')
    return words


def get_commune(value):
    play_value = value
    location = None
    while location is None:
        if play_value == '':
            return None
        play_value = location_cache.get(value)
        capitalized_value = capitalize_words(play_value)

        try:
            location = Location.objects.get(name=capitalized_value)
        except Location.DoesNotExist:
            print('Esta comuna no existe %s' % capitalized_value)
            play_value = raw_input('Comuna')
            location_cache.set(value, play_value)
    return location


def get_region(value):
    if not value:
        return None
    regions_map = {
        "I": "I - Tarapacá",
        "II": "II - Antofagasta",
        "III": "III - Atacama",
        "IV": "IV - Coquimbo",
        "V": "V - Valparaíso",
        "VI": "VI - Libertador General Bernardo O'Higgins",
        "VII": "VII - Maule",
        "VIII": "VIII - Biobío",
        "IX": "IX - La Araucanía",
        "X": "X - Los Lagos",
        "XI": "XI - Aisén del General Carlos Ibañez del Campo",
        "XII": "XII - Magallanes y de la Antártica Chilena",
        "RM": "XIII - Metropolitana de Santiago",
        "XIV": "XIV - Los Ríos",
        "XV": "XV - Arica y Parinacota",
        }

    value = regions_map[value]
    location = Location.objects.get(name=value)
    return location


def get_date(value):
    splited_value = value.split('-')
    if len(splited_value) == 3:
        return '-'.join([splited_value[2], splited_value[1], splited_value[0]])
    return None


def fix_empty_integer(value):
    try:
        value = int(value)
    except ValueError:
        value = 0
    return value


def create_company(c, errors, kwargs):
    company_kwargs = get_company_kwargs(kwargs)
    company_kwargs['status'] = Company.STATUS_CHOICES[1][0]
    empresa = None
    try:
        with transaction.atomic():
            empresa = Company.objects.create(**company_kwargs)
            c += 1
    except Exception, e:
        errors['companies'][company_kwargs['business_number']] = e
    return empresa, c


def get_company_kwargs(kwargs):
    import_kwargs = get_import_kwargs(kwargs)
    company_kwargs = map_to_company_model(kwargs)

    company_kwargs['business_number'] = company_kwargs['business_number'].replace('.', '')

    kwargs = {}
    kwargs.update(import_kwargs)
    kwargs.update(company_kwargs)

    return kwargs


def get_import_kwargs(kwargs):
    # prepend 'import_' to all kwargs
    prepended_kwargs = dict([('import_' + k, v) for k, v in kwargs.items()])
    return prepended_kwargs


def map_to_company_model(kwargs):
    company_kwargs = {}
    for item_k, item_v in TO_COMPANY_MODEL_FIELDS.items():
        company_kwargs[item_v] = kwargs[item_k]
    return company_kwargs


def get_values_from_row(row):
    kwargs = dict(zip(IMPORT_FIELDS, row))
    kwargs = transform_values(kwargs)
    return kwargs


def transform_values(kwargs):
    # Transform when necesary
    kwargs = dict([(k, v.strip()) for k, v in kwargs.items()])

    for field in DATE_FIELDS:
        kwargs[field] = get_date(kwargs[field])

    for field in COMMUNE_FIELDS:
        kwargs[field] = get_commune(kwargs[field])

    for field in REGION_FIELDS:
        kwargs[field] = get_region(kwargs[field])

    # Only for transport company
    # viajes_mes = fix_empty_integer(kwargs['viajes_mes'])
    # kwargs['viajes_mes_observacion'] = \
    #     kwargs['viajes_mes'] if viajes_mes == 0 else ''
    # kwargs['viajes_mes'] = viajes_mes
    return kwargs


def get_contacts_kwargs(kwargs):
    contacts_kwargs = {}
    for k in CONTACTS_FIELDS:
        contacts_kwargs[k] = kwargs[k]
    return contacts_kwargs


def get_first_last_name(value):
    names = value.split()
    names_len = len(names)
    first_name = ''
    last_name = ''
    if names_len == 1:
        first_name = names[0]
    elif names_len == 2:
        first_name = names[0]
        last_name = names[1]
    elif names_len == 3:
        first_name = names[0]
        last_name = names[1]
    elif names_len == 4:
        first_name = names[0]
        last_name = names[2]

    return {'first_name': first_name, 'last_name': last_name}


def get_contact_kwargs(contacts_kwargs, i):
    contact_kwargs = dict([(k.split('_')[0], v) for k, v in contacts_kwargs.items() if str(i) in k])
    return contact_kwargs


def create_contacts(empresa, kwargs, contacts_c, errors):
    contacts_kwargs = get_contacts_kwargs(kwargs)
    for i in range(1, 4):
        contact_kwargs = get_contact_kwargs(contacts_kwargs, i)
        contact_kwargs.update(get_first_last_name(contact_kwargs['contacto']))
        if not contact_kwargs['mail']:
            break
        try:
            with transaction.atomic():
                user, c = User.objects.get_or_create(
                    email=contact_kwargs['mail'],
                    defaults={
                        'first_name': contact_kwargs['first_name'],
                        'last_name': contact_kwargs['last_name']})
                UserDirectory.objects.create(
                    user=user,
                    company=empresa,
                    phone=contact_kwargs['telefono'],
                    mobile=contact_kwargs['celular'])
                contacts_c += 1
        except Exception, e:
            errors['contacts'][contact_kwargs['mail']] = e, empresa.business_number
    return contacts_c


def create_transport_company(empresa, kwargs, transportcompany_c, errors):
    transportcompany_kwargs = get_transportcompany_kwargs(kwargs)
    transportcompany_kwargs['company'] = empresa
    transportcompany = TransportCompany.objects.create(
        **transportcompany_kwargs)
    create_transport_company_coverage(transportcompany, kwargs)
    create_transport_company_freighttypes(transportcompany, kwargs)
    transportcompany_c += 1
    return transportcompany_c


def get_transportcompany_kwargs(kwargs):
    transportcompany_kwargs = {}
    for k in TRANSPORTCOMPANY_FIELDS:
        transportcompany_kwargs[k] = kwargs[k]
    return transportcompany_kwargs


def create_transport_company_coverage(transportcompany, kwargs):
    for item_k, item_v in COVERAGE_MAP.items():
        if kwargs[item_k]:
            transportcompany.coverage.add(item_v)


def create_transport_company_freighttypes(transportcompany, kwargs):
    freighttypes = get_freighttypes(kwargs['tipo_de_carga_que_mueve'])
    for freighttype_str in freighttypes:
        try:
            freighttype = FreightType.objects.get(name__icontains=freighttype_str)
            transportcompany.freight_types.add(freighttype)
        except:
            pass


def get_freighttypes(freighttypes):
    seps = (',', '-')
    for sep in seps:
        if sep in freighttypes:
            return freighttypes.split(sep)
    return [freighttypes]

CSV_HEADERS = (
    "TIPO DE EMPRESA",
    "NOMBRE EMPRESA",
    "RAZON SOCIAL",
    "RUT",
    "REGION",
    "COMUNA",
    "DIRECCIÓN",
    "SECTOR ECO. REVISADO",
    "RUBRO ECONOMICO",
    "TIPO DE CARGA que mueve",
    "Fecha de registro en el sitio web",
    "EJECUTIVO que lo visito",
    "FECHA REUNION",
    "TRANSPORTISTAS CON LOS QUE TRABAJA",
    "CLIENTES",
    "RUTAS FRECUENTES",
    "Zona Norte",
    "Centro Norte",
    "Centro",
    "RM",
    "Centro Sur",
    "Zona Sur",
    "Centro - Sur extremo",
    "Sur extremo",
    "SITIO WEB",
    "CONTACTO 1",
    "CARGO 1",
    "Mail 1",
    "TELÉFONO 1",
    "Celular 1",
    "CONTACTO 2",
    "CARGO 2",
    "mail 2",
    "TELÉFONO 2",
    "Celular 2",
    "CONTACTO 3",
    "CARGO 3",
    "MAIL 3",
    "TELÉFONO 3",
    "CELULAR 3"
    )


IMPORT_FIELDS = (
    'tipo_de_empresa',
    'nombre_empresa',
    'razon_social',
    'rut',
    'region',
    'comuna',
    'direccion',
    'sector_eco_revisado',
    'rubro_economico',
    'tipo_de_carga_que_mueve',
    'fecha_de_registro_en_el_sitio_web',
    'ejecutivo_que_lo_visito',
    'fecha_reunion',
    'transportistas_con_los_que_trabaja',
    'clientes',
    'rutas_frecuentes',
    'zona_norte',
    'centro_norte',
    'centro',
    'rm',
    'centro_sur',
    'zona_sur',
    'centro_sur_extremo',
    'sur_extremo',
    'sitio_web',
    'contacto_1',
    'cargo_1',
    'mail_1',
    'telefono_1',
    'celular_1',
    'contacto_2',
    'cargo_2',
    'mail_2',
    'telefono_2',
    'celular_2',
    'contacto_3',
    'cargo_3',
    'mail_3',
    'telefono_3',
    'celular_3',
)


TO_COMPANY_MODEL_FIELDS = {
    'nombre_empresa': 'name',
    'razon_social': 'business_name',
    'rut': 'business_number',
    'region': 'address_region',
    'comuna': 'address_commune',
    'sector_eco_revisado': 'economic_sector',
    'rubro_economico': 'economic_industry',
    'fecha_de_registro_en_el_sitio_web': 'registration_date',
    'sitio_web': 'web_site',
    }


TRANSPORTCOMPANY_FIELDS = (
    )


CONTACTS_FIELDS = (
    'contacto_1',
    'cargo_1',
    'mail_1',
    'telefono_1',
    'celular_1',
    'contacto_2',
    'cargo_2',
    'mail_2',
    'telefono_2',
    'celular_2',
    'contacto_3',
    'cargo_3',
    'mail_3',
    'telefono_3',
    'celular_3',
    )


COVERAGE_MAP = {
    'zona_norte': Coverage.objects.get(name='Norte'),
    'centro_norte': Coverage.objects.get(name='Centro - Norte'),
    'centro': Coverage.objects.get(name='Centro'),
    'rm': Coverage.objects.get(name='RM-Metropolitana de Santiago'),
    'centro_sur': Coverage.objects.get(name='Centro - Sur'),
    'zona_sur': Coverage.objects.get(name='Sur'),
    'centro_sur_extremo': Coverage.objects.get(name='Centro - Sur Extremo'),
    'sur_extremo': Coverage.objects.get(name='Sur Extremo'),
    }


DATE_FIELDS = (
    'fecha_de_registro_en_el_sitio_web',
    )


COMMUNE_FIELDS = (
    'comuna',
    )


REGION_FIELDS = (
    'region',
    )

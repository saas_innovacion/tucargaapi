from rest_framework import filters
from rest_framework import generics
from rest_framework import views
from rest_framework import viewsets
from rest_framework.response import Response

from django.contrib.auth import get_user_model

from locationstree.models import Location
from locationstree import levels

from tucargaapi.apps.directory.models import FreightFirstStep, \
    TransportCompany, Equipment, ContainerType, TruckType, \
    FreightDetail
from tucargaapi.apps.directory.serializers import \
    FreightFirstStepSerializer, \
    TransportCompanySerializer, \
    UserSerializer
from serializers import LocationSerializer


class TransportCompanyViewSet(generics.ListAPIView):

    model = TransportCompany
    serializer_class = TransportCompanySerializer
    paginate_by = 10
    max_paginate_by = 100


class FreightFirstStepView(generics.CreateAPIView):

    model = FreightFirstStep
    serializer_class = FreightFirstStepSerializer


class UserView(generics.RetrieveAPIView):

    model = get_user_model()
    lookup_field = 'email'
    serializer_class = UserSerializer


class RegionView(generics.ListAPIView):

    model = Location
    serializer_class = LocationSerializer

    def get_queryset(self):
        return Location.objects.filter(level=levels.CHILE['region'])


class CommuneView(generics.ListAPIView):

    model = Location
    serializer_class = LocationSerializer

    def get_queryset(self):
        communes = Location.objects.filter(level=levels.CHILE['commune'])
        if 'region' in self.kwargs:
            communes = communes.filter(parent__pk=self.kwargs['region'])
        communes = communes.order_by('name')

        return communes


class EquipmentView(generics.ListAPIView):

    model = Equipment

    def get_queryset(self):
        queryset = super(EquipmentView, self).get_queryset()
        queryset = queryset.order_by('sort_order')

        return queryset


class ContainerTypeView(generics.ListAPIView):

    model = ContainerType

    def get_queryset(self):
        queryset = super(ContainerTypeView, self).get_queryset()
        queryset = queryset.order_by('sort_order')

        return queryset


class FreightTypeView(views.APIView):

    def get(self, request, format=None):
        data = [{'id': ot[0], 'name': ot[1]} for ot in FreightDetail.OBJ_TYPES]
        return Response(data)


class TruckTypeView(generics.ListAPIView):

    model = TruckType

    def get_queryset(self):
        queryset = super(TruckTypeView, self).get_queryset()
        queryset = queryset.order_by('sort_order')

        return queryset

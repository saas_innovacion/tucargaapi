from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .api_views import FreightFirstStepView, TransportCompanyViewSet, \
    UserView, RegionView, CommuneView, EquipmentView, ContainerTypeView, \
    FreightTypeView, TruckTypeView
from .views import QuoteView, FreightDetailView, RegistrationView, \
    QuoteDetailView

urlpatterns = patterns(
    '',
    # API
    url(r'^freightfirststep/$',
        FreightFirstStepView.as_view(),
        name='freight-create'),
    url(r'^user/(?P<email>.+)$',
        UserView.as_view(),
        name='user-detail'),
    url(r'^transportcompany/$',
        TransportCompanyViewSet.as_view(),
        name='transportcompany'),
    # form.select's
    url(r'region/$',
        RegionView.as_view(),
        name='region-list'),
    url('commune/(?P<region>.+)/$',
        CommuneView.as_view(),
        name='commune-list'),
    url('commune/$',
        CommuneView.as_view(),
        name='commune-list'),
    url(r'equipment/$',
        EquipmentView.as_view(),
        name='equipment-list'),
    url(r'containertype/$',
        ContainerTypeView.as_view(),
        name='containertype-list'),
    url(r'freighttype/$',
        FreightTypeView.as_view(),
        name='freighttype-list'),
    url(r'trucktype/$',
        TruckTypeView.as_view(),
        name='trucktype-list'),

    # Web
    url(r'^freight/(?P<pk>\d+)/$',
        login_required(FreightDetailView.as_view()),
        name='freight-detail'),
    url(r'^freight/(?P<pk>\d+)/quote/$',
        login_required(QuoteView.as_view()),
        name='quote'),
    url(r'^registration/$',
        RegistrationView.as_view(),
        name='registration'),
    url(r'^quote/(?P<pk>\d+)/$',
        login_required(QuoteDetailView.as_view()),
        name='quote-detail'),
)

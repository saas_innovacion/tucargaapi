from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

import factory

from . import models

User = get_user_model()


class LocationFactory(factory.django.DjangoModelFactory):
    code = factory.Sequence(lambda n: '%d' % n)

    FACTORY_FOR = 'locationstree.Location'


class CompanyFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.Company
    FACTORY_DJANGO_GET_OR_CREATE = ('business_number',)

    business_number = factory.Sequence(lambda n: '1.111.1%02d-1' % n)
    address_commune = factory.SubFactory(LocationFactory)


class UserFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = User

    email = factory.Sequence(lambda n: 'user%d@company.com' % n)
    first_name = factory.Sequence(lambda n: 'first name %d' % n)


class GroupFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = Group


class UserDirectoryFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.UserDirectory

    company = factory.SubFactory(CompanyFactory)
    user = factory.SubFactory(UserFactory)


class FreightFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.Freight

    obj_type = models.Freight.OBJ_TYPE_EMPTY
    userdirectory = factory.SubFactory(UserDirectoryFactory)
    # status = models.Freight.STATUS[0][0]


class FreightFirstStepFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.FreightFirstStep

    obj_type = models.Freight.OBJ_TYPE_CHOICES


class CoverageFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.Coverage

    name = factory.Sequence(lambda n: 'coverage %d' % n)

    @factory.post_generation
    def from_regions(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for region in extracted:
                self.from_regions.add(region)

    @factory.post_generation
    def to_regions(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for region in extracted:
                self.to_regions.add(region)


class TransportCompanyFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.TransportCompany

    company = factory.SubFactory(CompanyFactory)

    @factory.post_generation
    def coverage(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for coverage in extracted:
                self.coverage.add(coverage)


class FreightCompanyTypeFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.FreightCompanyType

    name = factory.Sequence(lambda n: '%d' % n)


class FreightCompanyFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.FreightCompany

    company = factory.SubFactory(CompanyFactory)
    obj_type = factory.SubFactory(FreightCompanyTypeFactory)


class UserDirectoryFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.UserDirectory

    user = factory.SubFactory(UserFactory)
    company = factory.SubFactory(CompanyFactory)


class EquipmentFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.Equipment


class ContainerTypeFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.ContainerType


class TruckTypeFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.TruckType


class QuoteFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = models.Quote

    freight = factory.SubFactory(FreightFactory)

    units = factory.Sequence(lambda n: '%d' % n)
    truck_unit_price = factory.Sequence(lambda n: '%d' % n)
    max_stay_per_load_unload = factory.Sequence(lambda n: '%d' % n)
    freight_aditional_amount = factory.Sequence(lambda n: '%d' % n)
    freight_underslung_amount = factory.Sequence(lambda n: '%d' % n)
    deadfreight = factory.Sequence(lambda n: '%d' % n)
    validity = '1960-07-01'
    userdirectory = factory.SubFactory(UserDirectoryFactory)
    truck_type = factory.SubFactory(TruckTypeFactory)


def QuoteDataFactory():
    trucktype = TruckTypeFactory()
    data = {'truck_type': trucktype.pk,
            'units': 2,
            'truck_unit_price': 100,
            'trip_duration': '1 hora',
            'max_stay_per_load_unload': 4,
            'freight_aditional_amount': 5000,
            'freight_aditional_amount_unit': models.Quote.UNIT_DAY,
            'freight_underslung_amount': 6000,
            'freight_underslung_amount_unit': models.Quote.UNIT_DAY,
            'deadfreight': 20,
            'validity': '1879-05-21',
            'insurance': '5000',
            'comments': 'foo bar'}
    return data

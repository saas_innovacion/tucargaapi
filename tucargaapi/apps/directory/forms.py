from django import forms

from .models import Quote, CompanyAndUserRegistration


class QuoteForm(forms.ModelForm):

    validity = forms.CharField(
        label='Vigencia',
        widget=forms.TextInput(attrs={'class': 'datepicker-input'}))

    class Meta:
        exclude = ('freight', 'userdirectory', 'status', 'confirmation_email_sent', 'allotted')
        model = Quote

    def __init__(self, *args, **kwargs):
        self.freight = kwargs.pop('freight')
        self.userdirectory = kwargs.pop('userdirectory')
        return super(QuoteForm, self).__init__(*args, **kwargs)

    def save(self, force_insert=False, force_update=False, commit=True):
        form = super(QuoteForm, self).save(commit=False)

        form.freight = self.freight
        form.userdirectory = self.userdirectory

        if commit:
            form.save()


class CompanyAndUserRegistrationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CompanyAndUserRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['company_address_commune'].queryset = \
            self.fields['company_address_commune'].queryset.order_by('name')


    class Meta:
        exclude = ('company_type', )
        model = CompanyAndUserRegistration

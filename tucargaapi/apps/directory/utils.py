# coding=utf-8

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse

from tucargaapi.celery import app as celery


EMAIL_SUBJECTS = {
    # New freight, notify staff about new freight
    'staff_freight_created': 'Nueva Publicación de carga',
    # New freight, notify user that we received the publication
    'freight_created': 'Publicación de carga recibida',
    # Freight approved, notify user that freight was approved
    'freight_approved': 'Confirmación de carga aceptada!',
    # User want to email transport companies to receive quotes
    'freight_ready_to_be_quoted': 'Nueva Publicación de carga',
    # New Quote, email staff about new quote
    'staff_quote_created': 'Nueva Cotización',
    # New Quote, email Transport company users about new quote
    'quote_created': 'Cotización recibida',
    # Quote approved, email Transport company users that their quote
    # is approved
    'quote_confirmation': 'Cotización aprobada',
    # New Company, email staff about new company
    'staff_company_registered': 'Se registró una empresa',
    # New Company, email user to welcome
    'registration_confirmation': 'Bienvenido a Tu Carga', }


def get_users_emails(to_users):
    return [user.email for user in to_users]


def email_users(users, **kwargs):
    to_emails = get_users_emails(users)
    return send_mail.delay(to_emails, **kwargs)


def email_group(group_name, **kwargs):
    to_users = list(get_user_model().objects.filter(groups__name=group_name))
    return email_users(to_users, **kwargs)


@celery.task
def send_mail(to, template_name, subject=None, from_email=None,
              local_vars=None, global_vars=None):
    subject = subject or EMAIL_SUBJECTS[template_name]
    from_email = from_email or settings.DEFAULT_FROM_EMAIL
    local_vars = local_vars or {}
    global_vars = global_vars or {}

    msg = EmailMessage(from_email=from_email, to=to, subject=subject)
    msg.template_name = template_name
    msg.merge_vars = local_vars
    msg.global_merge_vars = global_vars

    return msg.send()


SERVICE_CHOICES = (('importation', 'Importation'),
                   ('exportation', 'Exportation'),
                   ('domestic', 'Domestic'),
                   ('empty container', 'Empty Container'),
                   )


def get_attributes_by_prefix(freightfirststep, prefix):
    kwargs = {}
    for key, value in freightfirststep.__dict__.items():
        if key.startswith(prefix):
            # cut key from prefix's lenght plus one('_')
            new_key = key[len(prefix) + 1:]
            kwargs[new_key] = value
    return kwargs


def get_api_site():
    site = Site.objects.get(name='Tu Carga API')
    return site


def get_url(site, name, arg):
    return 'http://' + site.domain + reverse(name, args=[arg])

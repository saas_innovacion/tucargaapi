# coding: utf-8

import json
import mock

from django.core import mail
from django.views.generic import FormView, DetailView, CreateView
from django.views.generic.detail import SingleObjectMixin
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse, resolve
from django.test import TestCase
from django.test.utils import override_settings
from django.utils.unittest import skipUnless

from django_nopassword.models import LoginCode
# from rest_framework import filters, status, serializers
from rest_framework import status, serializers
from rest_framework.generics import CreateAPIView, UpdateAPIView
# from rest_framework.viewsets import ReadOnlyModelViewSet
# import django_filters
import locationstree

from tucargaapi.apps.directory import utils
from tucargaapi.apps.directory.api_views import FreightFirstStepView, \
    UserView, RegionView, CommuneView
                                                 # CompanyViewSet)
# from tucargaapi.apps.directory.filters import CompanyFilter
from tucargaapi.apps.directory.forms import QuoteForm
from tucargaapi.apps.directory.models import Company, \
    CompanyAndUserRegistration, \
    Coverage, \
    Freight, \
    FreightDetail, \
    FreightFirstStep, \
    FreightLocation, \
    FreightWayPoint, \
    Quote
from tucargaapi.apps.directory.models import \
    freight_post_save_callback
from tucargaapi.apps.directory.models import \
    freight_created_email_user_about_publication_request
from tucargaapi.apps.directory.models import \
    freightfirststep_created_create_freight
from tucargaapi.apps.directory.models import \
    freightfirststep_created_send_email_if_user_does_not_exist
from tucargaapi.apps.directory.models import \
    freight_post_save_email_transport_companies
from tucargaapi.apps.directory.models import \
    freight_post_save_email_user_about_publication_approved
from tucargaapi.apps.directory.serializers import \
    FreightFirstStepSerializer, \
    TransportCompanySerializer, UserSerializer, LocationSerializer
# from tucargaapi.apps.directory.tests_filters import EmptyModelFilter
# from tucargaapi.apps.directory.tests_models import EmptyModel
from tucargaapi.apps.directory.tests_serializers import EmptyModelSerializer
from tucargaapi.apps.directory.views import QuoteView, FreightDetailView, \
    RegistrationView, QuoteDetailView

from . test_factories import CompanyFactory, FreightFactory, \
    FreightFirstStepFactory, UserFactory, TransportCompanyFactory, \
    UserDirectoryFactory, EquipmentFactory, \
    LocationFactory, ContainerTypeFactory, \
    QuoteDataFactory, QuoteFactory, GroupFactory


User = get_user_model()


# Commented out because changes needs to be made to company user model
# and filter is not a priority

# class CompanyListViewTest(TestCase):

#     def test_is_read_only_model_list_view(self):

#         self.assertEqual(ReadOnlyModelViewSet,
#                          CompanyViewSet().__class__.__bases__[0])  # noqa

#     @mock.patch.object(CompanyViewSet, 'serializer_class', EmptyModelSerializer)
#     @mock.patch.object(CompanyViewSet, 'filter_class', EmptyModelFilter)
#     def test_get_list(self):
#         EmptyModel.objects.create()
#         EmptyModel.objects.create()
#         url = reverse('company-list')

#         with mock.patch.object(CompanyViewSet, 'model', EmptyModel):

#             response = self.client.get(url)

#             self.assertEqual(
#                 EmptyModel.objects.all().count(),
#                 len(response.data))

#     def test_definition(self):
#         tcvs = CompanyViewSet()

#         self.assertEqual(tcvs.model, Company)
#         self.assertEqual(tcvs.serializer_class, CompanySerializer)
#         self.assertEqual(tcvs.filter_backends, (filters.DjangoFilterBackend, ))
#         self.assertEqual(tcvs.filter_class, CompanyFilter)


class TransportCompanySerializerTests(TestCase):

    def test_definition(self):
        self.assertEqual(TransportCompanySerializer().__class__.__bases__[0],
                         serializers.Serializer)
        tcs = TransportCompanySerializer()
        self.assertListEqual(
            ['logo', 'name', 'year_of_creation', 'address_region',
             'address_commune', 'coverage', 'storage'],
            tcs.fields.keys())


class FreightFirstStepViewTest(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_definition(self):
        view = FreightFirstStepView()

        self.assertEqual(view.__class__.__bases__[0],
                         CreateAPIView,)
        self.assertEqual(view.serializer_class, FreightFirstStepSerializer)
        self.assertEqual(view.model, FreightFirstStep)

    @mock.patch.object(FreightFirstStepView, 'serializer_class', new=EmptyModelSerializer)
    def test_url_exists(self):
        url = reverse('freight-create')

        response = self.client.post(url)

        self.assertEqual(response.status_code, 201)

    def test_url_resolve(self):
        url = reverse('freight-create')

        view = resolve(url)

        self.assertEqual(view.func.func_closure[0].cell_contents,
                         FreightFirstStepView)


class FreightFirstStepModelTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    @mock.patch('tucargaapi.apps.directory.models.FreightLocation')
    @mock.patch('tucargaapi.apps.directory.models.FreightDetail')
    def test_created_create_freight(self, mock_freightlocation, mock_freightdetail):
        # TODO: Only if company is approved, needs Company manager
        email = 'cargador@ec.com'
        freightfirststep = FreightFirstStepFactory.build(
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='Company 1',
            company_business_name='Company',
            company_business_number='11.111.111-1',
            company_address_street='Calle',
            company_address_number='1',
            company_address_city='Santiago')

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        self.assertTrue(Freight.objects.get())
        self.assertTrue(User.objects.get())
        self.assertTrue(Company.objects.get())

    @mock.patch('tucargaapi.apps.directory.models.FreightLocation')
    @mock.patch('tucargaapi.apps.directory.models.FreightDetail')
    def test_created_create_freight__user_exits(self, mock_freightlocation, mock_freightdetail):
        email = 'cargador@ec.com'
        user = UserFactory(email=email)
        company = CompanyFactory()
        UserDirectoryFactory(user=user, company=company)
        freightfirststep = FreightFirstStepFactory.build(
            company_business_number=company.business_number,
            contact_email=email)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        self.assertTrue(Freight.objects.get())
        self.assertTrue(User.objects.get())
        self.assertTrue(Company.objects.get())

    def test_created_create_freight_OBJ_TYPE_EMPTY(self):
        email='cargador@ec.com'
        business_number = '11.111.111-1'
        equipment = EquipmentFactory()
        region = LocationFactory()
        commune = LocationFactory()
        from_date = '2014-09-14T20:30'
        to_date = from_date
        containertype = ContainerTypeFactory()
        freightfirststep = FreightFirstStepFactory.build(
            obj_type=Freight.OBJ_TYPE_EMPTY,
            reason='foo',
            insurance='si, 5000',
            equipment=equipment,
            other='otro',
            service_conditions='foo bar',
            comments='# /* */ ;;',
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='company 1',
            company_business_name='freight company',
            company_business_number=business_number,
            company_address_street='the street',
            company_address_number='666',
            company_address_floor='3',
            company_address_region=region,
            company_address_commune=commune,
            company_address_city='gotham',
            freightlocation_withdraw_commune=commune,
            freightlocation_withdraw_deposit_name='foo',
            freightlocation_withdraw_from_date=from_date,
            freightlocation_withdraw_to_date=to_date,
            freightlocation_return_commune=commune,
            freightlocation_return_deposit_name='bar',
            freightlocation_return_from_date=from_date,
            freightlocation_return_to_date=to_date,
            freightdetail_0_units=1,
            freightdetail_0_weight=10000,
            # freightdetail_volume=2,
            # freightdetail_description='foo bar',
            freightdetail_0_container_type=containertype)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        freight = Freight.objects.get()

        # company with `business_number` exists
        self.assertEqual(
            business_number,
            freight.userdirectory.company.business_number)
        # company with `email` exists
        self.assertEqual(
            email,
            freight.userdirectory.user.email)
        # 2 FreightLocation exists, withdraw and return
        self.assertEqual(2, freight.freightlocation_set.all().count())
        self.assertEqual(
            1,
            freight.freightlocation_set.filter(
                obj_type=FreightLocation.OBJ_TYPE_WITHDRAW).count())
        self.assertEqual(
            1,
            freight.freightlocation_set.filter(
                obj_type=FreightLocation.OBJ_TYPE_RETURN).count())
        # a FreightDetail exists
        self.assertEqual(
            1,
            freight.freightdetail_set.filter(
                obj_type=FreightDetail.OBJ_TYPE_CONTAINER).count())

    def test_created_create_freight_OBJ_TYPE_DOMESTIC(self):
        email='cargador@ec.com'
        business_number = '11.111.111-1'
        equipment = EquipmentFactory()
        region = LocationFactory()
        commune = LocationFactory()
        from_date = '2014-09-14T20:30'
        to_date = from_date
        containertype = ContainerTypeFactory()
        freightfirststep = FreightFirstStepFactory.build(
            obj_type=Freight.OBJ_TYPE_DOMESTIC,
            reason='foo',
            insurance='si, 5000',
            equipment=equipment,
            other='otro',
            service_conditions='foo bar',
            comments='# /* */ ;;',
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='company 1',
            company_business_name='freight company',
            company_business_number=business_number,
            company_address_street='the street',
            company_address_number='666',
            company_address_floor='3',
            company_address_region=region,
            company_address_commune=commune,
            company_address_city='gotham',
            # WayPoint *origin*
            freightwaypoint_origin_region=region,
            freightwaypoint_origin_commune=commune,
            freightwaypoint_origin_who_load_unload='foo',
            freightwaypoint_origin_load_unload_time='dos horas',
            freightwaypoint_origin_from_date=from_date,
            freightwaypoint_origin_to_date=to_date,
            # WayPoint *destination*
            freightwaypoint_destination_region=region,
            freightwaypoint_destination_commune=commune,
            freightwaypoint_destination_who_load_unload='foo',
            freightwaypoint_destination_load_unload_time='3 horas',
            freightwaypoint_destination_from_date=from_date,
            freightwaypoint_destination_to_date=to_date,
            # FreightDetail
            freightdetail_0_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_0_units=1,
            freightdetail_0_weight=10000,
            freightdetail_0_volume=2,
            freightdetail_0_description='foo bar',
            freightdetail_0_container_type=containertype)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        freight = Freight.objects.get()

        # company with `business_number` exists
        self.assertEqual(
            business_number,
            freight.userdirectory.company.business_number)
        # company with `email` exists
        self.assertEqual(
            email,
            freight.userdirectory.user.email)
        # 2 FreightWayPoint exists, origin and destination
        self.assertEqual(2, freight.freightwaypoint_set.all().count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN).count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION).count())
        # a FreightDetail exists
        self.assertEqual(
            1,
            freight.freightdetail_set.filter(
                obj_type=FreightDetail.OBJ_TYPE_CONTAINER).count())

    def test_created_create_freight_OBJ_TYPE_IMPORT(self):
        email = 'cargador@ec.com'
        business_number = '11.111.111-1'
        equipment = EquipmentFactory()
        region = LocationFactory()
        commune = LocationFactory()
        from_date = '2014-09-14T20:30'
        to_date = from_date
        containertype = ContainerTypeFactory()
        freightfirststep = FreightFirstStepFactory.build(
            obj_type=Freight.OBJ_TYPE_IMPORT,
            reason='foo',
            insurance='si, 5000',
            equipment=equipment,
            other='otro',
            service_conditions='foo bar',
            comments='# /* */ ;;',
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='company 1',
            company_business_name='freight company',
            company_business_number=business_number,
            company_address_street='the street',
            company_address_number='666',
            company_address_floor='3',
            company_address_region=region,
            company_address_commune=commune,
            company_address_city='gotham',
            # FreightWayPoint *origin*
            freightwaypoint_origin_region=region,
            freightwaypoint_origin_from_date=from_date,
            freightwaypoint_origin_to_date=to_date,
            freightwaypoint_origin_ship_name='Cyana',
            freightwaypoint_origin_origin_destination_port='Puero de Palos',
            # FreightWayPoint *destination*
            freightwaypoint_destination_region=region,
            freightwaypoint_destination_commune=commune,
            freightwaypoint_destination_who_load_unload='foo',
            freightwaypoint_destination_load_unload_time='dos horas',
            freightwaypoint_destination_from_date=from_date,
            freightwaypoint_destination_to_date=to_date,
            freightwaypoint_destination_origin_destination_port='Area 51',
            # FreightLocation *return*
            freightlocation_return_commune=commune,
            freightlocation_return_deposit_name='bar',
            freightlocation_return_from_date=from_date,
            freightlocation_return_to_date=to_date,
            # FreightDetail
            freightdetail_0_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_0_units=1,
            freightdetail_0_weight=10000,
            freightdetail_0_volume=2,
            freightdetail_0_description='foo bar',
            freightdetail_0_container_type=containertype)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        freight = Freight.objects.get()

        # company with `business_number` exists
        self.assertEqual(
            business_number,
            freight.userdirectory.company.business_number)
        # company with `email` exists
        self.assertEqual(
            email,
            freight.userdirectory.user.email)
        # 2 FreightWayPoint exists, origin and destination
        self.assertEqual(2, freight.freightwaypoint_set.all().count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN).count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION).count())
        self.assertEqual(
            1,
            freight.freightlocation_set.filter(
                obj_type=FreightLocation.OBJ_TYPE_RETURN).count())
        # a FreightDetail exists
        self.assertEqual(
            1,
            freight.freightdetail_set.filter(
                obj_type=FreightDetail.OBJ_TYPE_CONTAINER).count())

    def test_created_create_freight_OBJ_TYPE_EXPORT(self):
        email = 'cargador@ec.com'
        business_number = '11.111.111-1'
        equipment = EquipmentFactory()
        region = LocationFactory()
        commune = LocationFactory()
        from_date = '2014-09-14T20:30'
        to_date = from_date
        containertype = ContainerTypeFactory()
        freightfirststep = FreightFirstStepFactory.build(
            obj_type=Freight.OBJ_TYPE_EXPORT,
            reason='foo',
            insurance='si, 5000',
            equipment=equipment,
            other='otro',
            service_conditions='foo bar',
            comments='# /* */ ;;',
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='company 1',
            company_business_name='freight company',
            company_business_number=business_number,
            company_address_street='the street',
            company_address_number='666',
            company_address_floor='3',
            company_address_region=region,
            company_address_commune=commune,
            company_address_city='gotham',
            # FreightWayPoint *origin*
            freightwaypoint_origin_region=region,
            freightwaypoint_origin_commune=commune,
            freightwaypoint_destination_who_load_unload='foo',
            freightwaypoint_destination_load_unload_time='dos horas',
            freightwaypoint_origin_from_date=from_date,
            freightwaypoint_origin_to_date=to_date,
            freightwaypoint_origin_origin_destination_port='Area 51',
            # FreightWayPoint *destination*
            freightwaypoint_destination_region=region,
            freightwaypoint_destination_from_date=from_date,
            freightwaypoint_destination_to_date=to_date,
            freightwaypoint_origin_ship_name='Cyana',
            freightwaypoint_destination_origin_destination_port='Puero de Palos',
            # FreightLocation *return*
            freightlocation_withdraw_commune=commune,
            freightlocation_withdraw_deposit_name='bar',
            freightlocation_withdraw_from_date=from_date,
            freightlocation_withdraw_to_date=to_date,
            # FreightDetail
            freightdetail_0_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_0_units=1,
            freightdetail_0_weight=10000,
            freightdetail_0_volume=2,
            freightdetail_0_description='foo bar',
            freightdetail_0_container_type=containertype)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        freight = Freight.objects.get()

        # company with `business_number` exists
        self.assertEqual(
            business_number,
            freight.userdirectory.company.business_number)
        # company with `email` exists
        self.assertEqual(
            email,
            freight.userdirectory.user.email)
        # 2 FreightWayPoint exists, origin and destination
        self.assertEqual(2, freight.freightwaypoint_set.all().count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN).count())
        self.assertEqual(
            1,
            freight.freightwaypoint_set.filter(
                obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION).count())
        self.assertEqual(
            1,
            freight.freightlocation_set.filter(
                obj_type=FreightLocation.OBJ_TYPE_WITHDRAW).count())
        # a FreightDetail exists
        self.assertEqual(
            1,
            freight.freightdetail_set.filter(
                obj_type=FreightDetail.OBJ_TYPE_CONTAINER).count())

    def test_created_create_freight_more_than_one_detail(self):
        email='cargador@ec.com'
        business_number = '11.111.111-1'
        equipment = EquipmentFactory()
        region = LocationFactory()
        commune = LocationFactory()
        from_date = '2014-09-14T20:30'
        to_date = from_date
        containertype = ContainerTypeFactory()
        freightfirststep = FreightFirstStepFactory.build(
            obj_type=Freight.OBJ_TYPE_EMPTY,
            reason='foo',
            insurance='si, 5000',
            equipment=equipment,
            other='otro',
            service_conditions='foo bar',
            comments='# /* */ ;;',
            contact_email=email,
            contact_first_name='John',
            contact_last_name='Doe',
            company_name='company 1',
            company_business_name='freight company',
            company_business_number=business_number,
            company_address_street='the street',
            company_address_number='666',
            company_address_floor='3',
            company_address_region=region,
            company_address_commune=commune,
            company_address_city='gotham',
            freightlocation_withdraw_commune=commune,
            freightlocation_withdraw_deposit_name='foo',
            freightlocation_withdraw_from_date=from_date,
            freightlocation_withdraw_to_date=to_date,
            freightlocation_return_commune=commune,
            freightlocation_return_deposit_name='bar',
            freightlocation_return_from_date=from_date,
            freightlocation_return_to_date=to_date,
            freightdetail_0_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_0_units=1,
            freightdetail_0_weight=10000,
            freightdetail_0_container_type=containertype,
            freightdetail_1_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_1_units=1,
            freightdetail_1_weight=10000,
            freightdetail_1_container_type=containertype,
            freightdetail_2_obj_type=FreightDetail.OBJ_TYPE_CONTAINER,
            freightdetail_2_units=1,
            freightdetail_2_weight=10000,
            freightdetail_2_container_type=containertype)

        freightfirststep_created_create_freight(
            sender=FreightFirstStep, instance=freightfirststep,
            created=True)

        freight = Freight.objects.get()

        # a FreightDetail exists
        self.assertEqual(
            3,
            freight.freightdetail_set.filter(
                obj_type=FreightDetail.OBJ_TYPE_CONTAINER).count())


class FreightDetailViewTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_definition(self):
        view = FreightDetailView()

        self.assertListEqual([DetailView],
                             list(view.__class__.__bases__))
        self.assertEqual(view.model, Freight)

    def test_url_exists(self):
        FreightFactory()
        user = User.objects.create(email='j@doe.com', password='')
        login_code = LoginCode.create_code_for_user(user)
        self.client.login(code=login_code.code, email='j@doe.com')
        url = reverse('freight-detail', kwargs={'pk': 1})

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_url_exists__anonymous(self):
        FreightFactory()
        url = reverse('freight-detail', kwargs={'pk': 1})

        response = self.client.get(url, follow=True)

        # It redirects to login but login does not exist because we
        # don't want a login page while using nopassword
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_url_resolve(self):
        url = reverse('freight-detail', kwargs={'pk': 1})

        view = resolve(url)

        # inner `func_closure` is FreightDetailView, outher is
        # `login_required` decorator
        self.assertEqual(
            view.func.func_closure[3].cell_contents.func_closure[0].cell_contents,
            FreightDetailView)


class FreightModelTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    @mock.patch('tucargaapi.apps.directory.models.utils.email_group')
    def test_post_save_callback(self, mock_email_group):
        freight = FreightFactory.build()
        send_mail_args = ('Subject', 'Message', 'contacto@tucarga.cl',
                          ['ltozzo@tucarga.cl'])
        mock_email_group.return_value = send_mail_args

        freight_post_save_callback(sender=Freight, instance=freight,
                                   created=True)

        mock_email_group.assert_called_once_with(
            'staff',
            global_vars={'URL': 'http://api.tucarga.info/admin/directory/freight/None/'},
            template_name='staff_freight_created')

    @mock.patch('tucargaapi.apps.directory.models.utils.email_group')
    def test_post_save_callback_dont_send_email(self, mock_email_group):
        freight = FreightFactory.build(send_emails=False)
        send_mail_args = ('Subject', 'Message', 'contacto@tucarga.cl',
                          ['ltozzo@tucarga.cl'])
        mock_email_group.return_value = send_mail_args

        freight_post_save_callback(sender=Freight, instance=freight,
                                   created=True)

        self.assertEqual(mock_email_group.call_count, 0)

    @mock.patch('tucargaapi.apps.directory.models.reverse')
    def test_get_admin_url(self, mock_reverse):
        mock_reverse.return_value = '/'
        freight = FreightFactory()

        mock_reverse.reset_mock()
        result = freight.get_admin_url()

        mock_reverse.assert_called_once_with(
            'admin:directory_freight_change', args=(1,))
        self.assertEqual(result, '/')

    # def test_post_save_send_email_to_transport_companies(self):
    #     user = UserFactory()
    #     company = CompanyFactory()
    #     TransportCompanyFactory(company=company)
    #     UserDirectory.objects.create(user=user, company=company)
    #     freight = FreightFactory(
    #         status='approved',
    #         send_email_to_transport_companies=True)

    #     freight_post_save_send_email_to_transport_companies(
    #         sender=Freight,
    #         instance=freight)

    #     self.assertEqual(len(mail.outbox), 1)
    #     self.assertEqual(mail.outbox[0].to, [user.email])

    def test_get_absolut_url(self):
        freight = FreightFactory()

        result = freight.get_absolute_url()

        self.assertEqual(result, '/directory/freight/1/')

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail')
    def test_when_created_email_user_about_publication_request(
            self, mock_send_mail):
        freight = FreightFactory.build()

        freight_created_email_user_about_publication_request(
            sender=Freight, instance=freight, created=False)

        self.assertFalse(mock_send_mail.called)

    def test_post_save_email_transport_companies(self):
        freight = FreightFactory(send_email_to_transport_companies=True)

        freight_post_save_email_transport_companies(
            sender=Freight, instance=freight, created=False)

        self.assertFalse(freight.send_email_to_transport_companies)
        self.assertTrue(freight.email_to_transport_companies_sent)

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail.delay')
    def test_email_transport_companies(self, mock_send_mail):
        freight = FreightFactory()
        user = UserFactory()
        user1 = UserFactory()
        user2 = UserFactory()
        company = CompanyFactory(status=Company.STATUS_APPROVED)
        company1 = CompanyFactory(status=Company.STATUS_APPROVED)
        transportcompany = TransportCompanyFactory(company=company)
        transportcompany1 = TransportCompanyFactory(company=company1)
        UserDirectoryFactory(user=user, company=company)
        UserDirectoryFactory(user=user1, company=company)
        UserDirectoryFactory(user=user2, company=company1)
        freight.contacted_transport_companies.add(transportcompany)
        freight.contacted_transport_companies.add(transportcompany1)

        freight.email_transport_companies()

        self.assertTrue(mock_send_mail.called)
        # 5 = 3 users + email to staff + email to company
        self.assertEqual(5, mock_send_mail.call_count)

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail.delay')
    def test_email_transport_companies_dont_send_email(self, mock_send_mail):
        freight = FreightFactory(send_emails=False)
        user = UserFactory()
        user1 = UserFactory()
        user2 = UserFactory()
        company = CompanyFactory(status=Company.STATUS_APPROVED)
        company1 = CompanyFactory(status=Company.STATUS_APPROVED)
        transportcompany = TransportCompanyFactory(company=company)
        transportcompany1 = TransportCompanyFactory(company=company1)
        UserDirectoryFactory(user=user, company=company)
        UserDirectoryFactory(user=user1, company=company)
        UserDirectoryFactory(user=user2, company=company1)
        freight.contacted_transport_companies.add(transportcompany)
        freight.contacted_transport_companies.add(transportcompany1)

        freight.email_transport_companies()

        self.assertEqual(mock_send_mail.call_count, 0)

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail.delay')
    def test_post_save_email_user_about_publication_approved(self, mock_send_mail):
        company = CompanyFactory()
        user = UserFactory(email="juan@ec.com")
        userdirectory = UserDirectoryFactory(user=user, company=company)
        freight = FreightFactory(
            userdirectory=userdirectory,
            status='approved')

        freight_post_save_email_user_about_publication_approved(
            Freight, freight, False)

        # 3 = 1 user + freight creation + publication approved
        self.assertEqual(mock_send_mail.call_count, 3)
        self.assertTrue(freight.email_about_publication_approved_sent)

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail.delay')
    def test_email_transport_companies_dont_send_email(self, mock_send_mail):
        freight = FreightFactory(send_emails=False)
        user = UserFactory()
        user1 = UserFactory()
        user2 = UserFactory()
        company = CompanyFactory(status=Company.STATUS_APPROVED)
        company1 = CompanyFactory(status=Company.STATUS_APPROVED)
        transportcompany = TransportCompanyFactory(company=company)
        transportcompany1 = TransportCompanyFactory(company=company1)
        UserDirectoryFactory(user=user, company=company)
        UserDirectoryFactory(user=user1, company=company)
        UserDirectoryFactory(user=user2, company=company1)
        freight.contacted_transport_companies.add(transportcompany)
        freight.contacted_transport_companies.add(transportcompany1)

        freight.email_transport_companies()

        self.assertEqual(mock_send_mail.call_count, 0)

    @mock.patch('tucargaapi.apps.directory.models.utils.send_mail.delay')
    def test_post_save_email_user_about_publication_approved_dont_send_email(self, mock_send_mail):
        company = CompanyFactory()
        user = UserFactory(email="juan@ec.com")
        userdirectory = UserDirectoryFactory(user=user, company=company)
        freight = FreightFactory(
            userdirectory=userdirectory,
            status='approved',
            send_emails=False)

        freight_post_save_email_user_about_publication_approved(
            Freight, freight, False)

        self.assertEqual(mock_send_mail.call_count, 0)

    def test_origin(self):
        freight = FreightFactory()
        freight_origin = FreightWayPoint.objects.create(
            freight=freight, obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN,
            from_date='2014-04-03')
        FreightWayPoint.objects.create(
            freight=freight, obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION,
            from_date='2014-04-07')

        result = freight.get_origin_waypoint()

        self.assertIsInstance(result, FreightWayPoint)
        self.assertEqual(result, freight_origin)

    def test_destination(self):
        freight = FreightFactory()
        freight_destination = FreightWayPoint.objects.create(
            freight=freight, obj_type=FreightWayPoint.OBJ_TYPE_DESTINATION,
            from_date='2014-04-06')
        FreightWayPoint.objects.create(
            freight=freight, obj_type=FreightWayPoint.OBJ_TYPE_ORIGIN,
            from_date='2014-04-02')

        result = freight.get_destination_waypoint()

        self.assertIsInstance(result, FreightWayPoint)
        self.assertEqual(result, freight_destination)

    def test_withdraw(self):
        commune = LocationFactory()
        freight = FreightFactory()
        freight_withdraw = FreightLocation.objects.create(
            freight=freight, obj_type=FreightLocation.OBJ_TYPE_WITHDRAW,
            commune=commune)
        FreightLocation.objects.create(
            freight=freight, obj_type=FreightLocation.OBJ_TYPE_RETURN,
            commune=commune)

        result = freight.get_withdraw_location()

        self.assertIsInstance(result, FreightLocation)
        self.assertEqual(result, freight_withdraw)

    def test_return(self):
        commune = LocationFactory()
        freight = FreightFactory()
        freight_return = FreightLocation.objects.create(
            freight=freight, obj_type=FreightLocation.OBJ_TYPE_RETURN,
            commune=commune)
        FreightLocation.objects.create(
            freight=freight, obj_type=FreightLocation.OBJ_TYPE_WITHDRAW,
            commune=commune)

        result = freight.get_return_location()

        self.assertIsInstance(result, FreightLocation)
        self.assertEqual(result, freight_return)


class FreightfirststepSerializerTests(TestCase):

    def test_definition(self):
        self.assertEqual(FreightFirstStepSerializer().__class__.__bases__[0],
                         serializers.ModelSerializer)
        self.assertEqual(
            FreightFirstStepSerializer.Meta.model,
            FreightFirstStep)


class UtilsTest(TestCase):

    fixtures = ['fixtures/development_sites.json', ]

    def test_get_users_emails(self):
        mock_user = mock.Mock()
        mock_user.email = 'john@example.com'
        to_users = [mock_user]

        result = utils.get_users_emails(to_users)

        self.assertListEqual(result, ['john@example.com'])

    @mock.patch('tucargaapi.apps.directory.utils.send_mail.delay')
    def test_email_users(self, mock_send_mail):
        email = 'user1@tc.com'
        to_emails = [email]
        user = UserFactory(email=email)
        to_users = [user]
        prefix = 'prefix'
        context = {'a': 'dict'}

        utils.email_users(to_users, template_name=prefix, local_vars=context)

        mock_send_mail.assert_called_once_with(to_emails, template_name=prefix, local_vars=context)

    @mock.patch('tucargaapi.apps.directory.utils.email_users')
    def test_email_group(self, mock_email_users):
        group_name = 'staff'
        user = UserFactory()
        group = GroupFactory(name=group_name)
        group.user_set.add(user)
        to_users = [user]
        prefix = 'prefix'
        context = {'a': 'dict'}

        utils.email_group(group_name, template_name=prefix, local_vars=context)

        mock_email_users.assert_called_once_with(to_users, template_name=prefix, local_vars=context)

    @skipUnless(getattr(settings, 'MANDRILL_API_KEY'), "Use --env .env_development_mandrill")
    @override_settings(EMAIL_BACKEND='djrill.mail.backends.djrill.DjrillBackend')
    def test_render_bodies(self):
        email = "a.varas+testing@tucarga.cl"
        to = [email]
        subject = 'Testing'
        local_vars = {
            email: {
                'FNAME': "Firstname",
                'LNAME': "Lastname",
                'COMPANY': "Companyname",
                'ORDERNUMBER': "X-1234",
                'LINKORDER': "http://www.tucarga.info/",
                'ORDERDATE': '2014-21-03',
                'URL': "http://backoffice.tucarga.info/",
                },
            }

        for template_name in utils.EMAIL_SUBJECTS:

            result = utils.send_mail(
                to=to,
                template_name=template_name,
                subject=subject,
                local_vars=local_vars, )

            self.assertEqual(1, result)


class CoverageModelTest(TestCase):

    fixtures = ['fixtures/locations']

    def test_coverage_region_choices(self):
        """Tests that `limit_choices_to` filter Locations by level

        """
        expected_choices = map(
            lambda x: (x['id'], x['name']),
            locationstree.models.Location.objects.filter(
                level=locationstree.levels.CHILE['region']
                ).values('id', 'name')
            )

        from_choices = Coverage._meta.get_field_by_name(
            'from_regions'
            )[0].get_choices()
        to_choices = Coverage._meta.get_field_by_name(
            'to_regions'
            )[0].get_choices()

        self.assertListEqual(from_choices[1:], expected_choices)
        self.assertListEqual(to_choices[1:], expected_choices)


# class CompanyFilterTest(TestCase):

#     fixtures = ['chile']

#     def test_definition(self):
#         tcf = CompanyFilter()

#         self.assertEqual(tcf._meta.model, Company)
#         self.assertEqual(tcf._meta.fields, ['coverage', 'freight_type'])
#         self.assertIsInstance(tcf.filters['coverage'],
#                               django_filters.MultipleChoiceFilter)
#         self.assertEqual(tcf.filters['coverage'].name,
#                          'coverage__regions__pk')
#         self.assertTrue(tcf.filters['coverage'].anded)

#     def test_filter(self):
#         freight = FreightFactory.objects.create()
#         c0 = Coverage.objects.create()
#         c1 = Coverage.objects.create()
#         c2 = Coverage.objects.create()
#         c3 = Coverage.objects.create()
#         c4 = Coverage.objects.create()
#         rs = locationstree.models.Location.objects.filter(level=1)
#         c0.regions.add(rs[0], rs[1])
#         c1.regions.add(rs[0], rs[1], rs[2])
#         c2.regions.add(rs[1], rs[2])
#         c3.regions.add(rs[2], rs[3])
#         c4.regions.add(rs[4], rs[5])
#         Company.objects.create(coverage=c0, freight_type=freight,
#                                business_number=1)
#         Company.objects.create(coverage=c1, freight_type=freight,
#                                business_number=2)
#         Company.objects.create(coverage=c2, freight_type=freight,
#                                business_number=3)
#         Company.objects.create(coverage=c3, freight_type=freight,
#                                business_number=4)
#         Company.objects.create(coverage=c4, freight_type=freight,
#                                business_number=5)

#         filter_list = (
#             ((rs[0].pk, rs[0].pk),  # request
#              [1, 2]),  # response
#             ((rs[1].pk, rs[1].pk),
#              [1, 2, 3]),
#             ((rs[2].pk, rs[2].pk),
#              [2, 3, 4]),
#             ((rs[3].pk, rs[3].pk),
#              [4, ]),
#             ((rs[4].pk, rs[4].pk),
#              [5, ]),
#             ((rs[0].pk, rs[1].pk),
#              [1, 2]),
#             ((rs[0].pk, rs[2].pk),
#              [2, ]),
#             ((rs[1].pk, rs[2].pk),
#              [2, 3]),
#             ((rs[2].pk, rs[3].pk),
#              [4, ]),
#             ((rs[4].pk, rs[5].pk),
#              [5, ]),
#             ((rs[3].pk, rs[4].pk),
#              []),
#             )

#         tcf = CompanyFilter()
#         companies = Company.objects.all()
#         for f in filter_list:
#             queryset = tcf.filters['coverage'].filter(companies, f[0])
#             expected_pks = [c[0] for c in queryset.values_list('pk')]

#             self.assertListEqual(
#                 expected_pks,
#                 f[1])


class QuoteFormTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_definition(self):
        self.assertEqual(
            QuoteForm.Meta.exclude,
            ('freight', 'userdirectory', 'status', 'confirmation_email_sent', 'allotted'))

    def test_init(self):
        QuoteForm(freight=None, userdirectory=None)

    def test_save(self):
        userdirectory = UserDirectoryFactory()
        freight = FreightFactory(
            status='approved',
            userdirectory=userdirectory)
        data = QuoteDataFactory()
        form = QuoteForm(data, freight=freight, userdirectory=userdirectory)
        form.save()


class QuoteViewTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    # Definition

    def test_class_definition(self):
        pov = QuoteView()

        self.assertListEqual([FormView, SingleObjectMixin],
                             list(QuoteView().__class__.__bases__))
        self.assertEqual(pov.model, Freight)
        self.assertEqual(pov.form_class, QuoteForm)
        self.assertTrue(pov.template_name)

    @mock.patch.object(Freight.objects, 'filter')
    def test_get_queryset__filter_by_status_equals_approved(self, mock_filter):
        pov = QuoteView()

        pov.get_queryset()

        mock_filter.assert_called_once_with(status='approved')

    @mock.patch('tucargaapi.apps.directory.views.reverse')
    def test_get_success_url(self, mock_reverse):
        pov = QuoteView()

        pov.get_success_url()

        mock_reverse.assert_called_once_with('logout')

    @mock.patch.object(QuoteView, 'get_success_url')
    @mock.patch.object(QuoteForm, 'save')
    def test_form_valid(self, mock_save, mock_get_success_url):
        freight = Freight()
        userdirectory = UserDirectoryFactory()
        kwargs = {'freight': freight, 'userdirectory': userdirectory}
        form = QuoteForm(**kwargs)
        pov = QuoteView()

        pov.form_valid(form)

        mock_save.assert_called_once_with()

    def test_get_form_kwargs(self):
        company = CompanyFactory()
        user = UserFactory()
        userdirectory = UserDirectoryFactory(user=user, company=company)
        freight = FreightFactory(
            status='approved',
            userdirectory=userdirectory)
        pov = QuoteView()
        pov.request = mock.Mock()
        pov.request.method = 'POST'
        pov.object = freight
        pov.request.user = user
        pov.kwargs = {'pk': freight.pk}

        result = pov.get_form_kwargs()

        self.assertIn('freight', result)
        self.assertEqual(result['freight'], freight)
        self.assertIn('userdirectory', result)
        self.assertEqual(result['userdirectory'], userdirectory)

    def test_get(self):
        email = 'j@doe.com'
        user = UserFactory(email=email)
        company = CompanyFactory()
        UserDirectoryFactory(user=user, company=company)
        login_code = LoginCode.create_code_for_user(user)
        self.client.login(code=login_code.code, email=email)
        freight = FreightFactory(status='approved')
        url = reverse('quote', kwargs={'pk': freight.pk})

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('directory/quote_form.html')

    def test_post(self):
        email = 'j@doe.com'
        user = UserFactory(email=email)
        company = CompanyFactory()
        UserDirectoryFactory(user=user, company=company)
        login_code = LoginCode.create_code_for_user(user)
        self.client.login(code=login_code.code, email=email)
        freight = FreightFactory(status='approved')
        data = QuoteDataFactory()
        logout_url = reverse('logout')
        url = reverse('quote', kwargs={'pk': freight.pk})

        response = self.client.post(url, data, follow=False)

        self.assertEqual(response.status_code, 302)
        self.assertIn(logout_url, response._headers['location'][1])


class CompanyModelTests(TestCase):

    def test_definition(self):
        # custom manager
        pass

    def test_get_logo_url(self):
        c = CompanyFactory()

        result = c.get_logo_url()

        self.assertEqual('', result)


class CompanyManagerTests(TestCase):
    def test_actives(self):
        # filter by active
        pass


class UserViewSetTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_definition(self):
        self.assertEqual(UserView.model,
                         User)
        self.assertEqual(UserView.lookup_field, 'email')
        self.assertEqual(UserView.serializer_class, UserSerializer)

    def test_get(self):
        email = 'thechoosenone@matrix.com'
        user = UserFactory(email=email, first_name='John', last_name='Doe')
        company = CompanyFactory(name='Big company', business_name='Do stuff',
                                 business_number='123')
        userdirectory = UserDirectoryFactory(user=user, position='Boss',
                                             phone='095555555',
                                             mobile='29999999',
                                             company=company)
        url = reverse('user-detail', kwargs={'email': email})
        result = {'first_name': user.first_name,
                  'last_name': user.last_name,
                  'position': userdirectory.position,
                  'phone': userdirectory.phone,
                  'mobile': userdirectory.mobile,
                  'company_name': company.name,
                  'company_business_name': company.business_name,
                  'company_business_number': company.business_number}

        response = self.client.get(url)

        self.assertEqual(result, json.loads(response.content))


class RegistrationViewTests(TestCase):

    def test_definition(self):
        self.assertIsInstance(RegistrationView(),
                              CreateView)
        self.assertEqual(RegistrationView.model,
                         CompanyAndUserRegistration)

    def test_url_resolve(self):
        url = reverse('registration')

        view = resolve(url)

        self.assertEqual(
            view.func.func_closure[0].cell_contents,
            RegistrationView)

    def test_template_exists(self):
        url = reverse('registration')

        self.client.get(url)

    def test_get_success_url(self):
        rv = RegistrationView()

        self.assertEqual(rv.get_success_url(),
                         settings.SUCCESS_URL.url)


class RegionViewTests(TestCase):

    fixtures = ['fixtures/locations.json']

    def test_definition(self):
        self.assertEqual(RegionView.serializer_class,
                         LocationSerializer)

    def test_get(self):
        url = reverse('region-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # number of regions 15
        self.assertEqual(15, len(response.data))


class CommuneViewTest(TestCase):

    fixtures = ['fixtures/locations.json', 'fixtures/development_sites.json']

    def test_definition(self):
        self.assertEqual(CommuneView.serializer_class,
                         LocationSerializer)

    def test_get(self):
        url = reverse('commune-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # number of communes 346
        self.assertEqual(346, len(response.data))

    def test_filter(self):
        region = locationstree.models.Location.objects.filter(
            level=locationstree.levels.CHILE['region']).latest('pk')
        communes = region.get_descendants()

        url = reverse('commune-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # number of communes 346
        self.assertEqual(346, len(response.data))


class EquipmentViewTest(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_get(self):
        url = reverse('equipment-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ContainerViewTest(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_get(self):
        url = reverse('containertype-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TruckTypeViewTest(TestCase):

    def test_get(self):
        url = reverse('trucktype-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class LocationSerializerTests(TestCase):

    def test_definition(self):
        self.assertEqual(LocationSerializer().__class__.__bases__[0],
                         serializers.ModelSerializer)
        self.assertEqual(LocationSerializer.Meta.model,
                         locationstree.models.Location)
        self.assertEqual(LocationSerializer.Meta.fields,
                         ('id', 'name'))


class QuoteDetailViewTests(TestCase):

    fixtures = ['fixtures/development_sites.json']

    def test_definition(self):
        view = QuoteDetailView()

        self.assertListEqual([DetailView],
                             list(view.__class__.__bases__))
        self.assertEqual(view.model, Quote)

    def test_url_exists(self):
        quote = QuoteFactory()
        user = User.objects.create(email='j@doe.com', password='')
        login_code = LoginCode.create_code_for_user(user)
        self.client.login(code=login_code.code, email='j@doe.com')
        url = reverse('quote-detail', kwargs={'pk': quote.pk})

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_url_exists__anonymous(self):
        quote = QuoteFactory()
        url = reverse('quote-detail', kwargs={'pk': quote.pk})

        response = self.client.get(url, follow=True)

        # It redirects to login but login does not exist because we
        # don't want a login page while using nopassword
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_url_resolve(self):
        quote = QuoteFactory()
        url = reverse('quote-detail', kwargs={'pk': quote.pk})

        view = resolve(url)

        # inner `func_closure` is QuoteDetailView, outher is
        # `login_required` decorator
        self.assertEqual(
            view.func.func_closure[3].cell_contents.func_closure[0].cell_contents,
            QuoteDetailView)

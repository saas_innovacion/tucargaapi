from django.contrib.auth import get_user_model

from rest_framework import serializers

from locationstree.models import Location

from tucargaapi.apps.directory.models import FreightFirstStep


class TransportCompanySerializer(serializers.Serializer):

    logo = serializers.CharField(source='company.get_logo_url')
    name = serializers.CharField(source='company.name')
    year_of_creation = serializers.CharField(source='company.year_of_creation')
    address_region = serializers.CharField(source='company.address_region')
    address_commune = serializers.CharField(source='company.address_commune')
    coverage = serializers.CharField(source='coverage_names')
    storage = serializers.CharField(source='has_storage')
    # freight_types = serializers.CharField(source='freighttypes_names')


class FreightFirstStepSerializer(serializers.ModelSerializer):

    class Meta:

        model = FreightFirstStep


class UserSerializer(serializers.Serializer):

    first_name = serializers.CharField(source='first_name')
    last_name = serializers.CharField(source='last_name')

    position = serializers.CharField(source='userdirectory.position')
    phone = serializers.CharField(source='userdirectory.phone')
    mobile = serializers.CharField(source='userdirectory.mobile')

    company_name = serializers.CharField(source='userdirectory.company.name')
    company_business_name = serializers.CharField(
        source='userdirectory.company.business_name')
    company_business_number = serializers.CharField(
        source='userdirectory.company.business_number')




class LocationSerializer(serializers.ModelSerializer):

    class Meta:

        model = Location
        fields = ('id', 'name')

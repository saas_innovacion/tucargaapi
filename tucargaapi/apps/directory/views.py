from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, FormView, CreateView
from django.views.generic.detail import SingleObjectMixin

from .forms import QuoteForm, CompanyAndUserRegistrationForm
from .models import Freight, UserDirectory, CompanyAndUserRegistration, Quote


class QuoteView(FormView, SingleObjectMixin):

    model = Freight
    form_class = QuoteForm
    template_name = 'directory/quote_form.html'

    def get_queryset(self):
        return Freight.objects.filter(status='approved')

    def get_success_url(self):
        logout_url = reverse('logout')
        return logout_url

    def form_valid(self, form):
        form.save()
        return super(QuoteView, self).form_valid(form)

    def get_form_kwargs(self):
        self.object = self.get_object()
        kwargs = super(QuoteView, self).get_form_kwargs()
        kwargs.update({'freight': self.object})
        userdirectory = UserDirectory.objects.get(user=self.request.user)
        kwargs.update({'userdirectory': userdirectory})
        return kwargs


class FreightDetailView(DetailView):

    model = Freight


class RegistrationView(CreateView):

    model = CompanyAndUserRegistration
    form_class = CompanyAndUserRegistrationForm

    def get_success_url(self):
        return settings.SUCCESS_URL.url


class QuoteDetailView(DetailView):

    model = Quote

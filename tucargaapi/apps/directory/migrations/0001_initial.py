# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Certification'
        db.create_table(u'directory_certification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'directory', ['Certification'])

        # Adding model 'FreightType'
        db.create_table(u'directory_freighttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sort_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'directory', ['FreightType'])

        # Adding model 'Coverage'
        db.create_table(u'directory_coverage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal(u'directory', ['Coverage'])

        # Adding M2M table for field from_regions on 'Coverage'
        m2m_table_name = db.shorten_name(u'directory_coverage_from_regions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('coverage', models.ForeignKey(orm[u'directory.coverage'], null=False)),
            ('location', models.ForeignKey(orm[u'locationstree.location'], null=False))
        ))
        db.create_unique(m2m_table_name, ['coverage_id', 'location_id'])

        # Adding M2M table for field to_regions on 'Coverage'
        m2m_table_name = db.shorten_name(u'directory_coverage_to_regions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('coverage', models.ForeignKey(orm[u'directory.coverage'], null=False)),
            ('location', models.ForeignKey(orm[u'locationstree.location'], null=False))
        ))
        db.create_unique(m2m_table_name, ['coverage_id', 'location_id'])

        # Adding model 'Company'
        db.create_table(u'directory_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('business_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('business_number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('year_of_creation', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('address_street', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('address_number', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('address_floor', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('address_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='company_address_regions', null=True, to=orm['locationstree.Location'])),
            ('address_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='company_address_communes', null=True, to=orm['locationstree.Location'])),
            ('address_city', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('economic_industry', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('economic_sector', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('registration_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('pilot_group', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('activation_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('web_site', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='new', max_length=100)),
            ('import_tipo_de_empresa', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_nombre_empresa', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_razon_social', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_rut', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_region', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_comuna', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_direccion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_sector_eco_revisado', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_rubro_economico', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_tipo_de_carga_que_mueve', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_fecha_de_registro_en_el_sitio_web', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_ejecutivo_que_lo_visito', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_fecha_reunion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_transportistas_con_los_que_trabaja', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_clientes', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_rutas_frecuentes', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_zona_norte', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_centro_norte', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_centro', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_rm', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_centro_sur', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_zona_sur', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_centro_sur_extremo', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_sur_extremo', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_sitio_web', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_contacto_1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_cargo_1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_mail_1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_telefono_1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_celular_1', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_contacto_2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_cargo_2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_mail_2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_telefono_2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_celular_2', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_contacto_3', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_cargo_3', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_mail_3', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_telefono_3', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('import_celular_3', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['Company'])

        # Adding M2M table for field certifications on 'Company'
        m2m_table_name = db.shorten_name(u'directory_company_certifications')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('company', models.ForeignKey(orm[u'directory.company'], null=False)),
            ('certification', models.ForeignKey(orm[u'directory.certification'], null=False))
        ))
        db.create_unique(m2m_table_name, ['company_id', 'certification_id'])

        # Adding model 'Route'
        db.create_table(u'directory_route', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='route_from_communes', null=True, to=orm['locationstree.Location'])),
            ('to_commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='route_to_communes', null=True, to=orm['locationstree.Location'])),
        ))
        db.send_create_signal(u'directory', ['Route'])

        # Adding model 'TransportCompany'
        db.create_table(u'directory_transportcompany', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('company', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['directory.Company'], unique=True)),
            ('number_of_trips_per_month', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('full_load_trips_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('LTL_trips_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('do_international_trips', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('international_trips_from_1', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('international_trips_to_1', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('international_trips_from_2', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('international_trips_to_2', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('international_trips_from_3', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('international_trips_to_3', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('empty_trips_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['TransportCompany'])

        # Adding M2M table for field freight_types on 'TransportCompany'
        m2m_table_name = db.shorten_name(u'directory_transportcompany_freight_types')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompany', models.ForeignKey(orm[u'directory.transportcompany'], null=False)),
            ('freighttype', models.ForeignKey(orm[u'directory.freighttype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompany_id', 'freighttype_id'])

        # Adding M2M table for field coverage on 'TransportCompany'
        m2m_table_name = db.shorten_name(u'directory_transportcompany_coverage')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompany', models.ForeignKey(orm[u'directory.transportcompany'], null=False)),
            ('coverage', models.ForeignKey(orm[u'directory.coverage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompany_id', 'coverage_id'])

        # Adding model 'FreightInsurance'
        db.create_table(u'directory_freightinsurance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transportcompany', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.TransportCompany'])),
            ('insurance_company', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('validity', self.gf('django.db.models.fields.DateField')()),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'directory', ['FreightInsurance'])

        # Adding model 'CompanyLocation'
        db.create_table(u'directory_companylocation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Company'])),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companylocation_regions', null=True, to=orm['locationstree.Location'])),
            ('commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companylocation_communes', null=True, to=orm['locationstree.Location'])),
            ('address_city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'directory', ['CompanyLocation'])

        # Adding model 'Day'
        db.create_table(u'directory_day', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
        ))
        db.send_create_signal(u'directory', ['Day'])

        # Adding model 'EmptyRoute'
        db.create_table(u'directory_emptyroute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transportcompany', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.TransportCompany'])),
            ('route', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Route'])),
            ('truck_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='emptyroute_body_types', to=orm['directory.TruckType'])),
            ('frecuency', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('number_of_trucks', self.gf('django.db.models.fields.IntegerField')()),
            ('reference_price', self.gf('django.db.models.fields.DecimalField')(max_digits=12, decimal_places=2)),
            ('reference_price_currency', self.gf('django.db.models.fields.CharField')(default='CLP', max_length=3)),
        ))
        db.send_create_signal(u'directory', ['EmptyRoute'])

        # Adding M2M table for field equipment on 'EmptyRoute'
        m2m_table_name = db.shorten_name(u'directory_emptyroute_equipment')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('emptyroute', models.ForeignKey(orm[u'directory.emptyroute'], null=False)),
            ('equipment', models.ForeignKey(orm[u'directory.equipment'], null=False))
        ))
        db.create_unique(m2m_table_name, ['emptyroute_id', 'equipment_id'])

        # Adding M2M table for field days on 'EmptyRoute'
        m2m_table_name = db.shorten_name(u'directory_emptyroute_days')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('emptyroute', models.ForeignKey(orm[u'directory.emptyroute'], null=False)),
            ('day', models.ForeignKey(orm[u'directory.day'], null=False))
        ))
        db.create_unique(m2m_table_name, ['emptyroute_id', 'day_id'])

        # Adding model 'TransportCompanyEquipment'
        db.create_table(u'directory_transportcompanyequipment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transportcompany', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['directory.TransportCompany'], unique=True)),
            ('truck', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('trailer', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('semi_trailer', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tractor', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('cargo_van', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('outsource', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('outsource_quantity', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('outsource_names', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('own_gps', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('outsource_gps', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['TransportCompanyEquipment'])

        # Adding M2M table for field truck_body_types on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_truck_body_types')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('trucktype', models.ForeignKey(orm[u'directory.trucktype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'trucktype_id'])

        # Adding M2M table for field truck_equipment on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_truck_equipment')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('equipment', models.ForeignKey(orm[u'directory.equipment'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'equipment_id'])

        # Adding M2M table for field trailer_body_types on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_trailer_body_types')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('trucktype', models.ForeignKey(orm[u'directory.trucktype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'trucktype_id'])

        # Adding M2M table for field trailer_equipment on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_trailer_equipment')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('equipment', models.ForeignKey(orm[u'directory.equipment'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'equipment_id'])

        # Adding M2M table for field semi_trailer_body_types on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_semi_trailer_body_types')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('trucktype', models.ForeignKey(orm[u'directory.trucktype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'trucktype_id'])

        # Adding M2M table for field semi_trailer_equipment on 'TransportCompanyEquipment'
        m2m_table_name = db.shorten_name(u'directory_transportcompanyequipment_semi_trailer_equipment')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('transportcompanyequipment', models.ForeignKey(orm[u'directory.transportcompanyequipment'], null=False)),
            ('equipment', models.ForeignKey(orm[u'directory.equipment'], null=False))
        ))
        db.create_unique(m2m_table_name, ['transportcompanyequipment_id', 'equipment_id'])

        # Adding model 'TransportCompanyStorage'
        db.create_table(u'directory_transportcompanystorage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transportcompany', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['directory.TransportCompany'], unique=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('total_m2', self.gf('django.db.models.fields.IntegerField')()),
            ('built_m2', self.gf('django.db.models.fields.IntegerField')()),
            ('capacidad', self.gf('django.db.models.fields.IntegerField')()),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address_region', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companystorage_address_regions', null=True, to=orm['locationstree.Location'])),
            ('address_commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companystorage_address_communes', null=True, to=orm['locationstree.Location'])),
            ('address_city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address_street', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address_floor', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'directory', ['TransportCompanyStorage'])

        # Adding model 'TruckType'
        db.create_table(u'directory_trucktype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('sort_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'directory', ['TruckType'])

        # Adding model 'Equipment'
        db.create_table(u'directory_equipment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('sort_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'directory', ['Equipment'])

        # Adding model 'UserDirectory'
        db.create_table(u'directory_userdirectory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['tucarga_user.TuCargaUser'], unique=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Company'])),
            ('is_owner', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('companylocation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.CompanyLocation'], null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['UserDirectory'])

        # Adding model 'Freight'
        db.create_table(u'directory_freight', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('obj_type', self.gf('model_utils.fields.StatusField')(default='impo', max_length=100, no_check_for_status=True)),
            ('reason', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('insurance', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('truck_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freight_truck_types', null=True, to=orm['directory.TruckType'])),
            ('equipment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freight_equipments', null=True, to=orm['directory.Equipment'])),
            ('other', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('needs_storage', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('service_conditions', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('userdirectory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.UserDirectory'])),
            ('custom_message_to_transport_companies', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('send_email_to_transport_companies', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('email_to_transport_companies_sent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('email_about_publication_approved_sent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('status', self.gf('model_utils.fields.StatusField')(default='new', max_length=100, no_check_for_status=True)),
        ))
        db.send_create_signal(u'directory', ['Freight'])

        # Adding M2M table for field contacted_transport_companies on 'Freight'
        m2m_table_name = db.shorten_name(u'directory_freight_contacted_transport_companies')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('freight', models.ForeignKey(orm[u'directory.freight'], null=False)),
            ('transportcompany', models.ForeignKey(orm[u'directory.transportcompany'], null=False))
        ))
        db.create_unique(m2m_table_name, ['freight_id', 'transportcompany_id'])

        # Adding model 'FreightLocation'
        db.create_table(u'directory_freightlocation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('freight', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Freight'])),
            ('obj_type', self.gf('model_utils.fields.StatusField')(default='withdraw', max_length=100, no_check_for_status=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightlocation_regions', null=True, to=orm['locationstree.Location'])),
            ('commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='freightlocation_communes', to=orm['locationstree.Location'])),
            ('deposit_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('from_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['FreightLocation'])

        # Adding unique constraint on 'FreightLocation', fields ['freight', 'obj_type']
        db.create_unique(u'directory_freightlocation', ['freight_id', 'obj_type'])

        # Adding model 'FreightWayPoint'
        db.create_table(u'directory_freightwaypoint', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('freight', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Freight'])),
            ('obj_type', self.gf('model_utils.fields.StatusField')(default='origin', max_length=100, no_check_for_status=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightwaypoint_regions', null=True, to=orm['locationstree.Location'])),
            ('commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightwaypoint_communes', null=True, to=orm['locationstree.Location'])),
            ('who_load_unload', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('load_unload_time', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('from_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('ship_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('origin_destination_port', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'directory', ['FreightWayPoint'])

        # Adding model 'ContainerType'
        db.create_table(u'directory_containertype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sort_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'directory', ['ContainerType'])

        # Adding model 'FreightDetail'
        db.create_table(u'directory_freightdetail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('freight', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Freight'])),
            ('obj_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.FreightType'], null=True, blank=True)),
            ('container_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.ContainerType'], null=True, blank=True)),
            ('units', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('weight', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('volume', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'directory', ['FreightDetail'])

        # Adding model 'FreightFirstStep'
        db.create_table(u'directory_freightfirststep', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('obj_type', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('reason', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('insurance', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('truck_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_truck_types', null=True, to=orm['directory.TruckType'])),
            ('equipment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_equipments', null=True, to=orm['directory.Equipment'])),
            ('other', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('needs_storage', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('service_conditions', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('contact_first_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('contact_last_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('userdirectory_position', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('userdirectory_phone', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('userdirectory_mobile', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('company_business_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_business_number', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('company_address_street', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_address_number', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_address_floor', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_address_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='firststep_company_address_regions', null=True, to=orm['locationstree.Location'])),
            ('company_address_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='firststep_company_address_communes', null=True, to=orm['locationstree.Location'])),
            ('company_address_city', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('freightlocation_withdraw_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightlocation_withdraw_regions', null=True, to=orm['locationstree.Location'])),
            ('freightlocation_withdraw_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightlocation_withdraw_communes', null=True, to=orm['locationstree.Location'])),
            ('freightlocation_withdraw_deposit_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('freightlocation_withdraw_from_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightlocation_withdraw_to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightlocation_return_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightlocation_return_regions', null=True, to=orm['locationstree.Location'])),
            ('freightlocation_return_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightlocation_return_communes', null=True, to=orm['locationstree.Location'])),
            ('freightlocation_return_deposit_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('freightlocation_return_from_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightlocation_return_to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightwaypoint_origin_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightwaypoint_origin_regions', null=True, to=orm['locationstree.Location'])),
            ('freightwaypoint_origin_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightwaypoint_origin_communes', null=True, to=orm['locationstree.Location'])),
            ('freightwaypoint_origin_who_load_unload', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightwaypoint_origin_load_unload_time', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('freightwaypoint_origin_from_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightwaypoint_origin_to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightwaypoint_origin_ship_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightwaypoint_origin_origin_destination_port', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightwaypoint_destination_region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightwaypoint_destination_regionss', null=True, to=orm['locationstree.Location'])),
            ('freightwaypoint_destination_commune', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightwaypoint_destination_communes', null=True, to=orm['locationstree.Location'])),
            ('freightwaypoint_destination_who_load_unload', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightwaypoint_destination_load_unload_time', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('freightwaypoint_destination_from_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightwaypoint_destination_to_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('freightwaypoint_destination_ship_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightwaypoint_destination_origin_destination_port', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('freightdetail_0_obj_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.FreightType'])),
            ('freightdetail_0_units', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('freightdetail_0_weight', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_0_volume', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_0_description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('freightdetail_0_container_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.ContainerType'])),
            ('freightdetail_1_obj_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightdetail_1_obj_types', null=True, to=orm['directory.FreightType'])),
            ('freightdetail_1_units', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_1_weight', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_1_volume', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_1_description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('freightdetail_1_container_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightdetail_1_container_types', null=True, to=orm['directory.ContainerType'])),
            ('freightdetail_2_obj_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightdetail_2_obj_types', null=True, to=orm['directory.FreightType'])),
            ('freightdetail_2_units', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_2_weight', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_2_volume', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('freightdetail_2_description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('freightdetail_2_container_type', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='freightfirststep_freightdetail_2_container_types', null=True, to=orm['directory.ContainerType'])),
        ))
        db.send_create_signal(u'directory', ['FreightFirstStep'])

        # Adding model 'Quote'
        db.create_table(u'directory_quote', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('freight', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Freight'])),
            ('userdirectory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.UserDirectory'])),
            ('deposit_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('truck_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='quote_truck_types', to=orm['directory.TruckType'])),
            ('equipment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='quote_equipments', null=True, to=orm['directory.Equipment'])),
            ('units', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('truck_unit_price', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('price_per_ton', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('trip_duration', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('deposit_name_2', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('truck_type_2', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='quote_truck_types_2', null=True, to=orm['directory.TruckType'])),
            ('equipment_2', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='quote_equipments_2', null=True, to=orm['directory.Equipment'])),
            ('units_2', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('truck_unit_price_2', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('price_per_ton_2', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('trip_duration_2', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('free_days', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('billing_unit', self.gf('model_utils.fields.StatusField')(default='ctrday', max_length=100, null=True, no_check_for_status=True, blank=True)),
            ('rate', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('max_stay_per_load_unload', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('freight_aditional_amount', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('freight_aditional_amount_unit', self.gf('model_utils.fields.StatusField')(default='day', max_length=100, no_check_for_status=True)),
            ('freight_underslung_amount', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('freight_underslung_amount_unit', self.gf('model_utils.fields.StatusField')(default=None, max_length=100, null=True, no_check_for_status=True, blank=True)),
            ('deadfreight', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('validity', self.gf('django.db.models.fields.DateField')()),
            ('insurance', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('status', self.gf('model_utils.fields.StatusField')(default='new', max_length=100, no_check_for_status=True)),
            ('confirmation_email_sent', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'directory', ['Quote'])

        # Adding model 'CompanyAndUserRegistration'
        db.create_table(u'directory_companyanduserregistration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('company_type', self.gf('model_utils.fields.StatusField')(default='trans', max_length=100, no_check_for_status=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('company_business_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('company_business_number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('company_address_street', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('company_address_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('company_address_floor', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('company_address_region', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companyanduser_address_regions', null=True, to=orm['locationstree.Location'])),
            ('company_address_commune', self.gf('django.db.models.fields.related.ForeignKey')(related_name='companyanduser_address_communes', null=True, to=orm['locationstree.Location'])),
            ('company_address_city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('company_own_trucks', self.gf('django.db.models.fields.BooleanField')()),
            ('company_own_storage', self.gf('django.db.models.fields.BooleanField')()),
            ('contact_first_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_last_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_position', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_phone', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_mobile', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'directory', ['CompanyAndUserRegistration'])


    def backwards(self, orm):
        # Removing unique constraint on 'FreightLocation', fields ['freight', 'obj_type']
        db.delete_unique(u'directory_freightlocation', ['freight_id', 'obj_type'])

        # Deleting model 'Certification'
        db.delete_table(u'directory_certification')

        # Deleting model 'FreightType'
        db.delete_table(u'directory_freighttype')

        # Deleting model 'Coverage'
        db.delete_table(u'directory_coverage')

        # Removing M2M table for field from_regions on 'Coverage'
        db.delete_table(db.shorten_name(u'directory_coverage_from_regions'))

        # Removing M2M table for field to_regions on 'Coverage'
        db.delete_table(db.shorten_name(u'directory_coverage_to_regions'))

        # Deleting model 'Company'
        db.delete_table(u'directory_company')

        # Removing M2M table for field certifications on 'Company'
        db.delete_table(db.shorten_name(u'directory_company_certifications'))

        # Deleting model 'Route'
        db.delete_table(u'directory_route')

        # Deleting model 'TransportCompany'
        db.delete_table(u'directory_transportcompany')

        # Removing M2M table for field freight_types on 'TransportCompany'
        db.delete_table(db.shorten_name(u'directory_transportcompany_freight_types'))

        # Removing M2M table for field coverage on 'TransportCompany'
        db.delete_table(db.shorten_name(u'directory_transportcompany_coverage'))

        # Deleting model 'FreightInsurance'
        db.delete_table(u'directory_freightinsurance')

        # Deleting model 'CompanyLocation'
        db.delete_table(u'directory_companylocation')

        # Deleting model 'Day'
        db.delete_table(u'directory_day')

        # Deleting model 'EmptyRoute'
        db.delete_table(u'directory_emptyroute')

        # Removing M2M table for field equipment on 'EmptyRoute'
        db.delete_table(db.shorten_name(u'directory_emptyroute_equipment'))

        # Removing M2M table for field days on 'EmptyRoute'
        db.delete_table(db.shorten_name(u'directory_emptyroute_days'))

        # Deleting model 'TransportCompanyEquipment'
        db.delete_table(u'directory_transportcompanyequipment')

        # Removing M2M table for field truck_body_types on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_truck_body_types'))

        # Removing M2M table for field truck_equipment on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_truck_equipment'))

        # Removing M2M table for field trailer_body_types on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_trailer_body_types'))

        # Removing M2M table for field trailer_equipment on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_trailer_equipment'))

        # Removing M2M table for field semi_trailer_body_types on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_semi_trailer_body_types'))

        # Removing M2M table for field semi_trailer_equipment on 'TransportCompanyEquipment'
        db.delete_table(db.shorten_name(u'directory_transportcompanyequipment_semi_trailer_equipment'))

        # Deleting model 'TransportCompanyStorage'
        db.delete_table(u'directory_transportcompanystorage')

        # Deleting model 'TruckType'
        db.delete_table(u'directory_trucktype')

        # Deleting model 'Equipment'
        db.delete_table(u'directory_equipment')

        # Deleting model 'UserDirectory'
        db.delete_table(u'directory_userdirectory')

        # Deleting model 'Freight'
        db.delete_table(u'directory_freight')

        # Removing M2M table for field contacted_transport_companies on 'Freight'
        db.delete_table(db.shorten_name(u'directory_freight_contacted_transport_companies'))

        # Deleting model 'FreightLocation'
        db.delete_table(u'directory_freightlocation')

        # Deleting model 'FreightWayPoint'
        db.delete_table(u'directory_freightwaypoint')

        # Deleting model 'ContainerType'
        db.delete_table(u'directory_containertype')

        # Deleting model 'FreightDetail'
        db.delete_table(u'directory_freightdetail')

        # Deleting model 'FreightFirstStep'
        db.delete_table(u'directory_freightfirststep')

        # Deleting model 'Quote'
        db.delete_table(u'directory_quote')

        # Deleting model 'CompanyAndUserRegistration'
        db.delete_table(u'directory_companyanduserregistration')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'directory.certification': {
            'Meta': {'object_name': 'Certification'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'directory.company': {
            'Meta': {'object_name': 'Company'},
            'activation_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'address_city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'company_address_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_floor': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'address_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'company_address_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_street': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'business_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'business_number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'certifications': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['directory.Certification']", 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'economic_industry': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'economic_sector': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_cargo_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_cargo_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_cargo_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_celular_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_norte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_sur': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_centro_sur_extremo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_clientes': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_comuna': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_contacto_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_direccion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_ejecutivo_que_lo_visito': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_fecha_de_registro_en_el_sitio_web': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_fecha_reunion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_mail_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_nombre_empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_razon_social': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_region': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rm': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rubro_economico': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rut': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_rutas_frecuentes': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sector_eco_revisado': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sitio_web': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_sur_extremo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_telefono_3': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_tipo_de_carga_que_mueve': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_tipo_de_empresa': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_transportistas_con_los_que_trabaja': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_zona_norte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'import_zona_sur': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pilot_group': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'registration_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'new'", 'max_length': '100'}),
            'web_site': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'year_of_creation': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.companyanduserregistration': {
            'Meta': {'object_name': 'CompanyAndUserRegistration'},
            'company_address_city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'company_address_commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companyanduser_address_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'company_address_floor': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'company_address_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'company_address_region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companyanduser_address_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'company_address_street': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'company_business_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'company_business_number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company_own_storage': ('django.db.models.fields.BooleanField', [], {}),
            'company_own_trucks': ('django.db.models.fields.BooleanField', [], {}),
            'company_type': ('model_utils.fields.StatusField', [], {'default': "'trans'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'contact_email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact_first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact_last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact_mobile': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact_position': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'})
        },
        u'directory.companylocation': {
            'Meta': {'object_name': 'CompanyLocation'},
            'address_city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companylocation_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Company']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companylocation_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"})
        },
        u'directory.containertype': {
            'Meta': {'object_name': 'ContainerType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'directory.coverage': {
            'Meta': {'object_name': 'Coverage'},
            'from_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_from_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'to_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_to_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"})
        },
        u'directory.day': {
            'Meta': {'ordering': "['pk']", 'object_name': 'Day'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        },
        u'directory.emptyroute': {
            'Meta': {'object_name': 'EmptyRoute'},
            'days': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['directory.Day']", 'symmetrical': 'False'}),
            'equipment': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'emptyroute_equipments'", 'symmetrical': 'False', 'to': u"orm['directory.Equipment']"}),
            'frecuency': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_of_trucks': ('django.db.models.fields.IntegerField', [], {}),
            'reference_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '12', 'decimal_places': '2'}),
            'reference_price_currency': ('django.db.models.fields.CharField', [], {'default': "'CLP'", 'max_length': '3'}),
            'route': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Route']"}),
            'transportcompany': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.TransportCompany']"}),
            'truck_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'emptyroute_body_types'", 'to': u"orm['directory.TruckType']"})
        },
        u'directory.equipment': {
            'Meta': {'object_name': 'Equipment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'directory.freight': {
            'Meta': {'object_name': 'Freight'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'contacted_transport_companies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['directory.TransportCompany']", 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'custom_message_to_transport_companies': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email_about_publication_approved_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email_to_transport_companies_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'equipment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freight_equipments'", 'null': 'True', 'to': u"orm['directory.Equipment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'needs_storage': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'obj_type': ('model_utils.fields.StatusField', [], {'default': "'impo'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'other': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'send_email_to_transport_companies': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'service_conditions': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'status': ('model_utils.fields.StatusField', [], {'default': "'new'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'truck_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freight_truck_types'", 'null': 'True', 'to': u"orm['directory.TruckType']"}),
            'userdirectory': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.UserDirectory']"})
        },
        u'directory.freightdetail': {
            'Meta': {'object_name': 'FreightDetail'},
            'container_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.ContainerType']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'freight': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Freight']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'obj_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.FreightType']", 'null': 'True', 'blank': 'True'}),
            'units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'volume': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.freightfirststep': {
            'Meta': {'object_name': 'FreightFirstStep'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'company_address_city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_address_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'firststep_company_address_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'company_address_floor': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_address_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_address_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'firststep_company_address_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'company_address_street': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_business_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_business_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'company_phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'contact_first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'contact_last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'equipment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_equipments'", 'null': 'True', 'to': u"orm['directory.Equipment']"}),
            'freightdetail_0_container_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.ContainerType']"}),
            'freightdetail_0_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'freightdetail_0_obj_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.FreightType']"}),
            'freightdetail_0_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'freightdetail_0_volume': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_0_weight': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_1_container_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightdetail_1_container_types'", 'null': 'True', 'to': u"orm['directory.ContainerType']"}),
            'freightdetail_1_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'freightdetail_1_obj_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightdetail_1_obj_types'", 'null': 'True', 'to': u"orm['directory.FreightType']"}),
            'freightdetail_1_units': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_1_volume': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_1_weight': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_2_container_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightdetail_2_container_types'", 'null': 'True', 'to': u"orm['directory.ContainerType']"}),
            'freightdetail_2_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'freightdetail_2_obj_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightdetail_2_obj_types'", 'null': 'True', 'to': u"orm['directory.FreightType']"}),
            'freightdetail_2_units': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_2_volume': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightdetail_2_weight': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freightlocation_return_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightlocation_return_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightlocation_return_deposit_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'freightlocation_return_from_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightlocation_return_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightlocation_return_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightlocation_return_to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightlocation_withdraw_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightlocation_withdraw_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightlocation_withdraw_deposit_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'freightlocation_withdraw_from_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightlocation_withdraw_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightlocation_withdraw_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightlocation_withdraw_to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightwaypoint_destination_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightwaypoint_destination_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightwaypoint_destination_from_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightwaypoint_destination_load_unload_time': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'freightwaypoint_destination_origin_destination_port': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'freightwaypoint_destination_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightwaypoint_destination_regionss'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightwaypoint_destination_ship_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'freightwaypoint_destination_to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightwaypoint_destination_who_load_unload': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'freightwaypoint_origin_commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightwaypoint_origin_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightwaypoint_origin_from_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightwaypoint_origin_load_unload_time': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'freightwaypoint_origin_origin_destination_port': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'freightwaypoint_origin_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_freightwaypoint_origin_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freightwaypoint_origin_ship_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'freightwaypoint_origin_to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'freightwaypoint_origin_who_load_unload': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'needs_storage': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'obj_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'other': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'service_conditions': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'truck_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightfirststep_truck_types'", 'null': 'True', 'to': u"orm['directory.TruckType']"}),
            'userdirectory_mobile': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'userdirectory_phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'userdirectory_position': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'directory.freightinsurance': {
            'Meta': {'object_name': 'FreightInsurance'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance_company': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'transportcompany': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.TransportCompany']"}),
            'validity': ('django.db.models.fields.DateField', [], {})
        },
        u'directory.freightlocation': {
            'Meta': {'unique_together': "(('freight', 'obj_type'),)", 'object_name': 'FreightLocation'},
            'commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'freightlocation_communes'", 'to': u"orm['locationstree.Location']"}),
            'deposit_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'freight': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Freight']"}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'obj_type': ('model_utils.fields.StatusField', [], {'default': "'withdraw'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightlocation_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.freighttype': {
            'Meta': {'object_name': 'FreightType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'directory.freightwaypoint': {
            'Meta': {'object_name': 'FreightWayPoint'},
            'commune': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightwaypoint_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'freight': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Freight']"}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'load_unload_time': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'obj_type': ('model_utils.fields.StatusField', [], {'default': "'origin'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'origin_destination_port': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'freightwaypoint_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'ship_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'who_load_unload': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        u'directory.quote': {
            'Meta': {'object_name': 'Quote'},
            'billing_unit': ('model_utils.fields.StatusField', [], {'default': "'ctrday'", 'max_length': '100', 'null': 'True', u'no_check_for_status': 'True', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'confirmation_email_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'deadfreight': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'deposit_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'deposit_name_2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'equipment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'quote_equipments'", 'null': 'True', 'to': u"orm['directory.Equipment']"}),
            'equipment_2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'quote_equipments_2'", 'null': 'True', 'to': u"orm['directory.Equipment']"}),
            'free_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freight': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Freight']"}),
            'freight_aditional_amount': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'freight_aditional_amount_unit': ('model_utils.fields.StatusField', [], {'default': "'day'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'freight_underslung_amount': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freight_underslung_amount_unit': ('model_utils.fields.StatusField', [], {'default': 'None', 'max_length': '100', 'null': 'True', u'no_check_for_status': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'max_stay_per_load_unload': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'price_per_ton': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'price_per_ton_2': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('model_utils.fields.StatusField', [], {'default': "'new'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'trip_duration': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'trip_duration_2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'truck_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'quote_truck_types'", 'to': u"orm['directory.TruckType']"}),
            'truck_type_2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'quote_truck_types_2'", 'null': 'True', 'to': u"orm['directory.TruckType']"}),
            'truck_unit_price': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'truck_unit_price_2': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'units': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'units_2': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'userdirectory': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.UserDirectory']"}),
            'validity': ('django.db.models.fields.DateField', [], {})
        },
        u'directory.route': {
            'Meta': {'object_name': 'Route'},
            'from_commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'route_from_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'to_commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'route_to_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"})
        },
        u'directory.transportcompany': {
            'LTL_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'TransportCompany'},
            'company': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['directory.Company']", 'unique': 'True'}),
            'coverage': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['directory.Coverage']", 'symmetrical': 'False'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'do_international_trips': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'empty_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'freight_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['directory.FreightType']", 'symmetrical': 'False'}),
            'full_load_trips_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'international_trips_from_1': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_from_2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_from_3': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_1': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'international_trips_to_3': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'number_of_trips_per_month': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'directory.transportcompanyequipment': {
            'Meta': {'object_name': 'TransportCompanyEquipment'},
            'cargo_van': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'outsource': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'outsource_gps': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'outsource_names': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'outsource_quantity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'own_gps': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'semi_trailer': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'semi_trailer_body_types': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_semi_trailer_body_types'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.TruckType']"}),
            'semi_trailer_equipment': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_semitrailer_equipments'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.Equipment']"}),
            'tractor': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'trailer': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'trailer_body_types': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_trailer_body_types'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.TruckType']"}),
            'trailer_equipment': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_trailer_equipments'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.Equipment']"}),
            'transportcompany': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['directory.TransportCompany']", 'unique': 'True'}),
            'truck': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'truck_body_types': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_truck_body_types'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.TruckType']"}),
            'truck_equipment': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'transportcompanyequipment_truck_equipments'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['directory.Equipment']"})
        },
        u'directory.transportcompanystorage': {
            'Meta': {'object_name': 'TransportCompanyStorage'},
            'address_city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'address_commune': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companystorage_address_communes'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_floor': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'address_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'address_region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'companystorage_address_regions'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            'address_street': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'built_m2': ('django.db.models.fields.IntegerField', [], {}),
            'capacidad': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_m2': ('django.db.models.fields.IntegerField', [], {}),
            'transportcompany': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['directory.TransportCompany']", 'unique': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'directory.trucktype': {
            'Meta': {'object_name': 'TruckType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'directory.userdirectory': {
            'Meta': {'object_name': 'UserDirectory'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Company']"}),
            'companylocation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.CompanyLocation']", 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_owner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['tucarga_user.TuCargaUser']", 'unique': 'True'})
        },
        u'locationstree.location': {
            'Meta': {'object_name': 'Location'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'tucarga_user.tucargauser': {
            'Meta': {'object_name': 'TuCargaUser'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"})
        }
    }

    complete_apps = ['directory']
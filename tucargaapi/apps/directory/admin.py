# encoding: utf-8

from django import forms
from django.contrib import admin

from salmonella.admin import SalmonellaMixin

from tucargaapi.apps.directory.models import (
    Company,
    CompanyAndUserRegistration,
    CompanyLocation,
    ContainerType,
    Coverage,
    Freight,
    FreightCompany,
    FreightCompanyType,
    FreightDetail,
    FreightLocation,
    FreightInsurance,
    FreightWayPoint,
    TransportCompany,
    TransportCompanyStorage,
    UserDirectory,
    FreightFirstStep,
    Quote,
    TruckType,
    Equipment,
    TransportCompanyEquipment,
    Route,
    EmptyRoute,
    Certification)


# forms

class TransportCompanyFrom(forms.ModelForm):

    freight_types = forms.MultipleChoiceField(
        label='Tipos de carga',
        widget=forms.SelectMultiple,
        choices=FreightDetail.OBJ_TYPES,
        required=False)

    def clean_freight_types(self):
        field = []
        for data in self.cleaned_data['freight_types']:
            field.append(str(data))
        field = ','.join([f for f in field])
        return field


# inlines

class CompanyLocationInline(admin.TabularInline):

    model = CompanyLocation
    extra = 1


class TransportCompanyEquipmentInline(admin.StackedInline):

    model = TransportCompanyEquipment
    max_num = 1


class EmptyRouteInline(admin.TabularInline):

    model = EmptyRoute
    extra = 1


class FreightInsuranceInline(admin.TabularInline):

    model = FreightInsurance
    extra = 1


class TransportCompanyStorageInline(admin.StackedInline):

    model = TransportCompanyStorage
    extra = 1


class QuoteInlineForm(forms.ModelForm):

    class Meta:
        model = Quote


class QuoteInline(SalmonellaMixin, admin.StackedInline):

    form = QuoteInlineForm
    model = Quote
    extra = 0

    salmonella_fields = ('userdirectory', )

    fieldsets = (
        ('Información general', {
            'fields': (
                'userdirectory',
                'validity',
                'insurance',
                'comments', ), }, ),
        ('Cotización flete 1', {
            'fields': (
                'deposit_name',
                'truck_type',
                'equipment',
                'units',
                'truck_unit_price',
                'price_per_ton',
                'trip_duration', ), }, ),
        ('Cotización flete 2', {
            'fields': (
                'deposit_name_2',
                'truck_type_2',
                'equipment_2',
                'units_2',
                'truck_unit_price_2',
                'price_per_ton_2',
                'trip_duration_2', ), }, ),
        ('Almacenaje', {
            'fields': (
                'free_days',
                'billing_unit',
                'rate', ), }, ),
        ('En caso de exceder tiempo de estadía', {
            'fields': (
                'max_stay_per_load_unload',
                'freight_aditional_amount',
                'freight_aditional_amount_unit',
                'freight_underslung_amount',
                'freight_underslung_amount_unit',
                'deadfreight', ), }, ),
        ('Uso interno', {
            'fields': (
                'status',
                'confirmation_email_sent',
                'allotted', ), }, ),
        )


# model admins


class FreightLocationInline(admin.TabularInline):

    model = FreightLocation
    max_num = 2
    verbose_name = 'Retiro/Devolución contenedor vacío'
    verbose_name_plural = 'Retiro/Devolución contenedor vacío'

    def get_extra(self, request, obj=None, **kwargs):
        """Show one form if creating an obj and cero extra if allready exists

        """
        if obj:
            return 0
        return 1


class FreightWayPointInline(admin.TabularInline):

    model = FreightWayPoint
    verbose_name = 'Origen/Destino'
    verbose_name_plural = 'Origen/Destino'

    def get_extra(self, request, obj=None, **kwargs):
        """Show one form if creating an obj and cero extra if allready exists

        """
        if obj:
            return 0
        return 1


class FreightDetailInline(admin.TabularInline):

    model = FreightDetail
    verbose_name = 'Descripción de la carga'
    verbose_name_plural = 'Descripción de las cargas'

    def get_extra(self, request, obj=None, **kwargs):
        """Show one form if creating an obj and cero extra if allready exists

        """
        if obj:
            return 0
        return 1


class FreightTypeFilter(admin.SimpleListFilter):
    title = 'Tipo de carga'

    parameter_name = 'freight_types'

    def lookups(self, request, model_admin):
        return FreightDetail.OBJ_TYPES

    def queryset(self, request, queryset):
        if self.value():
            qs = queryset.filter(
                freight_types__contains=self.value())
            return qs


class TransportCompanyAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('company', )
        }),
        ('Detalle de operaciones', {
            'fields': ('number_of_trips_per_month',
                       'full_load_trips_percentage',
                       'LTL_trips_percentage',
                       'freight_types',)
        }),
        ('Cobertura y rutas frecuentes', {
            'fields': ('coverage',
                       'do_international_trips',
                       'international_trips_from_1',
                       'international_trips_to_1',
                       'international_trips_from_2',
                       'international_trips_to_2',
                       'international_trips_from_3',
                       'international_trips_to_3',)
        }),
        ('Viajes vacios', {
            'fields': ('empty_trips_percentage',)
        }),
    )

    inlines = [
        EmptyRouteInline,
        TransportCompanyEquipmentInline,
        FreightInsuranceInline,
        TransportCompanyStorageInline, ]

    # list_filter = ('coverage', 'freight_types')
    list_filter = ('coverage', FreightTypeFilter)

    form = TransportCompanyFrom


# class UserDirectoryInline(admin.TabularInline):

#     model = UserDirectory
#     extra = 1


def get_company_readonly_fields():
    readonly_fields = []
    for f in Company._meta.fields:
        if f.name.startswith('import_'):
            readonly_fields.append(f.name)
    return readonly_fields


class CompanyAdmin(admin.ModelAdmin):

    readonly_fields = get_company_readonly_fields()
    inlines = [
        # needs a location that needs a company so does not worth
        # puting it in the create form
        # UserDirectoryInline,
        CompanyLocationInline, ]

    search_fields = ['business_number', 'name', ]
    list_display = ['business_number', 'name']


class FreightAdmin(SalmonellaMixin, admin.ModelAdmin):

    inlines = [FreightLocationInline,
               FreightWayPointInline,
               FreightDetailInline,
               QuoteInline, ]
    list_display = ['pk', 'get_status_display', 'userdirectory']

    salmonella_fields = ('contacted_transport_companies', 'userdirectory')


class UserDirectoryAdmin(admin.ModelAdmin):

    list_display = ['user', 'company']
    search_fields = ['user__email', 'company__name',
                     'company__business_number']


class QuoteAdmin(SalmonellaMixin, admin.ModelAdmin):

    list_display = ['freight', 'userdirectory']
    ordering = ['-freight']
    salmonella_fields = ('userdirectory', )


class EmptyRouteAdmin(admin.ModelAdmin):

    list_display = ['transportcompany', 'route']


class CompanyLocationAdmin(admin.ModelAdmin):

    list_display = ['company', 'region', 'commune']


class FreightFirstStepAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Tipo de sercicio', {
            'fields': ('obj_type',
                       'reason',)}),
        ('Información de contacto', {
            'fields': ('contact_email',
                       'contact_first_name',
                       'contact_last_name', )}),
        ('Información de la empresa', {
            'fields': ('company_name',
                       'company_business_name',
                       'company_business_number',
                       'company_address_street',
                       'company_address_number',
                       'company_address_floor',
                       'company_address_region',
                       'company_address_commune',
                       'company_address_city', )}),
        ('Retiro contenedor vacío', {
            'fields': ('freightlocation_withdraw_commune',
                       'freightlocation_withdraw_deposit_name',
                       'freightlocation_withdraw_from_date',
                       'freightlocation_withdraw_to_date', )}),
        ('Origen', {
            'fields': ('freightwaypoint_origin_region',
                       'freightwaypoint_origin_commune',
                       'freightwaypoint_origin_who_load_unload',
                       'freightwaypoint_origin_load_unload_time',
                       'freightwaypoint_origin_from_date',
                       'freightwaypoint_origin_to_date',
                       'freightwaypoint_origin_ship_name',
                       'freightwaypoint_origin_origin_destination_port',)}),
        ('Destino', {
            'fields': ('freightwaypoint_destination_region',
                       'freightwaypoint_destination_commune',
                       'freightwaypoint_destination_who_load_unload',
                       'freightwaypoint_destination_load_unload_time',
                       'freightwaypoint_destination_from_date',
                       'freightwaypoint_destination_to_date',
                       'freightwaypoint_destination_ship_name',
                       'freightwaypoint_destination_origin_destination_port',)}),
        ('Devolución contenedor vacío', {
            'fields': ('freightlocation_return_commune',
                       'freightlocation_return_deposit_name',
                       'freightlocation_return_from_date',
                       'freightlocation_return_to_date',)}),
        ('Detalle de la carga', {
            'fields': ('freightdetail_0_obj_type',
                       'freightdetail_0_units',
                       'freightdetail_0_weight',
                       'freightdetail_0_volume',
                       'freightdetail_0_description',
                       'freightdetail_0_container_type',
                       'freightdetail_1_obj_type',
                       'freightdetail_1_units',
                       'freightdetail_1_weight',
                       'freightdetail_1_volume',
                       'freightdetail_1_description',
                       'freightdetail_1_container_type',
                       'freightdetail_2_obj_type',
                       'freightdetail_2_units',
                       'freightdetail_2_weight',
                       'freightdetail_2_volume',
                       'freightdetail_2_description',
                       'freightdetail_2_container_type',
                       )}),
        ('¿Tiene algún requerimiento específico de transporte?', {
            'fields': ('insurance',
                       'truck_type',
                       'equipment',
                       'other',
                       'needs_storage',
                       'service_conditions',
                       'comments',
                       )}),
    )


class CompanyAndUserRegistrationAdmin(admin.ModelAdmin):

    list_display = ('company_name', )
    fieldsets = (
        ('Datos empresa', {
            'fields': ('company_type',
                       'company_name',
                       'company_business_name',
                       'company_business_number',
                       'company_address_street',
                       'company_address_number',
                       'company_address_floor',
                       'company_address_region',
                       'company_address_commune',
                       'company_address_city',
                       'company_own_trucks',
                       'company_own_storage', )
        }),
        ('Datos personales', {
            'fields': ('contact_first_name',
                       'contact_last_name',
                       'contact_position',
                       'contact_email',
                       'contact_phone',
                       'contact_mobile', )
        }), )


class ContainerTypeAdmin(admin.ModelAdmin):

    list_display = ('name', 'sort_order')
    list_editable = ('sort_order', )


class TruckTypeAdmin(admin.ModelAdmin):

    list_display = ('name', 'sort_order')
    list_editable = ('sort_order', )


class EquipmentAdmin(admin.ModelAdmin):

    list_display = ('name', 'sort_order')
    list_editable = ('sort_order', )


admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyAndUserRegistration,
                    CompanyAndUserRegistrationAdmin)
admin.site.register(CompanyLocation, CompanyLocationAdmin)
admin.site.register(ContainerType, ContainerTypeAdmin)
admin.site.register(Coverage)
admin.site.register(Freight, FreightAdmin)
admin.site.register(FreightCompany)
admin.site.register(FreightCompanyType)
admin.site.register(TransportCompany, TransportCompanyAdmin)
admin.site.register(UserDirectory, UserDirectoryAdmin)
admin.site.register(FreightFirstStep, FreightFirstStepAdmin)
admin.site.register(Quote, QuoteAdmin)
admin.site.register(TruckType, TruckTypeAdmin)
admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(TransportCompanyEquipment)
admin.site.register(Route)
admin.site.register(EmptyRoute, EmptyRouteAdmin)
admin.site.register(Certification)

from django.db import models

from custom_user.models import AbstractEmailUser
from model_utils.models import TimeStampedModel


class TuCargaUser(AbstractEmailUser, TimeStampedModel):
    """Custom user model with email as username
    """
    first_name = models.CharField('Nombre', max_length=30, blank=True)
    last_name = models.CharField('Apellido', max_length=30, blank=True)

    @property
    def full_name(self):
        return ' '.join((self.first_name, self.last_name)).strip()

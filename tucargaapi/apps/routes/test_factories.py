import factory

from tucargaapi.apps.directory import test_factories as directory_factories

from . import models


class RoutePlaceFactory(factory.DjangoModelFactory):

    FACTORY_FOR = models.RoutePlace

    name = factory.Sequence(lambda n: 'place %d' % n)


class RouteBaseFactory(factory.django.DjangoModelFactory):
    zone = factory.SubFactory(directory_factories.CoverageFactory)

    port = factory.SubFactory(RoutePlaceFactory)
    client = factory.SubFactory(RoutePlaceFactory)
    deposit = factory.SubFactory(RoutePlaceFactory)


class RouteExportFactory(RouteBaseFactory):

    FACTORY_FOR = models.RouteExport


class RouteImportFactory(RouteBaseFactory):

    FACTORY_FOR = models.RouteImport

# encoding: utf-8

from django.db import models


class RouteBase(models.Model):
    zone = models.ForeignKey('directory.Coverage',
                             null=True,
                             verbose_name='Zona')

    class Meta:
        abstract = True


class RoutePlace(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        verbose_name = 'Lugar'
        verbose_name_plural = 'Lugares'

    def __unicode__(self):
        return self.name


class RouteExport(RouteBase):

    deposit = models.ForeignKey(
        RoutePlace,
        verbose_name='Depósito',
        related_name='routeexport_desposits',
        null=True,
        blank=True)
    client = models.ForeignKey(
        RoutePlace,
        verbose_name='cliente',
        related_name='routeexport_clients')
    port = models.ForeignKey(
        RoutePlace,
        verbose_name='puerto',
        related_name='routeexport_ports')

    class Meta:
        unique_together = ('deposit', 'client', 'port')
        verbose_name = 'ruta exportación'
        verbose_name_plural = 'rutas exportación'

    def __unicode__(self):
        return u'%s/%s/%s' % (self.deposit, self.client, self.port)


class RouteImport(RouteBase):

    port = models.ForeignKey(
        RoutePlace,
        verbose_name='puerto',
        related_name='routeimport_ports')
    client = models.ForeignKey(
        RoutePlace,
        verbose_name='cliente',
        related_name='routeimport_clients')
    deposit = models.ForeignKey(
        RoutePlace,
        verbose_name='Depósito',
        related_name='routeimport_desposits',
        null=True,
        blank=True)

    class Meta:
        unique_together = ('port', 'client', 'deposit')
        verbose_name = 'ruta importación'
        verbose_name_plural = 'rutas importación'

    def __unicode__(self):
        return u'%s/%s/%s' % (self.port, self.client, self.deposit)

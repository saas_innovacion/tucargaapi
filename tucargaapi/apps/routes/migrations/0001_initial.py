# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RoutePlace'
        db.create_table(u'routes_routeplace', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
        ))
        db.send_create_signal(u'routes', ['RoutePlace'])

        # Adding model 'RouteExport'
        db.create_table(u'routes_routeexport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Coverage'], null=True)),
            ('deposit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='routeexport_desposits', null=True, to=orm['routes.RoutePlace'])),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='routeexport_clients', to=orm['routes.RoutePlace'])),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(related_name='routeexport_ports', to=orm['routes.RoutePlace'])),
        ))
        db.send_create_signal(u'routes', ['RouteExport'])

        # Adding unique constraint on 'RouteExport', fields ['deposit', 'client', 'port']
        db.create_unique(u'routes_routeexport', ['deposit_id', 'client_id', 'port_id'])

        # Adding model 'RouteImport'
        db.create_table(u'routes_routeimport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['directory.Coverage'], null=True)),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(related_name='routeimport_ports', to=orm['routes.RoutePlace'])),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='routeimport_clients', to=orm['routes.RoutePlace'])),
            ('deposit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='routeimport_desposits', null=True, to=orm['routes.RoutePlace'])),
        ))
        db.send_create_signal(u'routes', ['RouteImport'])

        # Adding unique constraint on 'RouteImport', fields ['port', 'client', 'deposit']
        db.create_unique(u'routes_routeimport', ['port_id', 'client_id', 'deposit_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'RouteImport', fields ['port', 'client', 'deposit']
        db.delete_unique(u'routes_routeimport', ['port_id', 'client_id', 'deposit_id'])

        # Removing unique constraint on 'RouteExport', fields ['deposit', 'client', 'port']
        db.delete_unique(u'routes_routeexport', ['deposit_id', 'client_id', 'port_id'])

        # Deleting model 'RoutePlace'
        db.delete_table(u'routes_routeplace')

        # Deleting model 'RouteExport'
        db.delete_table(u'routes_routeexport')

        # Deleting model 'RouteImport'
        db.delete_table(u'routes_routeimport')


    models = {
        u'directory.coverage': {
            'Meta': {'object_name': 'Coverage'},
            'from_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_from_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'to_regions': ('mptt.fields.TreeManyToManyField', [], {'related_name': "'coverage_to_regions'", 'symmetrical': 'False', 'to': u"orm['locationstree.Location']"})
        },
        u'locationstree.location': {
            'Meta': {'object_name': 'Location'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['locationstree.Location']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'routes.routeexport': {
            'Meta': {'unique_together': "(('deposit', 'client', 'port'),)", 'object_name': 'RouteExport'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeexport_clients'", 'to': u"orm['routes.RoutePlace']"}),
            'deposit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'routeexport_desposits'", 'null': 'True', 'to': u"orm['routes.RoutePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeexport_ports'", 'to': u"orm['routes.RoutePlace']"}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Coverage']", 'null': 'True'})
        },
        u'routes.routeimport': {
            'Meta': {'unique_together': "(('port', 'client', 'deposit'),)", 'object_name': 'RouteImport'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeimport_clients'", 'to': u"orm['routes.RoutePlace']"}),
            'deposit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'routeimport_desposits'", 'null': 'True', 'to': u"orm['routes.RoutePlace']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'routeimport_ports'", 'to': u"orm['routes.RoutePlace']"}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['directory.Coverage']", 'null': 'True'})
        },
        u'routes.routeplace': {
            'Meta': {'object_name': 'RoutePlace'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['routes']
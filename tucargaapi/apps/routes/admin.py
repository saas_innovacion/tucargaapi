from django.contrib import admin

from .models import RouteExport, RouteImport


class RouteExportAdmin(admin.ModelAdmin):

    list_display = ('deposit', 'client', 'port', 'zone')


class RouteImportAdmin(admin.ModelAdmin):

    list_display = ('port', 'client', 'deposit', 'zone')


admin.site.register(RouteExport, RouteExportAdmin)
admin.site.register(RouteImport, RouteImportAdmin)

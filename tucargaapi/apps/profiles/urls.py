from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import (CompanyProfileUpdateView,
                    TransportCompanyProfileView,
                    TransportCompanyProfileUpdateView)

urlpatterns = patterns(
    '',
    url(r'^transportcompany/$',
        login_required(TransportCompanyProfileView.as_view()),
        name='profile-transportcompany'),
    url(r'^company/(?P<pk>\d+)/$',
        login_required(CompanyProfileUpdateView.as_view()),
        name='profile-company-update'),
    url(r'^transportcompany/(?P<pk>\d+)/$',
        login_required(TransportCompanyProfileUpdateView.as_view()),
        name='profile-transportcompany-update'),

)

from django.core.urlresolvers import reverse
from django.test import TestCase

from locationstree import levels
from locationstree.models import Location
from django_nopassword.models import LoginCode
from rest_framework.test import APIClient

from tucargaapi.apps.directory import test_factories as directory_factories
from tucargaapi.apps.rates import test_factories as rates_factories
from tucargaapi.apps.routes import test_factories as routes_factories

import utils

client = APIClient()

USER_CREDENTIALS = {'email': 'j@doe.com', 'password': ''}


class ProfileTestCaseBase(TestCase):

    fixtures = ['fixtures/sites.json', 'fixtures/locations.json']

    def setUp(self):
        self.client = client
        self.user = directory_factories.UserFactory(**USER_CREDENTIALS)
        self.login_code = LoginCode.create_code_for_user(self.user)
        self.company = directory_factories.CompanyFactory()
        directory_factories.UserDirectoryFactory(
            user=self.user, company=self.company)
        from_regions = Location.objects.filter(
            level=levels.CHILE['region'])[:1]
        to_regions = Location.objects.filter(level=levels.CHILE['region'])[14:]
        self.coverage = directory_factories.CoverageFactory(
            from_regions=from_regions,
            to_regions=to_regions)
        self.transportcompany = directory_factories.TransportCompanyFactory(
            company=self.company,
            coverage=[self.coverage])

    def login(self, login_code=None, email=None):
        code = login_code and login_code.code or self.login_code.code
        email = email or self.user.email
        result = client.login(code=code, email=email)
        self.assertTrue(result, msg='%s: %s' % (code, email))


class TransportCompanyProfileViewTests(ProfileTestCaseBase):

    def test_get(self):
        url = reverse('profile-transportcompany')
        directory_factories.TransportCompanyFactory(company=self.company)
        self.login()

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertIn('transportcompany', response.context)
        self.assertIn('companyprofile_form', response.context)
        self.assertIn('transportcompanyprofile_form', response.context)
        self.assertIn('exportroute_forms', response.context)
        self.assertIn('has_rateexport', response.context)
        self.assertIn('importroute_forms', response.context)
        self.assertIn('has_rateimport', response.context)


class UtilsTests(ProfileTestCaseBase):

    def test_get_export_routes(self):
        from_regions = Location.objects.filter(
            level=levels.CHILE['region'])[:2]
        to_regions = Location.objects.filter(level=levels.CHILE['region'])[13:]

        for from_region in from_regions:
            for to_region in to_regions:
                for from_commune in from_region.children.all():
                    for to_commune in to_region.children.all():
                        routes_factories.RouteExportFactory(
                            client=from_commune,
                            port=to_commune)

        routes = utils.get_exportroutes_by_transportcompany(
            self.transportcompany)

        self.assertEqual(len(routes[self.coverage.name]), 44)

    def test_get_import_routes(self):
        from_regions = Location.objects.filter(
            level=levels.CHILE['region'])[:2]
        to_regions = Location.objects.filter(level=levels.CHILE['region'])[13:]

        for from_region in from_regions:
            for to_region in to_regions:
                for from_commune in from_region.children.all():
                    for to_commune in to_region.children.all():
                        routes_factories.RouteImportFactory(
                            port=from_commune,
                            client=to_commune)

        routes = utils.get_importroutes_by_transportcompany(
            self.transportcompany)

        self.assertEqual(len(routes[self.coverage.name]), 44)


class RegressionTests(ProfileTestCaseBase):

    def test_utils_get_forms_MultipleObjectsReturned(self):
        """
        Traceback (most recent call last):
        ...
        File "/app/tucargaapi/apps/profiles/views.py", line 39, in get_context_data
            transportcompany)
        File "/app/tucargaapi/apps/profiles/utils.py", line 67, in get_importrates_forms
            RateImportForm)
        File "/app/tucargaapi/apps/profiles/utils.py", line 36, in get_forms
            route=route)
        ...
        MultipleObjectsReturned: get() returned more than one RateImport -- it returned 2!
        """
        url = reverse('profile-transportcompany')
        route = routes_factories.RouteImportFactory(zone=self.coverage)
        rates_factories.RateImportFactory(
            route=route,
            transportcompany=self.transportcompany)
        rates_factories.RateImportFactory(
            route=route,
            transportcompany=self.transportcompany)
        self.login()

        response = self.client.get(url)

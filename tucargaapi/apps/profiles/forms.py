# encoding: utf-8

from django import forms

from tucargaapi.apps.directory.models import (Company, Coverage, TransportCompany,
                                              FreightDetail)


class CompanyProfileForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = ('logo', 'name', 'business_name', 'year_of_creation', 'phone')


class TransportCompanyProfileForm(forms.ModelForm):

    coverage = forms.ModelMultipleChoiceField(
        label='Cobertura',
        widget=forms.CheckboxSelectMultiple,
        queryset=Coverage.objects.all(),
        required=False)

    freight_types = forms.MultipleChoiceField(
        label='Tipos de carga',
        widget=forms.CheckboxSelectMultiple,
        choices=FreightDetail.OBJ_TYPES,
        required=False)

    class Meta:
        model = TransportCompany
        fields = ('number_of_trips_per_month', 'coverage', 'freight_types')

    def clean_freight_types(self):
        field = []
        for data in self.cleaned_data['freight_types']:
            field.append(str(data))
        field = ','.join([f for f in field])
        return field

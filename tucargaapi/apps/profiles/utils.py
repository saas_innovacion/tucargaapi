from tucargaapi.apps.routes.models import RouteExport, RouteImport
from tucargaapi.apps.rates.models import RateExport, RateImport
from tucargaapi.apps.rates.forms import RateExportForm, RateImportForm


def get_routes(transportcompany, model):
    routes_by_zone = {}
    coverage = transportcompany.coverage.all()
    for zone in coverage:
        routes = model.objects.filter(zone=zone)
        if routes.count():
            routes_by_zone[zone] = routes
    return routes_by_zone


def get_exportroutes_by_transportcompany(transportcompany):
    routes = get_routes(transportcompany, RouteExport)
    return routes


def get_importroutes_by_transportcompany(transportcompany):
    routes = get_routes(transportcompany, RouteImport)
    return routes


def get_forms(transportcompany, routes, model, form_class):
    zones = []
    has_rates = False
    for zone in routes.keys():
        forms = []
        for route in routes[zone]:
            initial = {'route': route}
            try:
                instance = model.objects.filter(
                    transportcompany=transportcompany,
                    route=route).latest('modified')
                has_rates = True
            except model.DoesNotExist:
                instance = None
            if instance:
                form = form_class(instance=instance)
            else:
                form = form_class(initial=initial)
            forms.append((route, form))
        zones.append((zone, forms))
    return zones, has_rates


def get_exportrates_forms(transportcompany):
    export_routes = get_exportroutes_by_transportcompany(
        transportcompany)
    exportroute_forms, has_rateexport = get_forms(
        transportcompany,
        export_routes,
        RateExport,
        RateExportForm)
    return exportroute_forms, has_rateexport


def get_importrates_forms(transportcompany):
    import_routes = get_importroutes_by_transportcompany(
        transportcompany)
    importroute_forms, has_rateimport = get_forms(
        transportcompany,
        import_routes,
        RateImport,
        RateImportForm)
    return importroute_forms, has_rateimport

from django.core.urlresolvers import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView

from tucargaapi.apps.directory.models import Company, TransportCompany

from .forms import CompanyProfileForm, TransportCompanyProfileForm
import utils


class TransportCompanyProfileView(TemplateView):

    template_name = 'profile_transportcompany.html'

    def get_context_data(self, **kwargs):
        context = super(TransportCompanyProfileView,
                        self).get_context_data(**kwargs)
        company = self.request.user.userdirectory.company
        transportcompany = company.transportcompany
        context['transportcompany'] = transportcompany

        # get company profile form
        companyprofile_form = CompanyProfileForm(instance=company)
        context['companyprofile_form'] = companyprofile_form

        # get transportcompany profile form
        transportcompanyprofile_form = TransportCompanyProfileForm(
            instance=transportcompany)
        context['transportcompanyprofile_form'] = transportcompanyprofile_form

        # get export forms
        exportroute_forms, has_rateexport = utils.get_exportrates_forms(
            transportcompany)
        context['exportroute_forms'] = exportroute_forms
        context['has_rateexport'] = has_rateexport

        # get import forms
        importroute_forms, has_rateimport = utils.get_importrates_forms(
            transportcompany)

        context['importroute_forms'] = importroute_forms
        context['has_rateimport'] = has_rateimport

        return context


class CompanyProfileUpdateView(UpdateView):

    template_name = 'profile_company_update.html'
    model = Company
    form_class = CompanyProfileForm
    success_url = reverse_lazy('profile-transportcompany')


class TransportCompanyProfileUpdateView(UpdateView):

    template_name = 'profile_transportcompany_update.html'
    model = TransportCompany
    form_class = TransportCompanyProfileForm
    success_url = reverse_lazy('profile-transportcompany')

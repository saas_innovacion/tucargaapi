from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.conf import settings

# See: https://docs.djangoproject.com/en/dev/ref/contrib/admin/#hooking-adminsite-instances-into-your-urlconf
admin.autodiscover()

# See: https://docs.djangoproject.com/en/dev/topics/http/urls/
urlpatterns = patterns(
    '',
    # Django NoPassWord
    url(r'^accounts/login-code/(?P<login_code>[a-zA-Z0-9]+)/$',
        'django_nopassword.views.login_with_code',
        name='login-with-code'),
    url(r'^accounts/logout/$',
        'django_nopassword.views.logout',
        name='logout',
        kwargs={
            'redirect_to': settings.SUCCESS_URL.url}),

    # Local apps
    url(r'^directory/', include('tucargaapi.apps.directory.urls')),
    url(r'^rates/', include('tucargaapi.apps.rates.urls')),
    url(r'^profiles/', include('tucargaapi.apps.profiles.urls')),

    # Admin panel and documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/salmonella/', include('salmonella.urls')),
)

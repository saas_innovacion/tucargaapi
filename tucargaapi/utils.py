from django_nopassword.backends import EmailBackend


class NoPasswordNoDeleteBackend(EmailBackend):

    def post_authenticate_action(self, login_code):
        pass

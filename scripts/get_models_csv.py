import csv

from tucargaapi.apps.directory.models import Company, TransportCompany


def get_transportcompanies():
    transportcompanies = TransportCompany.objects.filter(
        company__status=Company.STATUS_APPROVED)
    rows = [(str(tc.pk), tc.company.name) for tc in transportcompanies]
    filename = TransportCompany.__name__
    write_to_csv(filename, rows)


def write_to_csv(filename, rows):
    filename = 'data/output/' + filename + '.csv'
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow([s.encode("utf-8") for s in row])


def main():
    get_transportcompanies()


def run():
    main()

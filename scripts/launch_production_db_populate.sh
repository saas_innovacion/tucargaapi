# add new database
heroku addons:add heroku-postgresql:dev
heroku pg:promote $(heroku pg:info | grep POSTGRESQL | cut -d" " -f 2)

# fetch local config
heroku config:pull --env .env_tucargaapi-master --overwrite

# sync and migrate
heroku run:detached python manage.py syncdb --noinput
echo "Press enter when syncdb is ready"; read x
heroku run:detached python manage.py migrate --no-initial-data
echo "Press enter when migrate is ready"; read x

# foreman run python manage.py createsuperuser --env .env_tucargaapi-master

# load data
heroku run:detached python manage.py loaddata fixtures/containertype.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/locations.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/coverage.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/days.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/equipment.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/freighttypes.json
echo "Press enter when ready"; read x
heroku run:detached python manage.py loaddata fixtures/trucktype.json
echo "Press enter when ready"; read x

# import companies
foreman run python manage.py company_importer data/empresas_para_importar.csv --env .env_tucargaapi-master

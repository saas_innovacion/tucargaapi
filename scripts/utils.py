import csv


def get_model_from_csv(model):
    filename = 'data/input/' + model.__name__ + '.csv'

    with open(filename, 'rb') as csvfile:
        rows = csv.reader(csvfile)
        pks = []
        for row in rows:
            pks.append(int(row[0]))

    items = model.objects.filter(pk__in=pks)
    return items

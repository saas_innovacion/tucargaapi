import csv

from django.db import IntegrityError

from tucargaapi.apps.routes.models import RouteExport, RouteImport, RoutePlace


def capitalize_words(words):
    words = ' '.join([w.capitalize() for w in words.split(' ')])
    words = words.replace(' Del ', ' del ').replace(' De ', ' de ')
    return words


def get_place(value):
    capitalized_value = capitalize_words(value)
    place, c = RoutePlace.objects.get_or_create(name=capitalized_value)
    return place


def load_routes(model, rows, route_type):
    for row in rows:
        deposit = None
        row_len = len(row)
        if row_len == 3:
            if route_type == 'import':
                port, client, deposit = row
            else:
                deposit, client, port = row
        if row_len == 2:
            if route_type == 'import':
                port, client = row
            else:
                client, port = row
        client = get_place(capitalize_words(client))
        port = get_place(capitalize_words(port))
        if deposit is not None:
            deposit = get_place(capitalize_words(deposit))
        try:
            model.objects.create(
                client=client, port=port, deposit=deposit)
        except IntegrityError, e:
            print e
            pass


def main():
    label = 'data/rutas-exportacion-para-tarifas.csv'

    with open(label, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',')
        load_routes(RouteExport, rows, 'export')

    label = 'data/rutas-importacion-para-tarifas.csv'
    with open(label, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',')
        load_routes(RouteImport, rows, 'import')


def run():
    main()

if __name__ == "__main__":
    main()

BRANCH=$(git rev-parse --abbrev-ref HEAD)
REPO_NAME="tucarga"
APP_NAME=$REPO_NAME-$BRANCH

heroku pg:reset DATABASE_URL --app $APP_NAME
heroku pgbackups:restore DATABASE_URL `heroku pgbackups:url --app tucargaapi-master` --app $APP_NAME

sudo su postgres -c "psql -q <<-EOF
CREATE ROLE django WITH ENCRYPTED PASSWORD 'django';
ALTER ROLE django WITH SUPERUSER;
ALTER ROLE django WITH LOGIN;
EOF"

sudo su postgres -c "psql -q <<-EOF
CREATE DATABASE django WITH OWNER django;
GRANT ALL ON DATABASE django TO django;
EOF"


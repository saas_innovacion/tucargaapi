# directory
foreman run python manage.py graph_models directory --settings tucargaapi.settings.dev_postgres -g -o docs/db/modelos_directory.svg --env .env_development_postgresql
# tucarga_user
foreman run python manage.py graph_models tucarga_user --settings tucargaapi.settings.dev_postgres -g -o docs/db/modelos_tucarga_user.svg --env .env_development_postgresql
# reports
foreman run python manage.py graph_models reports --settings tucargaapi.settings.dev_postgres -g -o docs/db/modelos_reports.svg --env .env_development_postgresql
# rates
foreman run python manage.py graph_models rates --settings tucargaapi.settings.dev_postgres -g -o docs/db/modelos_reports.svg --env .env_development_postgresql
# routes
foreman run python manage.py graph_models routes --settings tucargaapi.settings.dev_postgres -g -o docs/db/modelos_reports.svg --env .env_development_postgresql
# all
foreman run python manage.py graph_models directory tucarga_user reports --settings tucargaapi.settings.dev_postgres locationstree django_nopassword auth -g -o docs/db/modelos_all.svg --env .env_development_postgresql

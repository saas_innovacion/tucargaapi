BRANCH=$(git rev-parse --abbrev-ref HEAD)
# prefix should be taken from repo name
REPO_NAME="tucargaapi"
APP_NAME=$REPO_NAME-$BRANCH
# this should be a mandatory argument
BASE_ENV=staging
BASE_APP_NAME=$REPO_NAME-$BASE_ENV
ENV_FILE_NAME=.env_"$APP_NAME"

heroku addons:remove heroku-postgresql
heroku addons:add heroku-postgresql:dev
heroku pg:promote $(heroku pg:info | grep POSTGRESQL | cut -d" " -f 2)
heroku config:pull --env $ENV_FILE_NAME --overwrite

heroku run python manage.py syncdb --noinput
heroku run python manage.py migrate --no-initial-data

bash script/heroku_load_development_data.sh

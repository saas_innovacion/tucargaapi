import os

from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.db import transaction

from tucargaapi.apps.directory.models import UserDirectory
import tucargaapi.apps.directory.models as directory_models

User = get_user_model()


def get_users():
    users0 = []
    users0.append(User.objects.create(email='fivasquez@ultramar.cl'))
    users0.append(User.objects.create(email='glarrain@ultramar.cl'))
    users1 = User.objects.filter(email__contains='tucarga.cl')
    users = set(users0).union(set(users1))
    return users


def change_users_password(users, password='1234'):
    map(User.set_password, users, [password]*len(users))
    map(User.save, users)


def associate_transport_company(users):
    count = 1
    for user in users:
        company = directory_models.Company.objects.create(
            name='Transportes %s' % user.email,
            business_name='Transportes %s' % user.email,
            business_number='11111111-%s' % (count),
            status=directory_models.Company.STATUS_APPROVED)
        directory_models.TransportCompany.objects.create(
            company=company)
        UserDirectory.objects.create(
            company=company, user=user)

        count += 1


def fix_sites():
    FRONT_SITE_DOMAIN = os.environ.get('FRONT_SITE_DOMAIN')
    API_SITE_DOMAIN = os.environ.get('API_SITE_DOMAIN')
    site = Site.objects.get(name='Tu Carga Web')
    site.domain = FRONT_SITE_DOMAIN
    site.save()
    site = Site.objects.get(name='Tu Carga API')
    site.domain = API_SITE_DOMAIN
    site.save()


def run():
    with transaction.atomic():
        users = get_users()
        change_users_password(users)
        associate_transport_company(users)
        fix_sites()

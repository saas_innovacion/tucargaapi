# From
# https://devcenter.heroku.com/articles/upgrade-heroku-postgres-with-pgbackups

# APP_NAME=

# heroku addons:add heroku-postgresql:hobby-basic --app $APP_NAME
# # keep track of the new database URL
# heroku pg:wait --app $APP_NAME
# heroku maintenance:on --app $APP_NAME
# heroku ps:scale worker=0 --app $APP_NAME
# heroku pgbackups:transfer HEROKU_POSTGRESQL_ --app $APP_NAME
# heroku pg:promote HEROKU_POSTGRESQL_ --app $APP_NAME
# heroku ps:scale worker=1 --app $APP_NAME
# heroku maintenance:off --app $APP_NAME
# heroku pg:info --app $APP_NAME
# heroku addons:remove HEROKU_POSTGRESQL_BROWN_URL --app $APP_NAME

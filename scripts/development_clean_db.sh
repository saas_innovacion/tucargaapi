# clean db
rm -f tucargaapi/default.db
python manage.py syncdb --noinput
python manage.py migrate
python manage.py loaddata $(cat scripts/development_fixtures_list.txt)

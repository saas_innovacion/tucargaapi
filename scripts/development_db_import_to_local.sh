dropdb django -h 127.0.0.1 -p 5432 -U django
createdb django -O django -h 127.0.0.1 -p 5432 -U postgres
pg_restore --clean --no-acl --no-owner -h 127.0.0.1 -U django -d django latest.dump

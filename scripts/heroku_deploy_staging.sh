echo "This is set for development instances"
echo "Get your id rsa key!"
read x

BRANCH=$(git rev-parse --abbrev-ref HEAD)
# TODO: move to base_bars.sh
REPO_NAME="tucarga"
# TODO: move to base_bars.sh
APP_NAME=$REPO_NAME-$BRANCH
# this should be a mandatory argument
BASE_ENV=staging
BASE_APP_NAME=$REPO_NAME-$BASE_ENV
ENV_FILE_NAME=.env_"$APP_NAME"
ENV_BASE_FILE_NAME=.env_"$BASE_ENV"_base

# in any error(like "Name is already taken") script should exit
heroku create $APP_NAME

echo "worked ?????? C - c if Name is already taken"; read x

heroku git:remote -r $BRANCH --app $APP_NAME

heroku addons:add heroku-postgresql:hobby-basic --app $APP_NAME
heroku addons:add pgbackups:auto-month --app $APP_NAME
heroku addons:add mailtrap --app $APP_NAME
heroku addons:add rabbitmq-bigwig --app $APP_NAME

heroku pg:promote $(heroku pg:info --app $APP_NAME | grep POSTGRESQL | cut -d" " -f 2) --app $APP_NAME

cp $ENV_BASE_FILE_NAME $ENV_FILE_NAME
echo AWS_STORAGE_BUCKET_NAME=$APP_NAME >> $ENV_FILE_NAME

echo FRONT_SITE_DOMAIN=www.tucarga.info >> $ENV_FILE_NAME
echo API_SITE_DOMAIN=$APP_NAME.herokuapp.com >> $ENV_FILE_NAME

# For addon rabbitmq-bigwig
echo BROKER_URL=$(heroku config:get RABBITMQ_BIGWIG_URL --app $APP_NAME) >> $ENV_FILE_NAME
echo CELERY_RESULT_BACKEND=$(heroku config:get RABBITMQ_BIGWIG_URL --app $APP_NAME) >> $ENV_FILE_NAME

# Get and set email settings from addon
MAILTRAP_API_TOKEN=$(heroku config:get MAILTRAP_API_TOKEN --app $APP_NAME)
python scripts/parse_mailtrap_credentials.py $MAILTRAP_API_TOKEN $ENV_FILE_NAME

# Set and get environment
heroku config:push --env $ENV_FILE_NAME --app $APP_NAME
heroku config:pull --env $ENV_FILE_NAME --overwrite --app $APP_NAME

git push $BRANCH $BRANCH:master

heroku ps --app $APP_NAME
heroku open --app $APP_NAME

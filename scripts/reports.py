# encoding: utf-8

from tucargaapi.apps.directory.models import Company, TransportCompany
from tucargaapi.apps.reports.models import Report


def company_to_mailchimp():
    cc = Company.objects.all()

    for c in cc:
        try:
            c.transportcompany
        except TransportCompany.DoesNotExist:
            for u in c.userdirectory_set.all():
                print(
                    u'{},{},{},{}'.format(
                        u.user.first_name, u.user.last_name, u.user.email, c.name).encode('utf-8'))


def transportcompany_coverage():
    tc = TransportCompany.objects.all()

    for c in tc:
        print(
            u'{},{},{},{}'.format(
                c.company.name, c.company.business_name, c.company.business_number, c.coverage_names()))


def weekly_operations():
    rp = Report()
    rp.main()
    rp.to_model()
    rp.save()

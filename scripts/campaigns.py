from django.core.urlresolvers import reverse

from django_nopassword.models import LoginCode

from tucargaapi.apps.directory.models import TransportCompany
import tucargaapi.apps.directory.utils as directory_utils

import utils


def email_transport_companies():
    DEFAULT_EMPTY_VALUE = 'Sin Datos'
    subject = 'En tucarga.cl te queremos conseguir carga. Actualiza tu perfil ahora'
    row_names = (
        'FNAME',
        'LNAME',
        'PNAME',
        'HREF',
        'ADDRESS',
        'RUT',
        'PHONE',
    )

    transportcompanies = utils.get_model_from_csv(TransportCompany)

    site = directory_utils.get_api_site()
    update_url = reverse('profile-transportcompany')

    rows = []
    for transportcompany in transportcompanies:
        userdirectories = \
            transportcompany.company.userdirectory_set.all()
        for userdirectory in userdirectories:
            user = userdirectory.user
            company = userdirectory.company
            login_code = LoginCode.create_code_for_user(
                user=user, next=update_url)
            url = directory_utils.get_url(site, 'login-with-code', login_code.code)
            rows.append((user.email,
                         user.full_name or DEFAULT_EMPTY_VALUE,
                         '',
                         company.name,
                         url,
                         company.address or DEFAULT_EMPTY_VALUE,
                         company.business_number,
                         userdirectory.phone or DEFAULT_EMPTY_VALUE, ))

    local_vars = {}

    print(rows)
    raw_input('Continuar?')

    for row in rows:
        detail = dict(zip(row_names, row[1:]))
        user = row[0]
        local_vars[user] = detail
        directory_utils.send_mail.delay(
            [user],
            template_name='transport-profile',
            subject=subject,
            local_vars=local_vars)


def run():
    email_transport_companies()

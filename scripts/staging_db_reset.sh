ENV_FILE_NAME=.env_tucargaapi-staging
APP_NAME=tucargaapi-staging

heroku pgbackups:capture --app $APP_NAME

heroku addons:remove heroku-postgresql --app $APP_NAME
heroku addons:add heroku-postgresql:dev --app $APP_NAME
heroku pg:promote $(heroku pg:info --app $APP_NAME | grep POSTGRESQL | cut -d" " -f 2) --app $APP_NAME
heroku config:pull --env $ENV_FILE_NAME --overwrite --app $APP_NAME

heroku run python manage.py syncdb --noinput --app $APP_NAME
heroku run python manage.py migrate --no-initial-data --app $APP_NAME

bash scripts/staging_db_load_data.sh

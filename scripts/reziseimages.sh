if [[ $# -eq 0 ]]
then
    echo "Usage: reziseimages.sh <images dir path>"
    exit 0
fi

input=$1
output="$input"output/
outputlast="$input"outputlast/

# lowercase extensions
rename 's/\.JPG$/\.jpg/' *.JPG
# replace spaces by underscores
rename 'y/ /_/' *
# find . -type f
mkdir -p $output
mkdir -p $outputlast
# find all files and rezise
for i in $(find $input -type f)
do
    filename=$(basename "$i")
    extension="${filename##*.}"
    filename="${filename%.*}"

    inputfile="$input""$filename"."$extension"
    outputfile="$output""$filename"_125x125."$extension"
    outputfilelast="$outputlast""$filename"_125x125."$extension"

    # echo "$inputfile"
    # echo "$outputfile"
    # rezise to max(125x125)
    convert "$inputfile" -resize 125x125 "$outputfile"

    # rezise to 125x125
    convert "$outputfile" -gravity center -extent 125x125 -background none  "$outputfilelast"
done

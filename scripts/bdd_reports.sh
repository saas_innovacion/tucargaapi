# Reporte completo
python manage.py test directory \
  --behave_summary \
  --behave_format html \
  --behave_tags v1 \
  --behave_no-color \
  --behave_no-source \
  --behave_no-logcapture \
  --behave_no-skipped \
  --behave_outfile docs/tucargaapi_features.html

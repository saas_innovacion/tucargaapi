#!/bin/env python
from urllib2 import Request, urlopen
import json
import sys

MAILTRAP_ENDPOINT = "https://mailtrap.io/api/v1/inboxes?api_token={}"
token = sys.argv[1]
output_file_name = sys.argv[2]
output_file = file(output_file_name, 'a')
url = MAILTRAP_ENDPOINT.format(token)

request = Request(url)
response_body = urlopen(request).read()
credentials = json.loads(response_body)[0]

output_file.write('EMAIL_HOST={}\n'.format(credentials['domain']))
output_file.write('EMAIL_HOST_USER={}\n'.format(credentials['username']))
output_file.write('EMAIL_HOST_PASSWORD={}\n'.format(credentials['password']))
output_file.write('EMAIL_PORT={}\n'.format(credentials['smtp_ports'][0]))
output_file.write('EMAIL_USE_TLS={}\n'.format(True))

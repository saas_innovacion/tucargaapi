echo "This is set for development instances"
echo "Get your id rsa key!"
read x

BRANCH=$(git rev-parse --abbrev-ref HEAD)
# prefix should be taken from repo name
# can be moved to base_bars.sh
REPO_NAME="tucarga"
# can be moved to base_bars.sh
APP_NAME=$REPO_NAME-$BRANCH
# this should be a mandatory argument

DOMAIN='info'
BASE_ENV=staging
if [ $BRANCH == 'master' ];
then
    DOMAIN='cl'
    BASE_ENV=production
fi

BASE_APP_NAME=$REPO_NAME-$BASE_ENV
ENV_FILE_NAME=.env_"$APP_NAME"
ENV_BASE_FILE_NAME=.env_"$BASE_ENV"_base

# in any error(like "Name is already taken") script should exit
heroku create $APP_NAME

echo "worked ?????? C - c if Name is already taken"; read x

heroku git:remote -r $BRANCH --app $APP_NAME

heroku addons:add heroku-postgresql:hobby-basic --app $APP_NAME
heroku addons:add pgbackups:auto-month --app $APP_NAME
heroku addons:add rabbitmq-bigwig --app $APP_NAME
# heroku addons:add scheduler:standard --app $APP_NAME
# heroku addons:add memcachier:dev --app $APP_NAME
# heroku addons:add newrelic --app $APP_NAME
# heroku addons:add sentry:developer --app $APP_NAME
# heroku addons:add mandrill --app $APP_NAME

heroku pg:promote $(heroku pg:info --app $APP_NAME | grep POSTGRESQL | cut -d" " -f 2) --app $APP_NAME

cp $ENV_BASE_FILE_NAME $ENV_FILE_NAME
echo AWS_STORAGE_BUCKET_NAME=$APP_NAME >> $ENV_FILE_NAME

# Get and set email settings from addon

# not needed if creating
# heroku config:unset EMAIL_HOST EMAIL_HOST_PASSWORD EMAIL_PORT EMAIL_HOST_USER --app $APP_NAME
echo EMAIL_HOST=$(heroku config:get smtp.mandrillapp.com --app $APP_NAME) >> $ENV_FILE_NAME
echo EMAIL_PORT=$(heroku config:get 587 --app $APP_NAME) >> $ENV_FILE_NAME
echo EMAIL_HOST_USER=$(heroku config:get MANDRILL_USERNAME --app $APP_NAME) >> $ENV_FILE_NAME
echo EMAIL_HOST_PASSWORD=$(heroku config:get MANDRILL_APIKEY --app $APP_NAME) >> $ENV_FILE_NAME

# For addon rabbitmq-bigwig
echo BROKER_URL=$(heroku config:get RABBITMQ_BIGWIG_URL --app $APP_NAME) >> $ENV_FILE_NAME
echo CELERY_RESULT_BACKEND=$(heroku config:get RABBITMQ_BIGWIG_URL --app $APP_NAME) >> $ENV_FILE_NAME

# Set and get environment

heroku config:push --env $ENV_FILE_NAME --app $APP_NAME
heroku config:pull --env $ENV_FILE_NAME --overwrite --app $APP_NAME

git push $BRANCH $BRANCH:master

# This should only be run when creating an app when an app exists
# heroku pgbackups:capture $(heroku pg:info --app $BASE_APP_NAME | grep POSTGRESQL | cut -d" " -f 2) --app $BASE_APP_NAME
# heroku pgbackups:restore DATABASE_URL $(heroku pgbackups:url $(heroku pgbackups --app $BASE_APP_NAME | tail -n 1 | cut -d " " -f 1) --app $BASE_APP_NAME) --app $APP_NAME --confirm $APP_NAME

# can be usefull to have a file with all the paths to fixtures that
# should be loaded at install so this command doesn't fail
#
#  # heroku run python manage.py loaddata 
#  # or
#  # DJANGO_SETTINGS_MODULE='tucargaapi.settings.prod' \
#  #   foreman run python manage.py loaddata
#        --env=.env_tucargaapi-master --app $APP_NAME
#   fixtures/locationstree.json fixtures/freighttypes.json chile.json fixtures/development.json fixtures/coverage.json


# heroku run python manage.py loaddata fixtures/days.json --app $APP_NAME

# heroku calls collectstatic
# heroku run python manage.py collectstatic --noinput --app $APP_NAME

heroku domains:add api.tucarga.$DOMAIN --app $APP_NAME
heroku domains:add backoffice.tucarga.$DOMAIN --app $APP_NAME
#heroku domains:add estado.tucarga.$DOMAIN --app $APP_NAME

heroku ps --app $APP_NAME
heroku open --app $APP_NAME

#####
# RUN MANUALLY using detached, because heroku client report "Error R13 (Attach error) -> Failed to attach to process"
#
# heroku run:detached python manage.py syncdb --noinput --app $APP_NAME
# wait until it's finish
# heroku run:detached python manage.py migrate --no-initial-data --app $APP_NAME
#####
